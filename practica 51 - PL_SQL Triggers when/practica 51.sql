/*Una librer�a almacena los datos de sus libros en una tabla denominada "libros" y en otra denominada "ofertas", 
almacena los c�digos y precios de los libros cuyo precio es inferior a 50.

1- Elimine las tablas:*/
drop table libros;
drop table ofertas;

--2- Cree las tablas con las siguientes estructuras:
create table libros(codigo number(6),titulo varchar2(40),autor varchar2(30),editorial varchar(20),precio number(6,2));
create table ofertas(codigo number(6),titulo varchar2(40));

--3- Ingrese algunos registros en "libros":
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);

--4- Cree un trigger a nivel de fila que se dispara "antes" que se ejecute un "insert" sobre "libros". Se activa 
--solamente si el precio que se ingresa es inferior a 30, en caso de serlo, se ingresa en "ofertas" el c�digo y precio del libro
create or replace trigger bi_libros
before insert on libros
for each row
declare
begin
    if :new.precio<30 then
        insert into ofertas values(:new.codigo, :new.titulo);
    end if;
end;

--5- Ingrese un libro en "libros" cuyo precio sea inferior a 30
insert into libros values(146,'Alicia barato','Carroll','Planeta',25);

--6- Verifique que el trigger se dispar� consultando "ofertas"
select * from ofertas;
--146	Alicia barato

--7- Ingrese un libro en "libros" cuyo precio supere los 30
insert into libros values(147,'Alicia caro','Carroll','Planeta',35);

--8- Verifique que el trigger no se dispar� consultando "ofertas"
select * from ofertas;

--9- Cree un trigger a nivel de fila que se dispare al borrar un libro de "libros", �nicamente si el precio del libro que se 
--elimina es inferior a 30, es decir, si existe en "ofertas"
create or replace trigger bd_libros
before delete on libros
for each row when(old.precio<30)--equivalente a if
declare
begin
    delete from ofertas where codigo=:old.codigo;
end;

--10- Elimine un registro de "libros" cuyo precio sea inferior a 30
delete from libros where codigo=146;

--11- Verifique que el trigger se dispar� consultando "ofertas" y "libros"
select * from libros;
select * from ofertas;

--12- Elimine un registro de "libros" cuyo precio supere los 30
delete from libros where codigo=147;

--13- Verifique que el trigger no se dispar� consultando "ofertas" y que si se ha eliminado el registro en "libros"
select * from libros;
select * from ofertas;