/*Un videoclub que alquila pel�culas en video almacena la informaci�n de sus pel�culas 
en una tabla llamada "peliculas"; para cada pel�cula necesita los siguientes datos:

1-nombre, cadena de caracteres de 20 de longitud, -actor, cadena de caracteres de 20 de longitud, 
-duraci�n, valor num�rico entero que no supera los 3 d�gitos. -cantidad de copias: valor entero 
de un s�lo d�gito (no tienen m�s de 9 copias de cada pel�cula). 1- Elimine la tabla "peliculas" si ya existe.*/
DROP TABLE PELICULAS;

/*2- Cree la tabla eligiendo el tipo de dato adecuado para cada campo. Note que los campos 
"duracion" y "cantidad", que almacenar�n valores sin decimales, fueron definidos de maneras 
diferentes, en el primero especificamos el valor 0 como cantidad de decimales, en el 
segundo no especificamos cantidad de decimales, es decir, por defecto, asume el valor 0.*/
CREATE TABLE peliculas( nombre varchar2(20), actor varchar2(20), duracion number(3,0),
cantidad number(1)); 

/*3- Vea la estructura de la tabla.*/
describe peliculas;

/*4- Ingrese los siguientes registros: */
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',128,3); 
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',130,2); 
insert into peliculas (nombre, actor, duracion, cantidad) values ('Pretty Woman','Julia Roberts',118,3); 
insert into peliculas (nombre, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',110,2);


/*5- Muestre todos los registros (4 registros)*/
SELECT * FROM PELICULAS;

/*6- Intente ingresar una pel�cula con valor de cantidad fuera del rango permitido:
insert into peliculas (nombre, actor, duracion, cantidad) values (�Pretty Woman','Richard Gere',1200,10); 
Mensaje de error.*/ 
insert into peliculas (nombre, actor, duracion, cantidad) values ('Pretty Woman','Richard Gere',1200,10); 

/*7- Ingrese un valor con decimales en un nuevo registro, en el campo "duracion":*/
insert into peliculas (nombre, actor, duracion, cantidad) values ('Pretty Woman','Richard Gere',120.20,4); 

/*8- Muestre todos los registros para ver c�mo se almacen� el �ltimo registro ingresado.*/
SELECT * FROM PELICULAS;

/*9- Intente ingresar un nombre de pel�cula que supere los 20 caracteres.*/
insert into peliculas (nombre, actor, duracion, cantidad) values ('Pretty WomanNNNNNNNNN','Richard Gere',120.20,4); 

/*PROBAR CASO EXTRA DE NUMBER. Hay que tener cuidado con poner la cantidad de decimales en negativo*/
DROP TABLE PELICULAS;
CREATE TABLE peliculas( nombre varchar2(20), actor varchar2(20), duracion NUMBER(7,-2),
cantidad number(1)); 
insert into peliculas (nombre, actor, duracion, cantidad) values ('Pretty WomanN','Richard Gere',50.20,4); 
SELECT * FROM PELICULAS;