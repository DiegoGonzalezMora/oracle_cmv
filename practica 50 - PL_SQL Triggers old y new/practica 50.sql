/*Un comercio almacena los datos de sus art�culos para la venta en una tabla denominada "articulos" y controla las 
operaciones que se realizan en ella guardando ciertos datos en otra tabla denominada "control".

1- Elimine las tablas:*/
drop table control;
drop table articulos;

--2- Cree las tablas con las siguientes estructuras:
create table articulos(codigo number(6),descripcion varchar2(40),precio number (6,2),stock number(4));
create table control(usuario varchar2(30),fecha date,codigo number(6));

--3- Ingrese algunos registros en "articulos":
insert into articulos values(100,'regla 20 cm.',5.4,100);
insert into articulos values(102,'regla 40 cm.',15,80);
insert into articulos values(109,'lapices color x12',6,150);
insert into articulos values(130,'lapices color x6',4.5,100);
insert into articulos values(200,'compas metal',21.8,50);

--4- Cree un trigger a nivel de fila que se dispara "antes" que se ejecute un "insert" sobre la tabla "articulos". En el cuerpo 
--del disparador se debe ingresar en la tabla "control", el nombre del usuario que realiz� la inserci�n, la fecha y el 
--c�digo del articulo que se ha ingresado
create or replace trigger BI_articulos_control
before insert on articulos
for each row
declare
begin
    insert into control values(user, sysdate, :new.codigo);
end;

--5- Ingrese un nuevo registro en "articulos"
insert into articulos values(220,'paquete A4',4.99,150);

--6- Vea qu� se almacen� en "control"
select * from control;

--7- Cree un disparador que calcule el c�digo cada vez que se inserte un nuevo registro
drop trigger bi_articulo;

create or replace trigger bi_articulo
before insert on articulos
for each row
declare
v_codigo number(6);
begin
    select max(codigo+1) into v_codigo from articulos;
    :new.codigo:=v_codigo;
end;

--8- Ingrese un nuevo registro en "articulos"
insert into articulos values(99,'paquete A5',7.99,50);

--9- Vea qu� se almacen� en "articulos".
select * from articulos;
--Note que ignora el valor de c�digo ingresado y calcula el siguiente valor a partir del m�ximo existente.

--10- Vea qu� se almacen� en "control"
select * from control;
/*
SYSTEM	17/11/17	220
SYSTEM	17/11/17	221
*/

--11- Ingrese un nuevo art�culo sin especificar c�digo
insert into articulos (descripcion, precio, stock) values('paquete A6',7.99,50);

--12- Vea qu� se almacen� en "articulos"
select * from articulos;
--222	paquete A6	7,99	50

--13- Vea qu� se almacen� en "control"
select * from control;
/*
SYSTEM	17/11/17	220
SYSTEM	17/11/17	221
SYSTEM	17/11/17	222
*/