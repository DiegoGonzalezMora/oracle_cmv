/*Una cl�nica almacena los datos de los m�dicos en una tabla llamada "medicos" y los datos de los 
pacientes en otra denominada "pacientes".

1- Eliminamos ambas tablas:*/
drop table medicos;
drop table pacientes;

--2- Creamos las tablas:
create table medicos(
legajo number(3),
documento varchar2(8) not null,
nombre varchar2(30),
domicilio varchar2(30),
especialidad varchar2(30),
primary key(legajo)
);

create table pacientes(
documento varchar2(8) not null,
nombre varchar2(30),
domicilio varchar2(30),
obrasocial varchar2(20),
primary key(documento)
);

--3- Ingresamos algunos registros:
insert into medicos values(1,'20111222','Ana Acosta','Avellaneda 111','clinica');
insert into medicos values(2,'21222333','Betina Bustos','Bulnes 222','clinica');
insert into medicos values(3,'22333444','Carlos Caseros','Colon 333','pediatria');
insert into medicos values(4,'23444555','Daniel Duarte','Duarte Quiros 444','oculista');
insert into medicos values(5,'24555666','Estela Esper','Esmeralda 555','alergia');

insert into pacientes values('24555666','Estela Esper','Esmeralda 555','IPAM');
insert into pacientes values('23444555','Daniel Duarte','Duarte Quiros 444','OSDOP');
insert into pacientes values('30111222','Fabiana Fuentes','Famatina 666','PAMI');
insert into pacientes values('30111222','Gaston Gonzalez','Guemes 777','PAMI');

--4- La cl�nica necesita el nombre y domicilio de m�dicos y pacientes para enviarles una tarjeta de invitaci�n a 
--la inauguraci�n de un nuevo establecimiento. Emplee el operador "union" para obtener dicha informaci�n de ambas tablas 
--Note que existen dos m�dicos que tambi�n est�n presentes en la tabla "pacientes"; tales registros aparecen una 
--sola vez en el resultado de "union".
select nombre, domicilio from pacientes
union
select nombre, domicilio from medicos;

select nombre, domicilio, 'paciente' as categoria from pacientes
union
select nombre, domicilio, 'medico' as categoria from medicos;
--Las dos personas que son medicos y han sido pacientes son Daniel Duarte y Estela Esper

--5- La cl�nica necesita el nombre y domicilio de los pacientes que tambi�n son m�dicos para enviarles una tarjeta 
--de descuento para ciertas pr�cticas. Emplee el operador "intersect" para obtener dicha informaci�n de ambas tablas
select nombre, domicilio from pacientes
intersect
select nombre, domicilio from medicos;

--tambien se podria hacer con join
select m.nombre, m.domicilio 
from medicos m join pacientes p on(m.documento=p.documento);