/*Necesita almacenar los datos de amigos en una tabla. Los datos que guardar� 
ser�n: apellido, nombre, domicilio y tel�fono.

1- Elimine la tabla "agenda" Si no existe, un mensaje indicar� tal situaci�n.*/
DROP TABLE AGENDA;

/*2- Intente crear una tabla llamada "*agenda"

create table *agenda( apellido varchar2(30), nombre varchar2(20), domicilio 
varchar2(30), telefono varchar2(11) ); aparece un mensaje de error indicando que 
usamos un caracter inv�lido ("*") para el nombre de la tabla.*/
create table *agenda( apellido varchar2(30), nombre varchar2(20), domicilio varchar2(30), telefono varchar2(11) );

/*3- Cree una tabla llamada "agenda", debe tener los siguientes campos: apellido, 
varchar2(30); nombre, varchar2(20); domicilio, varchar2 (30) y telefono, varchar2(11) 
Un mensaje indica que la tabla ha sido creada exitosamente.*/
create table agenda( apellido varchar2(30), nombre varchar2(20), domicilio varchar2(30), telefono varchar2(11) );

/*4- Intente crearla nuevamente. Aparece mensaje de error indicando que el nombre
ya lo tiene otro objeto.*/
create table agenda( apellido varchar2(30), nombre varchar2(20), domicilio varchar2(30), telefono varchar2(11) );

/*5- Visualice las tablas existentes (all_tables) La tabla "agenda" aparece en la lista.*/
select * from sys.dba_tables;

/*6- Visualice la estructura de la tabla "agenda" (describe) Aparece la siguiente tabla:

Name Null Type

------------------------------

APELLIDO VARCHAR2(30)

NOMBRE VARCHAR2(20)

DOMICILIO VARCHAR2(30)

TELEFONO VARCHAR2(11)*/
DESCRIBE AGENDA;


