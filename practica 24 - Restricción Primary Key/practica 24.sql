/*Una empresa tiene registrados datos de sus empleados en una tabla llamada "empleados".

1- Elimine la tabla:*/
drop table empleados;

--2- Cr�ela con la siguiente estructura:
create table empleados ( documento char(8), nombre varchar2(30), seccion varchar2(20) );

--3- Ingrese algunos registros, dos de ellos con el mismo n�mero de documento:
insert into empleados values('00000001', 'Luis Manuel', 'charcuteria');
insert into empleados values('00000002', 'Manolo', 'cocina');
insert into empleados values('00000003', 'Pedro', 'almacen');
insert into empleados values('00000003', 'Alberto', 'cocina');

--4- Intente establecer una restricci�n "primary key" para la tabla para que el documento no se repita 
--ni admita valores nulos. No lo permite porque la tabla contiene datos que no cumplen con la restricci�n,
--debemos eliminar (o modificar) el registro que tiene documento duplicado.
ALTER TABLE empleados ADD CONSTRAINT EMPLEADOS_DOCUMENTOS_PK PRIMARY KEY(DOCUMENTO);
/*
ORA-02437: cannot validate (SYSTEM.EMPLEADOS_PK) - primary key violated
02437. 00000 -  "cannot validate (%s.%s) - primary key violated"
*Cause:    attempted to validate a primary key with duplicate values or null
           values.
*Action:   remove the duplicates and null values before enabling a primary
           key.*/
UPDATE EMPLEADOS SET DOCUMENTO='00000004' WHERE NOMBRE='Alberto';

--5- Establecezca la restricci�n "primary key" del punto 4
ALTER TABLE empleados ADD CONSTRAINT EMPLEADOS_DOCUMENTOS_PK PRIMARY KEY(DOCUMENTO);

--6- Intente actualizar un documento para que se repita. No lo permite porque va contra la restricci�n.
UPDATE EMPLEADOS SET DOCUMENTO='00000003' WHERE NOMBRE='Alberto';
/*
UPDATE EMPLEADOS SET DOCUMENTO='00000003' WHERE NOMBRE='Alberto'
Informe de error -
ORA-00001: unique constraint (SYSTEM.EMPLEADOS_PK) violated*/

--7-Intente establecer otra restricci�n "primary key" con el campo "nombre".
ALTER TABLE EMPLEADOS ADD CONSTRAINT EMPLEADOS_NOMBRE_PK PRIMARY KEY(NOMBRE);
/*
Informe de error -
ORA-02260: table can have only one primary key
02260. 00000 -  "table can have only one primary key"
*Cause:    Self-evident.
*Action:   Remove the extra primary key.*/

--8- Vea las restricciones de la tabla "empleados" consultando el cat�logo "user_constraints" (1 restricci�n "P")
SELECT * FROM USER_CONSTRAINTS WHERE TABLE_NAME='EMPLEADOS';
--SYSTEM	EMPLEADOS_PK	P	EMPLEADOS  ENABLED	NOT DEFERRABLE	IMMEDIATE	VALIDATED	USER NAME	09/11/17	SYSTEM	EMPLEADOS_PK		

--9- Consulte el cat�logo "user_cons_columns"
SELECT * FROM USER_CONS_COLUMNS;