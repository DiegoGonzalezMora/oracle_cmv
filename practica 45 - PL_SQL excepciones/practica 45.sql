drop table clientes;
create table clientes(
id_clientes number(3),
Descrip_cliente varchar2(30),
fecha date,
primary key (id_clientes)
);

insert into clientes values(100,	'Juan Martos Ga�lan',	'02/01/15');
insert into clientes values(150,	'La papeler�a s.a',	'18/01/15');
insert into clientes values(160,	'Librer�a T�cnica s.l',	'05/01/03');
insert into clientes values(200,	'Mar�a Garc�a Acosta', 	'12/01/15');
insert into clientes values(210,	'Jos� Mart�n Rubio',	'15/01/15');



set SERVEROUTPUT ON;
Declare
Vi_id NUMBER;
BEGIN
    BEGIN
        Select id_clientes into Vi_id from clientes where id_clientes=100;
        Exception
            When no_data_found then
                Dbms_output.put_line('Consulta 1: n� Error: ' ||SQLCODE||' Mensaje:'|| SQLERRM);
    End;
        
    BEGIN
        Select id_clientes into Vi_id from clientes where id_clientes=555;
        Exception
            When no_data_found then
                Dbms_output.put_line('Consulta 2: n� Error: ' ||SQLCODE||' Mensaje:'|| SQLERRM);
    End;
    
    BEGIN
        Select id_clientes into Vi_id from clientes where id_clientes=200;
        Exception
            When no_data_found then
                Dbms_output.put_line('Consulta 3: n� Error: ' ||SQLCODE||' Mensaje:'|| SQLERRM);
    End;

End;