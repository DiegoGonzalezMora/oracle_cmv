/*Una empresa almacena los datos de sus empleados en una tabla denominada "empleados" y en otra llamada "control" 
guarda un registro por cada empleado que se elimina de la tabla "empleados".

1- Elimine las tablas:*/
drop table empleados;                                                                                         
drop table control;

--2- Cree las tablas con las siguientes estructuras:
create table empleados(documento char(8),apellido varchar2(20),nombre varchar2(20),seccion varchar2(30),sueldo number(8,2));
create table control(usuario varchar2(30),fecha date);

--3- Ingrese algunos registros en "empleados":
insert into empleados values('22333444','ACOSTA','Ana','Secretaria',500);
insert into empleados values('22777888','DOMINGUEZ','Daniel','Secretaria',560);
insert into empleados values('22999000','FUENTES','Federico','Sistemas',680);
insert into empleados values('22555666','CASEROS','Carlos','Contaduria',900);
insert into empleados values('23444555','GOMEZ','Gabriela','Sistemas',1200);
insert into empleados values('23666777','JUAREZ','Juan','Contaduria',1000);

--4- Cree un disparador a nivel de fila, que se dispare cada vez que se borre un registro de "empleados"; el trigger debe 
--ingresar en la tabla "control", el nombre del usuario y la fecha en la cual se realiz� un "delete" sobre "empleados"
ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI'; 

create or replace trigger AD_empleados_control
before delete on empleados
for each row
declare
begin
    insert into control values(user,sysdate);
end;

--5- Vea qu� informa el diccionario "user_triggers" respecto del trigger anteriormente creado
select * from user_triggers;
--AD_EMPLEADOS_CONTROL	BEFORE EACH ROW	DELETE	SYSTEM	TABLE	EMPLEADOS

--6- Elimine todos los empleados cuyo sueldo supera los $800
delete from empleados where sueldo>800;
--3 filas eliminado

--7- Vea si el trigger se dispar� consultando la tabla "control"
select * from control;
--Se eliminaron 3 registros, como el trigger fue definido a nivel de fila, se dispar� 3 veces, una vez por cada 
--registro eliminado. Si el trigger hubiese sido definido a nivel de sentencia, se hubiese disparado una sola vez.

--8- Reemplace el disparador creado anteriormente por otro con igual c�digo pero a nivel de sentencia
create or replace trigger AD_empleados_control
before delete on empleados
declare
begin
    insert into control values(user,sysdate);
end;

--9- Vea qu� nos informa el diccionario "user_triggers" respecto del trigger anteriormente creado
select * from user_triggers;
--en este caso es un desencadenador a nivel de sentencia; en la columna "TRIGGER_TYPE" muestra "BEFORE STATEMENT".
--AD_EMPLEADOS_CONTROL	BEFORE STATEMENT	DELETE	SYSTEM	TABLE	EMPLEADOS

--10- Elimine todos los empleados de la secci�n "Secretaria"
delete from empleados where seccion like 'Secretaria';
--Se han eliminado 2 registros, pero el trigger se ha disparado una sola vez.

--11- Consultamos la tabla "control"
select * from control;
--Si el trigger hubiese sido definido a nivel de fila, se hubiese disparado dos veces.