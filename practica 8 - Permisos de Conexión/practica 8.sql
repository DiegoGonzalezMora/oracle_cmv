/*Una escuela necesita crear 3 usuarios diferentes en su base de datos. Uno denominado "director", 
otro "profesor" y otro "estudiante". Luego se les conceder�n diferentes permisos para restringir 
el acceso a los diferentes objetos. Con�ctese como administrador (por ejemplo "system").

1- Cree un usuario denominado "director", con contrase�a "escuela", asign�ndole 100M de 
espacio en "system" (100M). Antes elim�nelo por si existe:*/
DROP USER director;
CREATE USER director IDENTIFIED BY escuela DEFAULT TABLESPACE system QUOTA 100M ON SYSTEM;

/*2- Intente iniciar una sesi�n como "director". No es posible, no hemos concedido el permiso 
correspondiente. Aparece un mensaje indicando que el usuario "director" no tiene permiso 
"create session" por lo tanto no puede conectarse.*/

/*3- Vea los permisos de "director"*/
select * from dba_sys_privs where GRANTEE LIKE 'DIRECTOR';
/*No tiene ning�n permiso.*/

/*4- Conceda a "director" permiso para iniciar sesi�n y para crear tablas*/
GRANT CREATE SESSION, CREATE TABLE TO director;

/*5- Vea los permisos de "director"*/
SELECT PRIVILEGE from dba_sys_privs where GRANTEE LIKE 'DIRECTOR';
/*Tiene permiso "create session" y para crear tablas.<*/

/*6- Inicie una sesi�n como "director".*/

/*7- Como "administrador", elimine los usuarios "profesor" y "alumno", por si existen*/
DROP USER profesor;
DROP USER alumno;

/*8- Cree un usuario denominado "profesor", con contrase�a "maestro", asigne espacio en "system" (100M)*/
CREATE USER profesor IDENTIFIED BY maestro DEFAULT TABLESPACE system QUOTA 100M ON SYSTEM;

/*9- Cree un usuario denominado "estudiante", con contrase�a "alumno" y tablespace "system" (no asigne "quota")*/
CREATE USER estudiante IDENTIFIED BY alumno DEFAULT TABLESPACE system;

/*10- Consulte el diccionario de datos correspondiente para ver si existen los 3 usuarios creados*/
SELECT * from dba_sys_privs where GRANTEE LIKE 'DIRECTOR' 
    OR GRANTEE LIKE 'PROFESOR'
    OR GRANTEE LIKE 'ESTUDIANTE';
    
/*11- Conceda a "profesor" y a "estudiante" permiso para conectarse*/
GRANT CREATE SESSION TO profesor;
GRANT CREATE SESSION TO estudiante;

/*12- Conceda a "estudiante" permiso para crear tablas*/
GRANT CREATE TABLE TO ESTUDIANTE;

/*13- Consulte el diccionario de datos "sys_privs" para ver los permisos de los 3 usuarios creados "director" 
y "estudiante" tienen permisos para conectarse y para crear tablas, "profesor" tiene permiso para conectarse.*/
SELECT * from dba_sys_privs where GRANTEE LIKE 'DIRECTOR' 
    OR GRANTEE LIKE 'PROFESOR'
    OR GRANTEE LIKE 'ESTUDIANTE';
    
/*14- Retome su sesi�n como "director" y cree una tabla:
create table prueba( nombre varchar2(30), apellido varchar2(30) ); 
Podemos hacerlo porque "director" tiene el permiso necesario y espacio en "system".*/
create table prueba( nombre varchar2(30), apellido varchar2(30) ); 

/*15- Inicie una sesi�n como "profesor" e intente crear una tabla:
create table prueba( nombre varchar2(30), apellido varchar2(30) );
Mensaje de error "privilegios insuficientes". 
Esto sucede porque "profesor" NO tiene permiso para crear tablas.*/
create table prueba( nombre varchar2(30), apellido varchar2(30) );

/*16- Consulte los permisos de "profesor" No tiene permiso para crear 
tablas, �nicamente para crear sesi�n.*/
SELECT PRIVILEGE from dba_sys_privs where GRANTEE LIKE 'PROFESOR';

/*17- Cambie a la conexi�n de administrador y conceda a "profesor" permiso para crear tablas*/
GRANT CREATE TABLE TO profesor;

/*18- Cambie a la sesi�n de "profesor" y cree una tabla Ahora si podemos hacerlo, 
"profesor" tiene permiso "create table".*/
create table prueba( nombre varchar2(30), apellido varchar2(30) );

/*19- Consulte nuevamente los permisos de "profesor" Tiene permiso para crear tablas 
y para crear sesi�n.*/
SELECT PRIVILEGE from dba_sys_privs where GRANTEE LIKE 'PROFESOR';

/*20- Inicie una sesi�n como "estudiante" e intente crear una tabla:

create table prueba( nombre varchar2(30), apellido varchar2(30) ); 
Mensaje de error "no existen privilegios en tablespace SYSTEM". Esto sucede porque "estudiante", si bien tiene 
permiso para crear tablas, no tiene asignado espacio (recuerde que al crearlo no especificamos "quota", 
por lo tanto, por defecto es cero).*/
create table prueba( nombre varchar2(30), apellido varchar2(30) ); 

/*21- Vuelva a la conexi�n de "administrador" y consulte todas las tablas denominadas "PRUEBA" Note que hay 
una tabla propiedad de "director" y otra que pertenece a "profesor".*/
select * from sys.dba_tables where table_name LIKE 'prueba';
select * from sys.dba_objects where object_name LIKE 'prueba';

/*22- Eliminar los privilegios del usuario 'director'*/
Revoke all on Director.prueba from Director;

/*23- Da permisos a la tabla prueba del usuario Director*/
/*(desde usuario system)*/
GRANT ALL ON director.prueba TO Director with GRANT OPTION;
/*(desde usuario Director)*/
GRANT ALL ON director.prueba TO profesor;
/*(desde usuario profesor)*/
select * from director.prueba;