/*En una p�gina web se guardan los siguientes datos de las visitas: nombre, mail, pais y fecha.

1- Elimine la tabla "visitas" y cr�ela con la siguiente estructura:*/
drop table visitas;

create table visitas (nombre varchar2(30) default 'Anonimo', mail varchar2(50), pais varchar2(20), fecha date);

--3- Ingrese algunos registros:
ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI'; 
insert into visitas values ('Ana Maria Lopez','AnaMaria@hotmail.com','Argentina','10/10/2006 10:10');
insert into visitas values ('Gustavo Gonzalez','GustavoGGonzalez@hotmail.com','Chile','10/10/2006 21:30');
insert into visitas values ('Juancito','JuanJosePerez@hotmail.com','Argentina','11/10/2006 15:45');
insert into visitas values ('Fabiola Martinez','MartinezFabiola@hotmail.com','Mexico','12/10/2006 08:15');
insert into visitas values ('Fabiola Martinez','MartinezFabiola@hotmail.com','Mexico','12/09/2006 20:45');
insert into visitas values ('Juancito','JuanJosePerez@hotmail.com','Argentina','12/09/2006 16:20');
insert into visitas values ('Juancito','JuanJosePerez@hotmail.com','Argentina','15/09/2006 16:25');

--4- Ordene los registros por fecha, en orden descendente.
SELECT * from visitas ORDER by FECHA DESC;

--5- Muestre el nombre del usuario, pais y el mes, ordenado por pais (ascendente) y el mes (descendente)
SELECT NOMBRE, PAIS, to_char(to_date(FECHA,'dd/mm/yyyy hh24:mi:ss'),'MM') MES
    FROM VISITAS     
    ORDER BY PAIS ASC, MES DESC;

--6- Muestre los mail, pa�s, ordenado por pa�s, de todos los que visitaron la p�gina en octubre (4 registros)
SELECT MAIL, PAIS
    FROM VISITAS
    WHERE to_char(to_date(FECHA,'dd/mm/yyyy hh24:mi:ss'),'MM')=10
    ORDER BY PAIS;