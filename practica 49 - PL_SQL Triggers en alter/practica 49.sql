/*Una empresa almacena los datos de sus empleados en una tabla denominada "empleados" y en otra llamada "control" 
guarda un registro por cada empleado que se elimina de la tabla "empleados".

1- Elimine las tablas:*/
drop table empleados;                                                                                         
drop table control;

--2- Cree las tablas con las siguientes estructuras:
create table empleados(documento char(8),apellido varchar2(20),nombre varchar2(20),seccion varchar2(30),sueldo number(8,2));
create table control(usuario varchar2(30),fecha date);

--3- Ingrese algunos registros en "empleados":
insert into empleados values('22333444','ACOSTA','Ana','Secretaria',500);
insert into empleados values('22777888','DOMINGUEZ','Daniel','Secretaria',560);
insert into empleados values('22999000','FUENTES','Federico','Sistemas',680);
insert into empleados values('22555666','CASEROS','Carlos','Contaduria',900);
insert into empleados values('23444555','GOMEZ','Gabriela','Sistemas',1200);
insert into empleados values('23666777','JUAREZ','Juan','Contaduria',1000);

--4- Cree un disparador que se dispare cada vez que se actualice un registro en "empleados"; el trigger debe ingresar en 
--la tabla "control", el nombre del usuario y la fecha en la cual se realiz� un "update" sobre "empleados"
ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI'; 

create or replace trigger BU_empleados_control
before update on empleados
for each row
declare
begin
    insert into control values(user,sysdate);
end;

--5- Actualice varios registros de "empleados". Aumentamos en un 10% el sueldo de todos los empleados de la seccion "Secretaria'
update empleados set sueldo=sueldo+(sueldo*10/100) where seccion like 'Secretaria';
--2 filas actualizadas.

--6- Vea cu�ntas veces se dispar� el trigger consultando la tabla "control"
select * from control;
/*
SYSTEM	17/11/2017 16:07
SYSTEM	17/11/2017 16:07*/

--7- El trigger se dispar� 2 veces, una vez por cada registro modificado en "empleados". Si el trigger 
--hubiese sido creado a nivel de sentencia, el "update" anterior hubiese disparado el trigger 1 sola vez 
--a�n cuando se modifican 2 filas.
create or replace trigger BU_empleados_control
before update on empleados
declare
begin
    insert into control values(user,sysdate);
end;
update empleados set sueldo=sueldo+(sueldo*10/100) where seccion like 'Secretaria';
select * from control;
--SYSTEM	17/11/2017 16:10

--8- Vea qu� informa el diccionario "user_triggers" respecto del trigger anteriormente creado
select * from user_triggers;
--BU_EMPLEADOS_CONTROL	BEFORE EACH ROW	UPDATE	SYSTEM	TABLE	EMPLEADOS
/*obtenemos el nombre del disparador, el momento y nivel (before each row), evento que lo dispara 
(update), a qu� objeto est� asociado (table), nombre de la tabla al que est� asociado (empleados) 
y otras columnas que no analizaremos por el momento.*/
