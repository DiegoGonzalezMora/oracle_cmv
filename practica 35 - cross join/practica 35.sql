--Una agencia matrimonial almacena la informaci�n de sus clientes de sexo femenino en una tabla llamada 
--"mujeres" y en otra la de sus clientes de sexo masculino llamada "varones".
--
--1- Elimine las tablas y cr�elas con las siguientes estructuras:
drop table mujeres;
drop table varones;
create table mujeres(nombre varchar2(30), domicilio varchar2(30), edad number(2));

create table varones(nombre varchar2(30), domicilio varchar2(30), edad number(2));

--2- Ingrese los siguientes registros:
insert into mujeres values('Maria Lopez','Colon 123',45);
insert into mujeres values('Liliana Garcia','Sucre 456',35);
insert into mujeres values('Susana Lopez','Avellaneda 98',41);
insert into varones values('Juan Torres','Sarmiento 755',44);
insert into varones values('Marcelo Oliva','San Martin 874',56);
insert into varones values('Federico Pereyra','Colon 234',38);
insert into varones values('Juan Garcia','Peru 333',50);

--3- La agencia necesita la combinaci�n de todas las personas de sexo femenino con las de sexo masculino. Use un 
--"cross join" (12 filas)
select m.*, v.*
    from mujeres m cross join varones v;

--4- Realice la misma combinaci�n pero considerando solamente las personas mayores de 40 a�os (6 filas)
select m.*, v.*
    from mujeres m cross join varones v
    where m.edad>40 and v.EDAD>40;

--5- Forme las parejas pero teniendo en cuenta que no tengan una diferencia superior a 10 a�os (8 filas)
select m.*, v.*, abs(m.edad - v.EDAD) as diferencia
    from mujeres m cross join varones v
    where abs((m.edad - v.EDAD))<=10;