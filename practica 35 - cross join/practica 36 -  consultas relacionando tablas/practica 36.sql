/*creacion de tablas*/
drop table antiguedades;
drop table compras;
drop table anticuarios;
drop table precios;

create table anticuarios(id_anticuario number(2), apellidos varchar(30), nombre varchar(20));
alter table anticuarios add constraint PK_id_anticuario primary key(id_anticuario);

create table precios(objeto varchar(30), precio number(3));
alter table precios add constraint PK_objeto primary key(objeto);

create table compras(id_anticuario number(2), objeto varchar(30));
ALTER TABLE compras ADD constraint PK_anticuarios_objeto PRIMARY KEY(id_anticuario, objeto);
alter table compras add constraint FK_anticuarios_Objetos foreign key(id_anticuario) references anticuarios(id_anticuario);
alter table compras add constraint FK_Precios_Objetos foreign key(objeto) references precios(objeto);

create table antiguedades(id_vendedor number(2), id_comprador number(2), objeto varchar(30));
ALTER TABLE antiguedades ADD constraint PK_Vendedor_comprador_objeto PRIMARY KEY(id_vendedor, id_comprador, objeto);
alter table antiguedades add constraint FK_Anticuarios_id_anticuario foreign key(id_vendedor) references anticuarios(id_anticuario);
alter table antiguedades add constraint FK_Anticuarios_id_comprador foreign key(id_comprador) references anticuarios(id_anticuario);
alter table antiguedades add constraint FK_Precios_Objetos2 foreign key(objeto) references precios(objeto);

/*insertar valores*/
insert into anticuarios values(01, 'Jones', 'Bill');
insert into anticuarios values(02, 'Smith', 'Bob');
insert into anticuarios values(15, 'Lawson', 'Patricia');
insert into anticuarios values(21, 'Akins', 'Jane');
insert into anticuarios values(50, 'Fowler', 'Sam');
select * from anticuarios;

insert into precios values('Cama', 24);
insert into precios values('Mesa', 150);
insert into precios values('Silla', 39);
insert into precios values('Espejo', 48);
insert into precios values('Escritorio', 301);
insert into precios values('Vitrina', 120);
insert into precios values('Mesa de Caf�', 90);
insert into precios values('Joyero', 30);
insert into precios values('Jarr�n', 54);
insert into precios values('Librer�a', 210);
insert into precios values('Perchero', 45);
select * from precios;

insert into compras values(02, 'Mesa');
insert into compras values(02, 'Escritorio');
insert into compras values(21, 'Silla');
insert into compras values(15, 'Espejo');
select * from compras;

insert into antiguedades values(01, 50, 'Cama');
insert into antiguedades values(02, 15, 'Mesa');
insert into antiguedades values(15, 02, 'Silla');
insert into antiguedades values(21, 50, 'Espejo');
insert into antiguedades values(50, 01, 'Escritorio');
insert into antiguedades values(01, 21, 'Vitrina');
insert into antiguedades values(02, 21, 'Mesa de caf�');
insert into antiguedades values(15, 50, 'Silla');
insert into antiguedades values(01, 15, 'Joyero');
insert into antiguedades values(02, 21, 'Jarr�n');
insert into antiguedades values(21, 02, 'Librer�a');
insert into antiguedades values(50, 01, 'Perchero');
select * from antiguedades;

--Realiza las siguientes consultas apoy�ndote en las anteriores tablas:

--1.-Selecciona el nombre del anticuario que desea comprar una Mesa
select a.nombre 
    from anticuarios a join compras c on (a.Id_Anticuario=c.Id_Anticuario)
    where objeto='Mesa';
    
--2.- Selecciona que objeto quiere comprar el anticuario Jane
select c.objeto
    from ANTICUARIOS a join compras c on (a.ID_ANTICUARIO=c.Id_Anticuario)
    where a.NOMBRE='Jane';
    
--3.- �Cu�nto vale el objeto que quiere comprar Jane?
select distinct pre.precio
    from precios pre join antiguedades an on (pre.objeto=an.OBJETO)
        join compras c on (an.OBJETO=c.OBJETO)    
        join anticuarios a on (c.ID_ANTICUARIO=a.ID_ANTICUARIO)
    where a.NOMBRE='Jane';
    
select objeto, precio 
    from precios where objeto=(select objeto 
                                from compras 
                                where id_anticuario=(select id_anticuario 
                                                        from anticuarios 
                                                        where nombre='Jane')
                                );

--4.- �Cu�nto se quiere gastar el anticuario llamado Bob? 451
select sum(p.precio) as Gasto_bob
    from precios p join antiguedades a on (p.objeto=a.objeto)
        join compras c on (a.OBJETO=c.OBJETO)   
        join anticuarios an on (c.ID_ANTICUARIO=an.ID_ANTICUARIO)
    where an.NOMBRE='Bob'; 

--5.- �Qu� Anticuario le compr� una cama a Bill?
select a.*
    from anticuarios a join antiguedades an on(a.ID_ANTICUARIO=an.id_comprador)
        join antiguedades an2 on (an2.id_vendedor=a.ID_ANTICUARIO and a.NOMBRE='Bill' and an2.OBJETO='Cama');
        
select a.*
    from anticuarios a join antiguedades an on(a.ID_ANTICUARIO=an.id_comprador)
    where an.OBJETO='Cama'
            and id_vendedor=(select a.id_anticuario 
                                from anticuarios a
                                where a.NOMBRE='Bill'
                                );


--6.- �Cu�ntos dineros ha ganado Bill por los objetos que ha vendido?
select sum(distinct p.precio) as ganado
    from precios p, antiguedades a, anticuarios an
    where an.NOMBRE='Bill' and a.ID_VENDEDOR=an.ID_ANTICUARIO and a.objeto=p.OBJETO;
    
    
select sum(distinct p.precio) as ganado
    from precios p join  antiguedades a on(a.objeto=p.OBJETO) 
        join anticuarios an on(an.NOMBRE='Bill' and a.ID_VENDEDOR=an.ID_ANTICUARIO);

--7.- �Qu� cantidad de objetos distintos ha comprado Jane?
select count(an.objeto) as num_objetos
    from anticuarios a join antiguedades an on (a.ID_ANTICUARIO=an.ID_COMPRADOR)
    where a.nombre='Jane';
    
--8.- �Cu�ntos objetos distintos ha comprado Jane de m�s de 120 �?
select count(an.OBJETO) num_objetos 
    from anticuarios a join antiguedades an on (a.ID_ANTICUARIO=an.ID_COMPRADOR)
        join precios p on (an.OBJETO=p.objeto)
    where a.nombre='Jane' and p.precio>120;

--9.- �Cu�l es el objeto m�s caro vendido por Patricia?
select an.OBJETO
    from anticuarios a join antiguedades an on (a.ID_ANTICUARIO=an.ID_COMPRADOR)
        join precios p on (an.OBJETO=p.objeto)
    where a.nombre='Patricia' and max(p.precio);

--10.- Nombre de los anticuarios que han vendido objetos cuyo nombre empiece por e
select a.nombre
    from anticuarios a, antiguedades an
    where a.nombre like 'E%' and a.ID_ANTICUARIO=an.ID_VENDEDOR;

--11.- �Qu� anticuarios desean comprar un objeto que han vendido previamente?
select a.* 
    from anticuarios a join compras c on (a.Id_Anticuario=c.Id_Anticuario)
        join antiguedades an on (a.ID_ANTICUARIO=an.ID_VENDEDOR)
        where c.OBJETO=an.OBJETO;

--12.- Dinero que se ha gastado Sam en objetos que valen menos de 60 euros.
select distinct sum(p.precio)
    from anticuarios a, antiguedades an, precios p
    where a.nombre='Sam' and p.precio<60 and a.id_anticuario=an.id_comprador and an.objeto=p.objeto;

--13.-Nombre de aquellos anticuarios que poseen un objeto que alguien quiere comprar
select distinct a.nombre
    from anticuarios a join antiguedades an on(a.ID_ANTICUARIO=an.ID_VENDEDOR);

--14.- �Cu�nto dinero ha ganado Bob en los objetos que ha vendido al anticuario 21?
select sum(p.precio)
    from anticuarios a, antiguedades an, precios p
    where a.nombre='Bob' and an.id_comprador=21 and an.objeto=p.objeto;

--15.- �Cu�nto ha ganado Patricia por vender sillas?
select sum(p.precio)
    from anticuarios a, antiguedades an, precios p
    where a.nombre='Patricia' and p.OBJETO='Silla' and an.ID_VENDEDOR=a.id_anticuario;