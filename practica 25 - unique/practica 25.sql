/*Una empresa de remises tiene registrada la información de sus vehículos en una tabla llamada "remis".
1- Elimine la tabla:*/
drop table remis;
--2- Cree la tabla con la siguiente estructura:
create table remis( numero number(5), patente char(6), marca varchar2(15), modelo char(4) ); 

--3- Ingrese algunos registros, 2 de ellos con patente repetida y alguno con patente nula.
INSERT INTO REMIS VALUES ('00001', '895314', 'SEAT', 'LEON');
INSERT INTO REMIS VALUES ('00002', '872247', 'SEAT', 'IBIZ');
INSERT INTO REMIS VALUES ('00003', '872247', 'SEAT', 'MALA');

--4- Agregue una restricción "primary key" para el campo "numero".
ALTER TABLE REMIS ADD CONSTRAINT PK_REMIS_NUMERO PRIMARY KEY(NUMERO);

--5- Intente agregar una restricción "unique" para asegurarse que la patente del remis no tomará valores repetidos.
ALTER TABLE REMIS ADD CONSTRAINT UQ_REMIS_PATENTE UNIQUE(PATENTE);
--No se puede porque hay valores duplicados, un mensaje indica que se encontraron claves duplicadas.

--6- Elimine el registro con patente duplicada y establezca la restricción.
UPDATE REMIS SET PATENTE=NULL WHERE NUMERO=00003;
ALTER TABLE REMIS ADD CONSTRAINT UQ_REMIS_PATENTE UNIQUE(PATENTE);
--Note que hay 1 registro con valor nulo en "patente".
SELECT * FROM REMIS;

--7- Intente ingresar un registro con patente repetida (no lo permite)
INSERT INTO REMIS VALUES('00004', '872247', 'SKODA', 'FAB');
--ORA-00001: unique constraint (SYSTEM.UQ_REMIS_PATENTE) violated

--8- Ingrese un registro con valor nulo para el campo "patente".
INSERT INTO REMIS VALUES('00004', NULL, 'SKODA', 'FAB');

--9- Muestre la información de las restricciones consultando "user_constraints" y "user_cons_columns" y analice 
--la información retornada (2 filas en cada consulta)
SELECT * FROM USER_CONSTRAINTS WHERE TABLE_NAME='REMIS';
SELECT * FROM USER_CONS_COLUMNS WHERE TABLE_NAME='REMIS';