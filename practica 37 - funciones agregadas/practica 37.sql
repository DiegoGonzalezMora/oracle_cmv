drop table personas;
drop table accionistas;
drop table empresas;
drop table actividades;

/*------------------*/

create table personas (dni varchar2(8), nombre varchar2(10));
alter table personas add constraint pk_personas primary key(dni);
insert into personas values('25000111','LOPEZ');
insert into personas values('28123765','RUIZ'); 
insert into personas values('31526778','PLA');
insert into personas values('42556201','GOMEZ');

/*------------------*/

create table actividades (actividad varchar2(15),tipo char(1),ayuda number(2,0) default 0);
alter table actividades add constraint pk_actvidades primary key(actividad);
insert into actividades values ('Textil','C',10); 
insert into actividades values ('Motor','B',5);
insert into actividades values ('Papel','B',5); 
insert into actividades values ('Electr�nica','D',15);
insert into actividades values ('Calzado','C',10);

/*------------------*/

create table empresas(empresa varchar2(10),factura int, actividad varchar2(15)); 
alter table empresas add constraint pk_empresa primary key (empresa);
alter table empresas add constraint fk_empresa_actv FOREIGN key (actividad) references actividades (actividad);
insert into empresas values ('LOCK','10000','Textil'); 
insert into empresas values ('KMB','24000','Motor'); 
insert into empresas values ('ALPHA','34000','Motor');
insert into empresas values ('PATH','10000','Electr�nica'); 

/*------------------*/

create table accionistas (dni varchar2(8), nombre varchar2(10),acciones number(3),empresa VARCHAR2(10));
alter table accionistas add constraint pk_accionistas primary key (dni,empresa);
alter table accionistas add constraint fk_accionistas_emp foreign key (empresa) references empresas(empresa);
insert into accionistas values ('25000111','LOPEZ',200,'LOCK');
insert into accionistas values ('25000111','LOPEZ',500,'ALPHA');
insert into accionistas values ('25000111','LOPEZ',750,'KMB');
insert into accionistas values ('28123765','RUIZ',100,'LOCK');
insert into accionistas values ('28123765','RUIZ',30,'PATH');
insert into accionistas values ('31526778','PLA',100,'KMB');
insert into accionistas values ('31526778','PLA',200,'ALPHA');
insert into accionistas values ('31526778','PLA',250,'PATH'); 
insert into accionistas values ('42556201','GOMEZ',200,'ALPHA');

--1. Obtener los nombres de todas las empresas.
select empresa from empresas;

--2. Obtener todas las actividades.
select * from actividades;

--3. Obtener el DNI de los accionistas sin repetir.
select distinct dni from accionistas;

--4. Obtener las ayudas de las actividades.
select ayuda from actividades;

--5. Obtener la actividad y ayuda de cada actividad
select actividad, ayuda from actividades;

--6. Obtener la actividad y facturaci�n de cada empresa
select * from empresas join actividades on(empresas.actividad=actividades.actividad);

--7. Obtener todos los registros de accionistas
select * from accionistas;

--8. Obtener todos los registros de empresas
select * from empresas;

--9. Obtener todos los nombres de empresas que facturan m�s de 15000
select empresa
    from empresas
    where factura>15000;

--10. Obtener el DNI, nombre del accionista y n�mero de acciones de los accionistas de la empresa KMB
select a.dni, a.nombre, count(a.acciones) num_acciones
    from  accionistas a join empresas e on(a.empresa=e.empresa)
    where e.empresa='KMB'
    group by a.dni, a.nombre;
    
--11. Obtener las actividades y ayudas de tipo C
select actividad, ayuda from actividades where tipo='C';

--12. Obtener las empresas de MOTOR que facturan menos de 30000
select e.*
    from empresas e join actividades a on (e.actividad=a.actividad)
    where a.actividad='Motor' and e.factura<30000;    

--13. Obtener los nombres de los accionistas que tienen entre 100 y 200 acciones
select nombre
    from accionistas
    where acciones between 100 and 200;

--14. Obtener el DNI de los accionistas con acciones en empresas que facturen m�s de 15000
select distinct a.dni
    from  accionistas a join empresas e on(a.empresa=e.empresa)
    where e.factura>15000;

--15. Obtener el nombre de los accionistas con acciones en empresas que no sean de MOTOR
select distinct a.nombre
    from  accionistas a join empresas e on(a.empresa=e.empresa)
        join actividades ac on (e.actividad=ac.actividad)
    where ac.actividad not like 'Motor';

--16. Igual que el 15 pero ordenadas alfab�ticamente
select distinct a.nombre
    from  accionistas a join empresas e on(a.empresa=e.empresa)
        join actividades ac on (e.actividad=ac.actividad)
    where ac.actividad not like 'Motor'
    order by a.nombre;

--17. Obtener el DNI y la actividad de la empresa para cada accionista.
select a.dni, ac.actividad
    from accionistas a join empresas e on(a.empresa=e.empresa)
        join actividades ac on(e.actividad=ac.actividad);

--18. Obtener el nombre de la empresa, la facturaci�n y la ayuda para cada una de ellas.
select e.empresa, e.factura, a.ayuda
    from empresas e join actividades a on(e.actividad=a.actividad);

--19. Igual que el 18 pero para las empresas con facturaci�n entre 15000 y 25000 y adem�s cuya actividad sea de tipo B
select e.empresa, e.factura, a.ayuda
    from empresas e join actividades a on(e.actividad=a.actividad)
    where e.FACTURA between 15000 and 25000 and a.tipo='B';
    
    
--20. Obtener el nombre de los accionistas, de la empresa, su facturaci�n y tipo
select a.nombre, e.empresa, e.factura, ac.tipo
    from accionistas a join empresas e on (a.empresa=e.empresa)
        join actividades ac on (e.actividad=ac.actividad);

--21. Obtener el nombre de los accionistas que no tienen ninguna acci�n en empresas de MOTOR
select a.nombre, e.empresa
    from accionistas a join empresas e on (a.empresa=e.empresa)
        where e.actividad='Motor'
        and a.acciones=0;

--22. Obtener la suma de todas las acciones
select sum(acciones) from accionistas;

--23. Obtener para cada empresa la suma de las acciones
select e.empresa, sum(a.acciones) acciones
    from accionistas a join empresas e on(a.empresa=e.empresa)
    group by e.empresa;

--24. Obtener para cada empresa la suma de sus acciones ,la media de sus acciones y el n�mero de accionistas
select e.empresa, sum(a.acciones) acciones, avg(a.acciones) media_acciones, count(dni) num_accionistas
    from accionistas a join empresas e on(a.empresa=e.empresa)
    group by e.empresa;

--25. Igual que el 24 pero para las empresas con m�s de 2 accionistas
select e.empresa, sum(a.acciones) acciones, avg(a.acciones) media_acciones, count(dni) num_accionistas
    from accionistas a join empresas e on(a.empresa=e.empresa)
    group by e.empresa
    having count(a.dni)>2;

--26. Igual que el 24 pero para las empresas de MOTOR con un solo accionista
select e.empresa, sum(a.acciones) acciones, avg(a.acciones) media_acciones, count(dni) num_accionistas
    from accionistas a join empresas e on(a.empresa=e.empresa)
    where e.ACTIVIDAD='Motor'
    group by e.empresa
    having count(a.dni)=1;

--27. Obtener el nombre de los accionistas que tienen acciones en las empresas de mayor facturaci�n
select a.nombre, MAX(e.factura)
    from accionistas a join empresas e on(a.empresa=e.empresa)
    group by a.nombre, e.factura
    having e.factura=(select max(e2.factura) from empresas e2);
    
--28. Obtener el DNI de los accionistas que tienen acciones en todas las empresas 
select dni
    from accionistas
    where empresa = all (select empresa from empresas);

--29. Obtener las empresas y su actividad de las que tienen menos de 1000 acciones
select e.empresa, e.actividad, sum(a.acciones) as suma
    from empresas e join accionistas a on (e.empresa=a.empresa)
    group by e.empresa, e.actividad
    having sum(a.acciones)<1000;

--30. Obtener las empresas y su ayuda para las empresas que tienen m�s de 100 acciones
select e.empresa, ac.ayuda
    from empresas e join accionistas a on (e.empresa=a.empresa)
        join actividades ac on (e.actividad=ac.actividad)
    group by e.empresa, ac.ayuda
    having sum(a.acciones)>100;

--31. Obtener la ayuda y la actividad para las actividades que no tienen m�s de 3 empresas
select ac.ayuda, ac.actividad
    from empresas e right join actividades ac on (e.actividad=ac.actividad)
    group by ac.ayuda, ac.actividad
    having count(*)<=3;

--32. Selecciona los nombres de los accionistas que tienen en su nombre la letra Z
select distinct nombre
    from accionistas 
    where nombre like '%Z%';
    
--33. Nombre de los accionistas que tienen acciones en todas las empresas
select nombre
    from accionistas
    where empresa = all (select empresa from empresas);

--34.A�adir a la tabla accionista la columna nacionalidad
alter table accionistas add nacionalidad varchar2(20);