/*Una librer�a almacena los datos de sus libros en una tabla denominada "libros" y en una tabla "ofertas", 
algunos datos de los libros cuyo precio no supera los 30 euros. Adem�s, controla las inserciones que los 
empleados realizan sobre "ofertas", almacenando en la tabla "control" el nombre del usuario, la fecha y hora, 
cada vez que se ingresa un nuevo registro en la tabla "ofertas".

1- Elimine las tres tablas:*/
drop table libros;
drop table ofertas;
drop table control;

--2- Cree las tablas con las siguientes estructuras:
create table libros(codigo number(6),titulo varchar2(40),autor varchar2(30),editorial varchar2(20),precio number(6,2));
create table ofertas(titulo varchar2(40),autor varchar2(30),precio number(6,2));
create table control(usuario varchar2(30),fecha date);

--3- Establezca el formato de fecha para que muestre "DD/MM/YYYY HH24:MI":
ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI'; 

--4- Cree un disparador que se dispare cuando se ingrese un nuevo registro en "ofertas"; el trigger debe ingresar en 
--la tabla "control", el nombre del usuario, la fecha y la hora en la cual se realiz� un "insert" sobre "ofertas"
create or replace trigger AC_ofertas_control
after insert on ofertas
for each row
declare
begin
    insert into control values(USER, sysdate());
end;

begin
    insert into ofertas values('crepusculo', 'baboso', '-5');
end;
select * from control;
--SYSTEM	16/11/2017 20:04

--5- Vea qu� informa el diccionario "user_triggers" respecto del trigger anteriormente creado
select * from user_triggers;
/*
AC_OFERTAS_CONTROL	AFTER EACH ROW	INSERT	SYSTEM	TABLE	OFERTAS		REFERENCING NEW AS NEW OLD AS OLD		ENABLED
REPCATLOGTRIG	AFTER STATEMENT	UPDATE OR DELETE	SYSTEM	TABLE	REPCAT$_REPCATLOG		REFERENCING NEW AS NEW OLD AS OLD		ENABLED
DEF$_PROPAGATOR_TRIG	BEFORE STATEMENT	INSERT	SYSTEM	TABLE	DEF$_PROPAGATOR		REFERENCING NEW AS NEW OLD AS OLD		ENABLED
*/

--6- Ingrese algunos registros en "libros"
insert into libros values(100,'Uno','Richard Bach','Planeta',25); 
insert into libros values(102,'Matematica estas ahi','Paenza','Nuevo siglo',12); 
insert into libros values(105,'El aleph','Borges','Emece',32); 
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);

--7- Ingrese en "ofertas" los libros de "libros" cuyo precio no superen los 30 euros, utilizando la siguiente sentencia:
insert into ofertas select titulo,autor,precio from libros where precio<30;

--8- Verifique que el trigger se dispar� consultando la tabla "control"
select * from control;
/*
SYSTEM	16/11/2017 20:04
SYSTEM	16/11/2017 20:08
SYSTEM	16/11/2017 20:08
*/