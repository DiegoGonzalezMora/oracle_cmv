/*Trabaje con la tabla "agenda" que almacena informaci�n de sus amigos.

1- Elimine la tabla "agenda"*/
DROP TABLE AGENDA;

/*2- Cree una tabla llamada "agenda". Debe tener los siguientes campos: apellido 
(cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11)*/
create table agenda( apellido varchar2(30), nombre varchar2(20), domicilio varchar2(30), telefono varchar2(11) );

/*3- Visualice las tablas existentes para verificar la creaci�n de "agenda" (all_tables)*/
SELECT * FROM ALL_TABLES;

/*4- Visualice la estructura de la tabla "agenda" (describe)*/
DESCRIBE AGENDA;

/*5- Ingrese los siguientes registros:
insert into agenda (apellido, nombre, domicilio, telefono) values 
('Moreno','Alberto','Colon 123','4234567'); insert into agenda (apellido,nombre, domicilio, telefono) 
values ('Torres','Juan','Avellaneda 135','4458787'); */
insert into agenda (apellido, nombre, domicilio, telefono) values 
('Moreno','Alberto','Colon 123','4234567'); insert into agenda (apellido,nombre, domicilio, telefono) 
values ('Torres','Juan','Avellaneda 135','4458787');

/*6- Seleccione todos los registros de la tabla.*/
SELECT * FROM AGENDA;

/*7- Elimine la tabla "agenda"*/
DROP TABLE AGENDA;

/*8- Intente eliminar la tabla nuevamente (aparece un mensaje de error)*/
DROP TABLE AGENDA;