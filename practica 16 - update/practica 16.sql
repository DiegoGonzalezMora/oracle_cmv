--Trabaje con la tabla "agenda" que almacena los datos de sus amigos.
--1- Elimine la tabla y cr�ela con la siguiente estructura:
drop table agenda; 
create table agenda( apellido varchar2(30), nombre varchar2(20), domicilio varchar2(30), telefono varchar2(11) ); 

--3- Ingrese los siguientes registros:
insert into agenda (apellido,nombre,domicilio,telefono) values ('Acosta','Alberto','Colon 123','4234567'); 
insert into agenda (apellido,nombre,domicilio,telefono) values ('Juarez','Juan','Avellaneda 135','4458787'); 
insert into agenda (apellido,nombre,domicilio,telefono) values ('Lopez','Maria','Urquiza 333','4545454'); 
insert into agenda (apellido,nombre,domicilio,telefono) values ('Lopez','Jose','Urquiza 333','4545454'); 
insert into agenda (apellido,nombre,domicilio,telefono) values ('Suarez','Susana','Gral. Paz 1234','4123456');

--4- Modifique el registro cuyo nombre sea "Juan" por "Juan Jose" (1 registro actualizado)
update agenda set nombre='Juan Jose' where nombre='Juan';

--5- Actualice los registros cuyo n�mero telef�nico sea igual a "4545454" por "4445566" (2 registros)
update agenda set telefono='4445566' where telefono='4545454';

--6- Actualice los registros que tengan en el campo "nombre" el valor "Juan" por "Juan Jose" 
--(ning�n registro afectado porque ninguno cumple con la condici�n del "where")
update agenda set nombre='Juan Jose' where nombre='Juan';
