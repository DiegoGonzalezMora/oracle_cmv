drop table ventas;
create table ventas(
id_ventas number(3),
total_ventas number(10),
fecha_ve date,
Cli_ID_cliente number(3),
primary key (id_ventas)
);

insert into ventas values(1, 2500, '02/02/15', 100);
insert into ventas values(2,	5000,	'10/01/15',	150);
insert into ventas values(3,	9500,	'05/01/15',	160);
insert into ventas values(4,    2350,	'12/01/15',	200);
insert into ventas values(5,	3750,	'15/01/15',	210);
insert into ventas values(6,	1250,	'18/01/03',	150);
insert into ventas values(7,	3500,	'04/01/15',	160);
insert into ventas values(8,	7500,	'04/01/15',	210);
insert into ventas values(9,	2900,	'10/12/02',	100);
insert into ventas values(10,	7000,	'11/01/15',	210);

/*1.	Partiendo de los datos de la tabla ventas existentes en la imagen, deseamos saber si la suma de las 
ventas del cliente cuyo id_cliente es 100,superan los 5000 euros.Como resultado debemos mostrar el mensaje 
�Total de compras superior a 5000 euros� o �Total de compras inferior a 5000 euros�.*/
set SERVEROUTPUT ON;

SELECT SUM(total_ventas) FROM VENTAS where Cli_ID_cliente=100;

DECLARE
    v_cliente number(3):=100;
    v_total number(4);
    err_num number;
    err_msg varchar2(255);
    
BEGIN
    SELECT SUM(total_ventas) into v_total FROM VENTAS where Cli_ID_cliente=v_cliente;
    
    IF v_total> 5000 then
        dbms_output.put_line('Total de compras superior a 5000 euros');
    else
        dbms_output.put_line('Total de compras inferior a 5000 euros');
    end if;
    
    exception
        WHEN OTHERS THEN
            err_num := SQLCODE;
            err_msg := SQLERRM;
            dbms_output.put_line('Se ha producido un error');
            dbms_output.put_line('Error:' || to_char(err_num));
            dbms_output.put_line(err_msg);
END;


--2.	Controla el error en la tabla ventas cuando insertemos un registro con el Id_Venta =10.
select * from ventas;
declare
    VI_ERROR exception;
begin
    insert into ventas values(14,	200,	'12/10/2002',	400);
    raise VI_ERROR;
    
    exception
        WHEN VI_ERROR THEN
            dbms_output.put_line('Se ha producido un error');
        
        when dup_val_on_index then
            dbms_output.put_line('Se ha incumplido la clave �nica.');
            
        when others then
            dbms_output.put_line('Error generico');
end;

--3.	Usa las funciones SQLCODE y SQLERRM  
DECLARE
    err_num number;
    err_msg varchar2(255);
    result number;
    
BEGIN
    SELECT 1/0 into result from dual;
    
    exception
        WHEN zero_divide THEN
            err_num := SQLCODE;
            err_msg := SQLERRM;
            dbms_output.put_line('Error:' || to_char(err_num));
            dbms_output.put_line(err_msg);
END;

--4.	Usa la funci�n predefinida RAISE_APPLICATION_ERROR para crear nuestros propios mensajes de error.
DECLARE
    numero integer := 666;

BEGIN
    IF (numero = 666) THEN
        Raise_Application_Error (-20666, 'Este es un error diabolico.');
    END IF;
END;

--5.	Supongamos que tenemos una tabla llamada PARES que queremos rellenar con todos los n�meros pares desde el 2 al 10000.
drop table PARES;
create table PARES(valor number(5));

declare
begin
    for i in 2..10000
    loop
        if mod(i,2)=0 then
            insert into PARES values(i);
        end if;
    end loop;
end;

select * from pares;

--6.	Tabla Clientes:
drop table clientes;
create table clientes(
id_clientes number(3),
Descrip_cliente varchar2(30),
fecha date,
primary key (id_clientes)
);

insert into clientes values(100,	'Juan Martos Ga�lan',	'02/01/15');
insert into clientes values(150,	'La papeler�a s.a',	'18/01/15');
insert into clientes values(160,	'Librer�a T�cnica s.l',	'05/01/03');
insert into clientes values(200,	'Mar�a Garc�a Acosta', 	'12/01/15');
insert into clientes values(210,	'Jos� Mart�n Rubio',	'15/01/15');

--a)Usando el operador ROWTYPE calcula el id_cliente y la descripci�n de cliente para el id_cliente 200
declare
cliente clientes%ROWTYPE;
begin
    select * into cliente from clientes where id_clientes=200;
    --select id_clientes, Descrip_cliente into cliente.id_clientes, cliente.descrip_cliente from clientes where id_clientes=200;
    
    dbms_output.put_line(cliente.descrip_cliente||' - '||to_char(cliente.id_clientes));
end;

--b)Almacena en una tabla llamada vi_matriz_cliente en el �ndice 160 el registro correspondiente al cliente 160

declare 
type vi_matriz_cliente is table of clientes%ROWTYPE index by binary_integer;
tmatriz vi_matriz_cliente;

begin
    select * into tmatriz(160) from clientes where id_clientes=160;
    
    dbms_output.put_line(tmatriz(160).id_clientes||' '||tmatriz(160).Descrip_cliente||' '||tmatriz(160).fecha);
end;

/*----------*/

drop table vi_matriz_cliente;
create table vi_matriz_cliente(
indice number(3),
id_clientes number(3),
Descrip_cliente varchar2(30),
fecha date,
primary key (indice)
);

declare 
cliente clientes%ROWTYPE;
begin
    select * into cliente from clientes where id_clientes=160;
    insert into vi_matriz_cliente values(160, cliente.id_clientes, cliente.Descrip_cliente, cliente.fecha);
end;

select * from vi_matriz_cliente;
