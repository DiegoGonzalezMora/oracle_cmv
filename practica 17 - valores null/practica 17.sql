--1- Elimine la tabla y cr�ela con la siguiente estructura:
drop table medicamentos; 
create table medicamentos( codigo number(5) not null, nombre varchar2(20) not null, 
laboratorio varchar2(20), precio number(5,2), cantidad number(3,0) not null ); 

--3- Visualice la estructura de la tabla "medicamentos"note que los campos "codigo", "nombre" 
--y "cantidad", en la columna "Null" muestra "NOT NULL".
describe medicamentos;

--4- Ingrese algunos registros con valores "null" para los campos que lo admitan:
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(1,'Sertal gotas',null,null,100); 
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(2,'Sertal compuesto',null,8.90,150); 
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(3,'Buscapina','Roche',null,200); 

--5- Vea todos los registros.
select * from medicamentos;

--6- Ingrese un registro con valor "0" para el precio y cadena vac�a para el laboratorio.
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(1,'aspirina','',0,100); 

--7- Intente ingresar un registro con cadena vac�a para el nombre (mensaje de error)
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(1,'','',5,150); 

--8- Intente ingresar un registro con valor nulo para un campo que no lo admite (aparece un mensaje de error)
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(1,null,'',5,150); 

--9- Ingrese un registro con una cadena de 1 espacio para el laboratorio.
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(1,'nolotil',' ',5,150);

--10- Recupere los registros cuyo laboratorio contenga 1 espacio (1 registro)
select * from medicamentos where laboratorio like ' ';

--11- Recupere los registros cuyo laboratorio sea distinto de ' '(cadena de 1 espacio) (1 registro)
select * from medicamentos where laboratorio not like ' ';


