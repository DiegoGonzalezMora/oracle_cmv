/*Una empresa registra los datos de sus empleados en una tabla llamada "empleados".

1- Elimine la tabla "empleados":*/
drop table empleados;

--2- Cree la tabla:
create table empleados(legajo number(3),documento char(8) not null,nombre varchar2(30) not null,primary key(legajo));

--3- Elimine la secuencia "sec_legajoempleados" y luego cr�ela estableciendo el valor m�nimo (1), 
--m�ximo (210), valor inicial (206), valor de incremento (2) y no circular. Finalmente inicialice la secuencia.
drop sequence sec_legajoempleados;
create sequence sec_legajoempleados start with 206 minvalue 1 maxvalue 210 increment by 2 nocycle;

--4- Ingrese algunos registros, empleando la secuencia creada para los valores de la clave primaria.
insert into empleados values (sec_legajoempleados.currval,'22333444','Ana Acosta');
insert into empleados values (sec_legajoempleados.nextval,'23444555','Betina Bustamante');
insert into empleados values (sec_legajoempleados.nextval,'24555666','Carlos Caseros');

--5- Recupere los registros de "libros" para ver los valores de clave primaria.
select TABLE_NAME, COLUMN_NAME from user_cons_columns where table_name='EMPLEADOS';

--select uc.table_name, column_name from user_cons_columns ucc join user_constraints uc 
--    on ucc.constraint_name=uc.constraint_name where uc.constraint_type='P' and uc.table_name='EMPLEADOS';

--6- Vea el valor actual de la secuencia empleando la tabla "dual"
select sec_legajoempleados.currval from dual;
--Da 208 pero segun el ejercicio deberia dar 210 asi que habria que meter un valor extra

--7- Intente ingresar un registro empleando "nextval":
insert into empleados values (sec_legajoempleados.nextval,'25666777','Diana Dominguez');
--Oracle muestra un mensaje de error indicando que la secuencia ha llegado a su valor m�ximo.
/*
Error que empieza en la l�nea: 27 del comando :
insert into empleados values (sec_legajoempleados.nextval,'25666777','Diana Dominguez')
Informe de error -
ORA-08004: sequence SEC_LEGAJOEMPLEADOS.NEXTVAL exceeds MAXVALUE and cannot be instantiated
*/

--8- Altere la secuencia modificando el atributo "maxvalue" a 999.
alter sequence sec_legajoempleados maxvalue 999;

--9- Obtenga informaci�n de la secuencia.
select * from dba_sequences where SEQUENCE_NAME='SEC_LEGAJOEMPLEADOS';
--SYSTEM	SEC_LEGAJOEMPLEADOS	1	999	2	N	N	20	180

--10- Ingrese el registro del punto 7.
insert into empleados values (sec_legajoempleados.nextval,'25666777','Diana Dominguez');

--11- Recupere los registros.
select * from empleados;

--12- Modifique la secuencia para que sus valores se incrementen en 1.
alter sequence sec_legajoempleados increment by 1;

--13- Ingrese un nuevo registro:
insert into empleados values (sec_legajoempleados.nextval,'26777888','Federico Fuentes');

--14- Recupere los registros.
select * from empleados;

--15- Elimine la secuencia creada.
drop sequence sec_legajoempleados;

--16- Consulte todos los objetos de la base de datos que sean secuencias y verifique que "sec_legajoempleados" ya no existe.
select * from dba_sequences where SEQUENCE_NAME='SEC_LEGAJOEMPLEADOS';