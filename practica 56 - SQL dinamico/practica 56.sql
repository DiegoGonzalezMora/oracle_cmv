--1. Usando una funci�n actualiza el nombre de los empleados del departamento 100 haciendo uso de los comandos SQL din�micos.
create or replace FUNCTION fn_actualiza (nombre VARCHAR2, codigo NUMBER) RETURN Number
IS
    result NUMBER;
BEGIN
    EXECUTE IMMEDIATE 'UPDATE empleados SET NOMBRE =' ||nombre|| 
    'WHERE departamento =' ||codigo; 
    
    EXCEPTION 
        WHEN NO_DATA_FOUND THEN
          return 0;
END;

DECLARE
  Valor NUMBER;
BEGIN
	Valor := fn_actualiza('Manolo',100);
END; 

--2. Parametriza una sentencia SQL din�mica a trav�s de variables host y haciendo uso de una funci�n que actualice 
--el nombre de los empleados del departamento que le pasemos por par�metro.
DECLARE
ret NUMBER;
FUNCTION fn_execute (nombre VARCHAR2, codigo NUMBER) RETURN NUMBER 
IS
    sql_str VARCHAR2(1000);
BEGIN
    sql_str := 'UPDATE empleados SET NOMBRE = :new_nombre 
    WHERE departamento = :codigo'; 
    EXECUTE IMMEDIATE sql_str USING nombre, codigo; 
    RETURN SQL%ROWCOUNT;
END fn_execute ;

BEGIN
ret := fn_execute('Devjoker',120);
dbms_output.put_line(TO_CHAR(ret));
END;

select * from empleados;