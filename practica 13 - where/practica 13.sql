/*Trabaje con la tabla "agenda" en la que registra los datos de sus amigos.

1- Elimine "agenda"*/
drop table agenda;

/*2- Cree la tabla, con los siguientes campos: apellido (cadena de 30), nombre 
(cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11):*/
create table agenda( apellido varchar2(30), nombre varchar2(30), domicilio varchar2(30),
telefono varchar2(11) ); 

/*3- Visualice la estructura de la tabla "agenda" (4 campos)*/
describe agenda;

/*4- Ingrese los siguientes registros ("insert into"):*/
insert into agenda(apellido,nombre,domicilio,telefono) values ('Acosta', 'Ana', 'Colon 123', '4234567'); 
insert into agenda(apellido,nombre,domicilio,telefono) values ('Bustamante', 'Betina', 'Avellaneda 135', '4458787'); 
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Hector', 'Salta 545', '4887788'); 
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Luis', 'Urquiza 333', '4545454'); 
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Marisa', 'Urquiza 333', '4545454'); 

/*5- Seleccione todos los registros de la tabla (5 registros)*/
select * from agenda;

/*6- Seleccione el registro cuyo nombre sea "Marisa" (1 registro)*/
select * from agenda where nombre='Marisa';

/*7- Seleccione los nombres y domicilios de quienes tengan apellido igual a "Lopez" (3 registros)*/
select nombre, domicilio from agenda where apellido='Lopez';

/*8- Seleccione los nombres y domicilios de quienes tengan apellido igual a "lopez" (en min�sculas)
No aparece ning�n registro, ya que la cadena "Lopez" no e igual a la cadena "lopez".*/
select nombre, domicilio from agenda where apellido='lopez';

/*9- Muestre el nombre de quienes tengan el tel�fono "4545454" (2 registros)*/
select nombre from agenda where telefono='4545454';
