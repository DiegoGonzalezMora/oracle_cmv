/*Una empresa tiene registrados sus clientes en una tabla llamada "clientes", tambi�n tiene una tabla 
"provincias" donde registra los nombres de las provincias.

1- Elimine las tablas "clientes" y "provincias", cr�elas y agregue restricciones �nicas para los campos "codigo" de ambas tablas:*/
drop table clientes;
drop table provincias;
create table clientes (codigo number(5), nombre varchar2(30), domicilio varchar2(30), ciudad varchar2(20),
codigoprovincia number(3));

create table provincias(codigo number(3), nombre varchar2(20));
alter table clientes add constraints UQ_clientes_codigo unique (codigo);
alter table provincias add constraints UQ_provincias_codigo unique (codigo);

/*2- Ingrese algunos registros para ambas tablas. Incluya valores nulos para el campo "codigo" de "provincias" 
y valores nulos para el campo "codigoprovincia" de "clientes". Incluya en "clientes" un c�digo de provincia 
que no exista en "provincias":*/
insert into provincias values(1,'Cordoba');
insert into provincias values(2,'Santa Fe');
insert into provincias values(3,'Corrientes');
insert into provincias values(4,'Santa Cruz');
insert into provincias values(null,'Salta');
insert into provincias values(null,'Jujuy');
insert into clientes values (100,'Lopez Marcos','Colon 111','C�rdoba',1);
insert into clientes values (200,'Perez Ana','San Martin 222','Cruz del Eje',1);
insert into clientes values (300,'Garcia Juan','Rivadavia 333','Villa Maria',null);
insert into clientes values (400,'Perez Luis','Sarmiento 444','Rosario',2);
insert into clientes values (500,'Gomez Ines','San Martin 666','Santa Fe',2);
insert into clientes values (600,'Torres Fabiola','Alem 777','La Plata',5);
insert into clientes values (700,'Garcia Luis','Sucre 475','Santa Rosa',null);

--3- Muestre todos los datos de los clientes, incluido el nombre de la provincia
select c.*, p.nombre as nombre_provincia
    from clientes c left join provincias p on (c.CODIGOPROVINCIA=p.codigo);
--Note que los clientes de "Santa Rosa", "Villa Maria" y "La Plata" se muestran seteados a null en la columna corespondiente 
--al nombre de la provincia porque tienen valores nulos o inexistentes.

--4- Realice la misma consulta anterior pero alterando el orden de las tablas.
select c.*, p.nombre as nombre_provincia
    from clientes c left join provincias p on (c.CODIGOPROVINCIA=p.codigo)
    order by c.nombre;

--5- Muestre solamente los clientes de las provincias que existen en "provincias" (4 registros)
select c.*, p.nombre as nombre_provincia
    from provincias p left join clientes c on (c.CODIGOPROVINCIA=p.codigo);
--Note que los clientes de "Jujuy", "Salta", "Santa Cruz" y "Corrientes" se muestran seteados a null en los campos 
--pertenecientes a la tabla "clientes" porque tienen valores nulos o inexistentes en dicha tabla.

--6- Muestre todos los clientes cuyo c�digo de provincia NO existe en "provincias" ordenados por nombre del cliente (3 registros)
select * from clientes c left join provincias p on (c.codigoprovincia=p.codigo) where p.CODIGO is null;

/*
select c.* from clientes c
where c.codigoprovincia not in (select c2.codigoprovincia from clientes c2,provincias p2 where c2.codigoprovincia=p2.codigo)
    order by c.nombre;

select c.* 
    from clientes c left join provincias p on (c.CODIGOPROVINCIA=p.codigo)
    where c.codigoprovincia!=p.codigo;

select * from clientes c
where c.codigoprovincia not in (select p.codigo
                                from provincias p);
*/

--7- Obtenga todos los datos de los clientes de "Cordoba" (2 registros)
select c.*
    from clientes c join provincias p on (c.CODIGOPROVINCIA=p.codigo)
    where p.nombre='Cordoba';