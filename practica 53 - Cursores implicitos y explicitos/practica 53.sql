--1. Usando la siguiente tabla trabajar con cursores.
drop table Pais;

CREATE TABLE Pais(

PaisCodigo varchar2(3) PRIMARY KEY,

PaisNombre varchar2(52) NOT NULL,

PaisContinente varchar2(50) NOT NULL

);

INSERT INTO Pais (PaisCodigo, PaisNombre, PaisContinente) VALUES ('Us','Usa', 'AMERICA');
INSERT INTO Pais (PaisCodigo, PaisNombre, PaisContinente) VALUES ('Es','Espa�a', 'EUROPA');
INSERT INTO Pais (PaisCodigo, PaisNombre, PaisContinente) VALUES ('Ni','Nigeria', 'AFRICA');
INSERT INTO Pais (PaisCodigo, PaisNombre, PaisContinente) VALUES ('Aus','Australia', 'OCEANIA');
INSERT INTO Pais (PaisCodigo, PaisNombre, PaisContinente) VALUES ('Ch','China', 'ASIA');

--Crear un ejemplo para cursor implicito y explicito

--implicito
SET SERVEROUTPUT ON;
declare
    pais VARCHAR2(50);
begin
  SELECT PaisNombre INTO pais from PAIS WHERE PaisContinente = 'ASIA';
  dbms_output.put_line('La lectura del cursor es: ' || pais);
end;

--explicito
set SERVEROUTPUT ON;
DECLARE
CURSOR CPAISES
IS
SELECT PAISNOMBRE, PAISCONTINENTE FROM PAIS;

NOMBRE VARCHAR2(10);
CONTINENTE VARCHAR2(10);
BEGIN
    OPEN CPAISES;
    FETCH CPAISES INTO NOMBRE, CONTINENTE;
    WHILE CPAISES%FOUND
    LOOP
        FETCH CPAISES INTO NOMBRE, CONTINENTE;
        dbms_output.put_line(NOMBRE||' '||CONTINENTE);
    END LOOP;
    CLOSE CPAISES;
END;