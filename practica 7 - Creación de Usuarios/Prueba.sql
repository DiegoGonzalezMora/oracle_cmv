/*Una escuela necesita crear 2 usuarios diferentes en su base de datos. Uno denominado �alumno" y otro "profesor". 
Luego se les conceder�n diferentes permisos para restringir el acceso a los diferentes objetos. 
Con�ctese como administrador (por ejemplo "system").*/

/*1- Primero eliminamos el usuario �alumno", porque si existe, aparecer� un mensaje de error:*/
DROP USER alumno CASCADE;

/*2- Cree un usuario �alumno", con contrase�a "escuela" y 100M de espacio en "system"*/
CREATE USER alumno IDENTIFIED BY escuela DEFAULT TABLESPACE system QUOTA 6 ON SYSTEM;

/*3- Elimine el usuario "profesor":*/
DROP USER profesor CASCADE;

/*4- Cree un usuario "profesor", con contrase�a "maestro" y espacio en "system"*/
CREATE USER profesor IDENTIFIED BY maestro DEFAULT TABLESPACE system;

/*5- Consulte el diccionario "dba_users" y analice la informaci�n que nos muestra*/
SELECT * FROM DBA_USERS;