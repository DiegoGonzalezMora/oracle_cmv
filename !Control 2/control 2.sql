DROP TABLE CENTROS1;
Create Table  Centros1 ( 
Numero_Centro  NUMBER(3)  PRIMARY KEY, 
Centro VARCHAR2(30), 
Direccion VARCHAR2(50) 
); 
 
DROP TABLE EMPLEADOS1;
Create Table Empleados1 ( 
Num_Empleado  VARCHAR(3) PRIMARY KEY, 
Nombre VARCHAR2(50), 
Fecha_Nacimiento Date, 
Fecha_Ingreso Date, 
Salario NUMBER, 
Numero_Centro  NUMBER(3)   
); 
 
DROP TABLE ACTIVIDADES1 ;
Create Table Actividades1 ( 
Cod_Actividades  VARCHAR(3) PRIMARY KEY, 
Actividad VARCHAR2(50), 
Departamento NUMBER(3), 
Num_Empleado VARCHAR(3) 
);
select * from actividades;
set SERVEROUTPUT ON;


--Realiza los siguientes ejercicios: 
--1.	Inserta registros en la tabla Centros que contengan direcciones en Madrid o Barcelona  
insert into centros1 values(1,'centro1','Avd de la independencia n�8 2c, barcelona');
insert into centros1 values(2,'centro2','Avd del patriota n�3 2d, madrid');

--2.	Crear un trigger en la tabla CENTROS que s�lo permita insertar registros que contengan la palabra MADRID o BARCELONA en el campo direcci�n. 
create or replace trigger BI_CENTROS1
BEFORE insert on CENTROS1
for each row
declare
begin
    IF (:NEW.Direccion NOT LIKE '%MADRID%') OR (:NEW.Direccion NOT LIKE '%BARCELONA%') THEN
        Raise_Application_Error (-20350, 'CENTRO INVALIDO, DEBE ESTAR EN MADRID O BARCELONA');
    END IF;
end;

begin
    insert into CENTROS1 values(18, 'CENTRO 888', 'AVD BUTANERO, MADRID');
end;
SELECT * FROM CENTROS1;

--3.	Ingrese registros de prueba en la tabla Empleados.  
INSERT INTO EMPLEADOS1 VALUES(1,'LUIS ALEGRE','12-12-83','11-11-12',1650,1);
INSERT INTO EMPLEADOS1 VALUES(2,'MANOLO SANCHEZ','1-2-80','1-1-13',1700,1);
INSERT INTO EMPLEADOS1 VALUES(3,'JESUS MU�OZ','06-05-90','02-07-12',1000,2);
INSERT INTO EMPLEADOS1 VALUES(10,'JESUS VAZQUES','06-01-91','02-07-10',4000,2);
INSERT INTO EMPLEADOS1 VALUES(11,'DIEGO','06-01-91','02-07-10',100,1);

--4.	Crear una secuencia llamada "sec_actividades"  estableciendo el valor m�nimo (1) y m�ximo (999). 
drop sequence sec_actividades;
create sequence sec_actividades minvalue 1 maxvalue 999 nocycle;

--5.	Ingrese algunos registros en la tabla Actividades, empleando la secuencia creada para los valores de la clave primaria. 
--Ten en cuenta que debes a�adir algunos registros que pertenezcan al departamento 122. 
DROP TRIGGER BI_ACTIVIDADES_AUTOINCREMENTAL;
CREATE OR REPLACE TRIGGER BI_ACTIVIDADES_AUTOINCREMENTAL
BEFORE INSERT ON ACTIVIDADES1
FOR EACH ROW
    DECLARE CODIGO NUMBER := 0;
BEGIN
    SELECT sec_actividades.NEXTVAL INTO CODIGO FROM DUAL;
    :NEW.COD_ACTIVIDADES := CODIGO;
END;

INSERT INTO ACTIVIDADES1 (ACTIVIDAD,DEPARTAMENTO,NUM_EMPLEADO) VALUES ('COMERCIAL',122,1);
INSERT INTO ACTIVIDADES1 (ACTIVIDAD,DEPARTAMENTO,NUM_EMPLEADO) VALUES ('VENTAS',122,1);
INSERT INTO ACTIVIDADES1 (ACTIVIDAD,DEPARTAMENTO,NUM_EMPLEADO) VALUES ('VENTAS',122,2);
INSERT INTO ACTIVIDADES1 (ACTIVIDAD,DEPARTAMENTO,NUM_EMPLEADO) VALUES ('ADMINISTRACION',111,2);
INSERT INTO ACTIVIDADES1 (ACTIVIDAD,DEPARTAMENTO,NUM_EMPLEADO) VALUES ('REPARACIONES',100,3);
INSERT INTO ACTIVIDADES1 (ACTIVIDAD,DEPARTAMENTO,NUM_EMPLEADO) VALUES ('REPARACIONES',100,11);
INSERT INTO ACTIVIDADES1 (ACTIVIDAD,DEPARTAMENTO,NUM_EMPLEADO) VALUES ('REPARACIONES',100,1);
INSERT INTO ACTIVIDADES1 (ACTIVIDAD,DEPARTAMENTO,NUM_EMPLEADO) VALUES ('REPARTO',130,11);
SELECT * FROM ACTIVIDADES1;

--6.	Crear un trigger a nivel de filas en la tabla empleados que tenga las siguientes condiciones : 
---Que el salario sea menor que 30 
---Que la  fecha de ingreso sea mayor que fecha de nacimiento 
create or replace trigger BI_EMPLEADOS1
BEFORE insert on EMPLEADOS1
for each row
declare
begin
    IF :NEW.SALARIO<30 THEN 
        Raise_Application_Error (-20351, 'SALARIO MENOR QUE 30');
    ELSIF (:NEW.FECHA_NACIMIENTO-:NEW.FECHA_INGRESO)>0 THEN
        --Dbms_output.put_line(to_char(:NEW.FECHA_NACIMIENTO-:NEW.FECHA_INGRESO));
        Raise_Application_Error (-20352, 'A FECHA DE INGRESO ES MENOR QUE LA DE NACIMIENTO');
    END IF;
end;

begin
    insert into EMPLEADOS1 VALUES(21,'LUIS ALEGRE2','13-8-12','11-12-12',1500,1);
end;
SELECT * FROM EMPLEADOS1;

--7. Obtener mediante SQL: 
---Para los DEPARTAMENTOS en los que hay alg�n empleado con salario sea superior a 400 euros mensuales hallar el n�mero de empleados y la suma 
--de sus salarios. 
SELECT AC.DEPARTAMENTO, COUNT(AC.NUM_EMPLEADO) NUM_EMPLEADOS, SUM(E1.SALARIO) SUMA_SALARIO
    FROM ACTIVIDADES AC JOIN EMPLEADOS1 E1 ON(E1.NUM_EMPLEADO=AC.NUM_EMPLEADO)
    WHERE E1.NUM_EMPLEADO = SOME (SELECT E2.NUM_EMPLEADO FROM EMPLEADOS1 E2 WHERE SALARIO>400)
    GROUP BY AC.DEPARTAMENTO;
    
---Obtener el empleado con m�s actividades asignadas. 
SELECT *
    FROM (SELECT E.NUM_EMPLEADO, E.NOMBRE, COUNT(AC.NUM_EMPLEADO) as actividades_asignadas
                FROM EMPLEADOS1 E JOIN ACTIVIDADES1 AC ON(E.NUM_EMPLEADO=AC.NUM_EMPLEADO)
                GROUP BY E.NUM_EMPLEADO, E.NOMBRE
                order by COUNT(AC.NUM_EMPLEADO) desc)
    WHERE ROWNUM = 1;
    
select * from empleados1 
    where num_empleado=(select a1.num_empleado 
                            from actividades1 a1 group by a1.num_empleado 
                            having count(a1.actividad)>=all(select count(a.actividad)as num_act 
                                                                from actividades1 a 
                                                                group by a.num_empleado)
                        );
    
---Obtener los nombres de los empleados cuyo salario supera a todos los salarios de los empleados del departamento 122. 
SELECT E1.NOMBRE
    FROM EMPLEADOS1 E1
    WHERE E1.SALARIO > (SELECT SUM(E.SALARIO)
                            FROM EMPLEADOS1 E JOIN ACTIVIDADES1 AC ON (E.NUM_EMPLEADO=AC.NUM_EMPLEADO)
                            WHERE AC.DEPARTAMENTO=122);


---Hallar el presupuesto medio de los departamentos. 
SELECT AC1.DEPARTAMENTO, AVG(E1.SALARIO) AS MEDIA_PRESUPUESTO
    FROM ACTIVIDADES AC1 JOIN EMPLEADOS1 E1 ON(AC1.NUM_EMPLEADO=E1.NUM_EMPLEADO)
    GROUP BY AC1.DEPARTAMENTO;

--8. Realizar una funci�n que regrese el n�mero de actividades que tiene asignada un empleado. 
create or replace FUNCTION NUM_ACT_EMPLEADO(NEMPLEADO number)
RETURN Number
IS
    RESULT NUMBER;
BEGIN
    SELECT COUNT(AC.NUM_EMPLEADO) INTO RESULT
            FROM EMPLEADOS1 E JOIN ACTIVIDADES AC ON(E.NUM_EMPLEADO=AC.NUM_EMPLEADO)
            WHERE E.NUM_EMPLEADO=NEMPLEADO;
    return(RESULT);
EXCEPTION
    WHEN NO_DATA_FOUND THEN
    return 0;
END;

BEGIN
    DBMS_OUTPUT.PUT_LINE(NUM_ACT_EMPLEADO(3));
END;

--9. Crear un procedimiento que actualice las actividades de los empleados que han ingresado en la empresa antes del  5/02/1978.Para ello 
--actualizaremos el campo Actividad a�adiendo al  final de la cadena el valor 2. 
create or replace procedure PA_ACTIVIDADES_EMPLEADOS
as
CURSOR CACTIVIDAD IS SELECT AC.ACTIVIDAD, E1.NUM_EMPLEADO, E1.FECHA_INGRESO 
                            FROM ACTIVIDADES AC JOIN EMPLEADOS1 E1 ON (AC.NUM_EMPLEADO=E1.NUM_EMPLEADO);
RACTIVIDAD CACTIVIDAD%ROWTYPE;
begin
    OPEN CACTIVIDAD;
    LOOP
        FETCH CACTIVIDAD INTO RACTIVIDAD;
        IF RACTIVIDAD.FECHA_INGRESO < '5/02/1978' THEN
            UPDATE ACTIVIDADES SET ACTIVIDAD=ACTIVIDAD||'2' WHERE RACTIVIDAD.NUM_EMPLEADO=NUM_EMPLEADO;
        END IF;
        EXIT WHEN CACTIVIDAD%NOTFOUND;
    END LOOP;
    CLOSE CACTIVIDAD;
end PA_ACTIVIDADES_EMPLEADOS;

UPDATE EMPLEADOS1 SET FECHA_INGRESO='5/02/1970' WHERE NUM_EMPLEADO=1;

BEGIN
    PA_ACTIVIDADES_EMPLEADOS();
END;

SELECT * FROM ACTIVIDADES;

--10. Usando Cursores desarrolla un procedimiento que visualice el nombre y la fecha de ingreso de todos los empleados ordenados por nombre. 
create or replace procedure PA_MUESTRA_EMPLEADOS
as
CURSOR CEMPLEADO IS SELECT NOMBRE, FECHA_INGRESO FROM EMPLEADOS1 ORDER BY NOMBRE;
REMPLEADO CEMPLEADO%ROWTYPE;
begin
    OPEN CEMPLEADO;
    LOOP
        FETCH CEMPLEADO INTO REMPLEADO;
        DBMS_OUTPUT.PUT_LINE('NOMBRE: '||REMPLEADO.NOMBRE||' - FECHA DE INGRESO: '||REMPLEADO.FECHA_INGRESO);
        EXIT WHEN CEMPLEADO%NOTFOUND;
    END LOOP;
    CLOSE CEMPLEADO;
end PA_MUESTRA_EMPLEADOS;

BEGIN
    PA_MUESTRA_EMPLEADOS();
END;

--11. Usando Cursores codificar un procedimiento que muestre el n�mero de cada departamento y el n�mero de empleados que tiene.
create or replace procedure PA_MUESTRA_DEPARTAMENTOS
as
CURSOR CDEPARTAMENTO IS SELECT DEPARTAMENTO, COUNT(NUM_EMPLEADO) AS NUM_EMPLEADOS FROM ACTIVIDADES GROUP BY DEPARTAMENTO;
RDEPARTAMENTO CDEPARTAMENTO%ROWTYPE;
begin
    OPEN CDEPARTAMENTO;
    LOOP
        FETCH CDEPARTAMENTO INTO RDEPARTAMENTO;
        DBMS_OUTPUT.PUT_LINE('DEPARTAMENTO: '||RDEPARTAMENTO.DEPARTAMENTO||' - NUMERO DE EMPLEADOS: '||RDEPARTAMENTO.NUM_EMPLEADOS);
        EXIT WHEN CDEPARTAMENTO%NOTFOUND;
    END LOOP;
    CLOSE CDEPARTAMENTO;
end PA_MUESTRA_DEPARTAMENTOS;

BEGIN
    PA_MUESTRA_DEPARTAMENTOS();
END;


--12. Usando cursores explicitos codificar un programa que visualice los dos empleados que ganan menos de cada departamento.
SELECT * 
    FROM(SELECT AC.DEPARTAMENTO, E1.NOMBRE, E1.SALARIO
        FROM ACTIVIDADES AC JOIN EMPLEADOS1 E1 ON (AC.NUM_EMPLEADO=E1.NUM_EMPLEADO)
        GROUP BY AC.DEPARTAMENTO, E1.NOMBRE, E1.SALARIO
        ORDER BY AC.DEPARTAMENTO, E1.SALARIO ASC)
    WHERE ROWNUM=1 OR ROWNUM=2;

create or replace procedure PA_MUESTRA_EMPLEADOS_PRECARIOS
IS
I NUMBER;
CURSOR CEMPLEADOS IS SELECT AC.DEPARTAMENTO, E1.NOMBRE, E1.SALARIO
                            FROM ACTIVIDADES AC JOIN EMPLEADOS1 E1 ON (AC.NUM_EMPLEADO=E1.NUM_EMPLEADO)
                            WHERE ROWNUM=I
                            GROUP BY AC.DEPARTAMENTO, E1.NOMBRE, E1.SALARIO
                            ORDER BY AC.DEPARTAMENTO, E1.SALARIO ASC;
REMPLEADOS CEMPLEADOS%ROWTYPE;
begin
    OPEN CEMPLEADOS;
    LOOP
        I:=1;
        FETCH CEMPLEADOS INTO REMPLEADOS;
        IF I<=2 THEN
            DBMS_OUTPUT.PUT_LINE(REMPLEADOS.DEPARTAMENTO||' '||REMPLEADOS.NOMBRE||' '||REMPLEADOS.SALARIO);
            I:=I+1;
        ELSE I:=1;
        END IF;
        EXIT WHEN CEMPLEADOS%NOTFOUND;
    END LOOP;
    CLOSE CEMPLEADOS;
end PA_MUESTRA_EMPLEADOS_PRECARIOS;

BEGIN
    PA_MUESTRA_EMPLEADOS_PRECARIOS();
END;

--13. 3. Desarrollar un procedimiento que visualice el nombre,salario y la fecha de ingreso de todos los empleados ordenados por nombre 
--usando cursores y SQL din�mico. 
create or replace procedure PA_MUESTRA_EMPLEADOS_DINAMICO
as
SENTENCIA VARCHAR2(500);
begin
    SENTENCIA:= 'SELECT NOMBRE, FECHA_INGRESO FROM EMPLEADOS1 ORDER BY NOMBRE;';
    EXECUTE IMMEDIATE SENTENCIA;
        
end PA_MUESTRA_EMPLEADOS_DINAMICO;

BEGIN
    PA_MUESTRA_EMPLEADOS_DINAMICO();
END;

--14. Realizar un formulario maestro-detalle, donde maestro ser� la tabla Empleados y el detalle la tabla Actividades. 
COMMIT;

--15. Realizar un formulario para dar de alta actividades. A la hora de insertar un Num_empleado, se utilizar� una lista de valores (LOV) 
--que muestre el nombre del empleado. 
--16. Realizar un informe en form report que muestre la siguiente informaci�n: 
---Nombre del centro 
---Direcci�n del centro 
---Nombre del empleado 
---Nombre de la actividad 
---Departamento de la actividad 
--17. Realizar un formulario donde con un bot�n desde el formulario empleados, se muestre el informe generado anteriormente. 
