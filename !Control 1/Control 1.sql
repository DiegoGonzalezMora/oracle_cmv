/*CONTROL 1*/

--11
select count(numero) as numero_empleados from empleados;

--12
select count(*) as num_dept, sum(presupuesto) as presupuesto_anual from departamentos; 

--13
select count(*) as num_empleados, count(distinct telefono) distinto_tlfn
    from empleados
    where departamento=112; 

--14
select d.NOMBRE, count(*) as num_empleados, count(distinct e.telefono) distinto_tlfn
    from empleados e join departamentos d on(e.DEPARTAMENTO=d.NUMERO)
    where d.TIPO_DIR!='P'
    group by d.nombre;

--15
select e.NOMBRE
    from empleados e join departamentos d on(e.DEPARTAMENTO=d.NUMERO)
        join centros c on(d.CENTRO=c.numero)
    where e.NUM_HIJOS=2 and c.direccion like '%ATOCHA%';
    
--16
select e.nombre, e.salario, e1.nombre, e1.salario, d.centro
    from empleados e join departamentos d on(e.departamento=d.numero), empleados e1 join departamentos d1 on(e1.departamento=d1.numero)
    where e.numero!=e1.numero and e.salario=e1.salario and d.centro=d1.centro;
    
--17 Utilice las operaciones de conjunto para extraer los c�digos de los departamentos que 
--no hacen de departamento jefe

--hay que sacar los que tienen un departamento jefe y los que no hacen de departamento jefe y hacer algo
select numero from departamentos
minus
select distinct dpto_jefe from departamentos;

--18 Extraiga un listado donde aparezca el codigo de los departamentos y su nombre conjuntamente con el
--codigo de los centros en donde estan situados y el nombre de estos centros
select d.numero num_depto, d.nombre nombre_depto, c.numero num_centro, c.nombre nombre_centro
    from departamentos d join centros c on(d.centro=c.numero);
    
--19 Hallar los siguientes datos para cada departamento

--a) Media de las comisiones, salario medio, y numero de comisiones que hay distintas
select floor(avg(comision)) media_comision, floor(avg(salario)) media_salario, count(DISTINCT comision) dif_comisiones
    from empleados e join departamentos d on(e.DEPARTAMENTO=d.numero);

--b) Salario max y min y media de las comisiones que hay distintas
--floor(sum(DISTINCT comision)/count(distinct comision)) media_dif_comisiones
select max(e.salario) max_salario, min(e.salario) min_salario, floor(avg(DISTINCT comision)) media_dif_comisiones
    from empleados e join departamentos d on(e.DEPARTAMENTO=d.numero);

--c)
--No. No afecta al calculo.

--20
select e.num_hijos, floor(avg(salario)) media_salario, floor(avg(comision)) comision_media
    from empleados e join departamentos d on(e.DEPARTAMENTO=d.numero)
    group by e.num_hijos
    order by e.num_hijos;

--21 Calcular cuantos numeros de telefono usa cada departamento mostrando el codigo y el nombre de cada departamento
select d.numero, d.nombre, count(e.telefono) as num_tlfn
    from departamentos d join empleados e on(d.numero=e.departamento)
    group by d.numero, d.nombre
    order by d.numero;

--22 Mostrar los numeros de tlfn que esten asignados a mas de un empleado, indicando a cuantos empleados esta asignado cada uno
select telefono, count(*) asignado
    from empleados
    group by telefono
    having count(*)>1;
    
--23 Mostrar los departamentos (numero y nombre) con sus centros (numero y nombre) y la edad media de sus empleados
--de auqellos departamentos que tienen una edad media en sus empleados mayor a 35 a�os
select d.numero num_depto, d.nombre nombre_depto, c.numero num_centro, c.nombre nombre_centro, 
            FLOOR(avg(EXTRACT(YEAR FROM CURRENT_DATE)-(EXTRACT(YEAR FROM E.fecha_nacimiento)-100))) AS MEDIA_ANIOS
    from empleados e join departamentos d on(e.departamento=d.numero)
        join centros c on(c.numero=d.centro)
    GROUP BY d.numero, d.nombre, c.numero, c.nombre
    HAVING AVG(EXTRACT(YEAR FROM CURRENT_DATE)-(EXTRACT(YEAR FROM E.fecha_nacimiento)-100))>35;
    
--24
set serveroutput on;
create or replace FUNCTION fn_compara(pnumero number, pcentro NUMBER) RETURN varchar2
IS
    mensaje varchar2(50);
    resultado number;
BEGIN
    mensaje:='Combinaci�n inv�lida';
    
    select numero into resultado from departamentos where numero=pnumero;
    select numero into resultado from centros where numero=pcentro;
    
    if pcentro=10 and pnumero<121 then
        mensaje:='Primeros Departamentos en Centro 10.';
    end if;
    if pcentro=20 then
        mensaje:='Departamentos en Centro 20.';
    end if;
    if pcentro=10 and pnumero>=121 then
        mensaje:='Departamentos restantes en Centro 10.';
    end if;
    return mensaje;
    EXCEPTION 
        WHEN NO_DATA_FOUND THEN
          return 'P�rametro no v�lido';
END;

DECLARE
  mensaje varchar2(50);
BEGIN
	mensaje := fn_compara(111,10);
    dbms_output.put_line(mensaje);
END; 