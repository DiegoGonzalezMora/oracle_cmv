/*Un videoclub que alquila pel�culas en video almacena la informaci�n de sus pel�culas en 
alquiler en una tabla llamada "peliculas".

1- Elimine la tabla si existe.*/
drop TABLE PELICULAS;

/*2- Cree la tabla:*/
create table peliculas(titulo varchar2(20), actor varchar2(20), duracion number(3), cantidad number(1));

/*3- Vea la estructura de la tabla*/
describe peliculas;

/*4- Ingrese los siguientes registros:*/
insert into peliculas (titulo, actor, duracion, cantidad)
values ('Mision imposible','Tom Cruise',180,3);

insert into peliculas (titulo, actor, duracion, cantidad)
values ('Mision imposible 2','Tom Cruise',190,2);

insert into peliculas (titulo, actor, duracion, cantidad)
values ('Mujer bonita','Julia Roberts',118,3);

insert into peliculas (titulo, actor, duracion, cantidad)
values ('Elsa y Fred','China Zorrilla',110,2);

/*5- Realice un "select" mostrando solamente el t�tulo y actor de todas las pel�culas*/
select titulo, actor from peliculas;

/*6- Muestre el t�tulo y duraci�n de todas las peliculas.*/
select titulo, duracion from peliculas;

/*7- Muestre el t�tulo y la cantidad de copias.*/
select titulo, cantidad from peliculas;