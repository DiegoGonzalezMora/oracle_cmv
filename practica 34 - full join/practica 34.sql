--Un club dicta clases de distintos deportes. Almacena la informaci�n en una tabla llamada 
--"deportes" en la cual incluye el nombre del deporte y el nombre del profesor y en otra tabla 
--llamada "inscriptos" que incluye el documento del socio que se inscribe, el deporte y si 
--la matricula est� paga o no.

--1- Elimine las tablas, cr�elas y agregue dos restricciones, una "primary key" sobre el campo "codigo" 
--de "deportes" y otra compuesta por "documento" y "codigodeporte" de "inscriptos":
drop table deportes;
drop table inscriptos;
create table deportes(codigo number(2), nombre varchar2(30), profesor varchar2(30));

create table inscriptos(documento char(8), codigodeporte number(2), matricula char(1) --'s'=paga; 'n'=impaga
);

alter table deportes
add constraint PK_deportes
primary key(codigo);
alter table inscriptos
add constraint PK_inscriptos
primary key(documento,codigodeporte);

--2- Ingrese algunos registros para ambas tablas:
insert into deportes values(1,'tenis','Marcelo Roca');
insert into deportes values(2,'natacion','Marta Torres');
insert into deportes values(3,'basquet','Luis Garcia');
insert into deportes values(4,'futbol','Marcelo Roca');
insert into inscriptos values('22222222',3,'s');
insert into inscriptos values('23333333',3,'s');
insert into inscriptos values('24444444',3,'n');
insert into inscriptos values('22222222',2,'s');
insert into inscriptos values('23333333',2,'s');
insert into inscriptos values('22222222',4,'n');
insert into inscriptos values('22222222',5,'n');

--3- Muestre todos la informaci�n de la tabla "inscriptos", y consulte la tabla "deportes" para 
--obtener el nombre de cada deporte (6 registros)
select i.*,  d.nombre
    from inscriptos i full join deportes d on (i.codigodeporte=d.codigo);
--Note que uno de los registros tiene seteado a null la columna "deporte".

--4- Empleando un "left join" con "deportes" obtenga todos los datos de los inscriptos (7 registros)
select i.*
    from inscriptos i left join deportes d on (i.codigodeporte=d.codigo);

--5- Obtenga la misma salida anterior empleando un "rigth join"
select i.*
    from inscriptos i right join deportes d on (i.codigodeporte=d.codigo);
--Note que se cambia el orden de las tablas y "right" por "left".

--6- Muestre los deportes para los cuales no hay inscriptos, empleando un "left join" (1 registro)
select d.* 
    from deportes d left join inscriptos i on (i.CODIGODEPORTE=d.CODIGO)
    where i.codigodeporte is null;

--7- Muestre los documentos de los inscriptos a deportes que no existen en la tabla "deportes" (1 registro)
select * 
    from inscriptos i full join deportes d on i.CODIGODEPORTE=d.CODIGO 
    where i.codigodeporte is null;

--8- Emplee un "full join" para obtener todos los datos de ambas tablas, incluyendo las inscripciones a deportes 
--inexistentes en "deportes" y los deportes que no tienen inscriptos (8 registros)
select i.* 
    from inscriptos i full join deportes d on i.CODIGODEPORTE=d.CODIGO;
--Note que uno de los registros con documento "22222222" tiene seteado a "null" los campos correspondientes 
--a "deportes" porque el c�digo "5" no est� presente en "deportes"; otro registro, que muestra "tenis" y 
--"Marcelo Roca", tiene valores nulos en los campos correspondientes a la tabla "inscriptos", ya que, para el 
--deporte con c�digo 1, no hay inscriptos.