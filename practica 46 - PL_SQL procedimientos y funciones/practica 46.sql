/*Una empresa almacena los datos de sus empleados en una tabla llamada "empleados".

1- Eliminamos la tabla y la creamos:*/
drop table empleados;
create table empleados(documento char(8),nombre varchar2(20),apellido varchar2(20),sueldo number(6,2),fechaingreso date);

--2- Ingrese algunos registros:
ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI'; 
insert into empleados values('22222222','Juan','Perez',300,'10/10/1980');
insert into empleados values('22333333','Luis','Lopez',300,'12/05/1998');
insert into empleados values('22444444','Marta','Perez',500,'25/08/1990');
insert into empleados values('22555555','Susana','Garcia',400,'05/05/2000');
insert into empleados values('22666666','Jose Maria','Morales',400,'24/10/2005');

--3- Cree un procedimiento almacenado llamado "pa_empleados_aumentarsueldo". Debe incrementar el sueldo de 
--los empleados con cierta cantidad de a�os en la empresa (par�metro "ayear" de tipo num�rico) en un porcentaje 
--(par�metro "aporcentaje" de tipo numerico); es decir, recibe 2 par�metros.
set SERVEROUTPUT ON;

create or replace procedure pa_empleados_aumentarsueldo (ayear number, aporcentaje number)
is
    current_year number;
begin
    update empleados set
        sueldo = (aporcentaje*sueldo/100)+sueldo
    where (extract(year from current_date)-extract(year from fechaingreso))>=ayear;
end pa_empleados_aumentarsueldo;

--4- Ejecute el procedimiento creado anteriormente.

--5- Verifique que los sueldos de los empleados con m�s de 10 a�os en la empresa han aumentado un 20%
begin
 pa_empleados_aumentarsueldo(10,20);
end;

select * from empleados;
/*
22222222	Juan	Perez	360	10/10/1980 00:00
22333333	Luis	Lopez	360	12/05/1998 00:00
22444444	Marta	Perez	600	25/08/1990 00:00
22555555	Susana	Garcia	480	05/05/2000 00:00
22666666	Jose Maria	Morales	480	24/10/2005 00:00
*/

--6- Ejecute el procedimiento creado anteriormente enviando otros valores como par�metros (por ejemplo, 8 y 10)
begin
 pa_empleados_aumentarsueldo(8,10);
end;

--7- Verifique que los sueldos de los empleados con m�s de 8 a�os en la empresa han aumentado un 10%
select * from empleados;
/*
22222222	Juan	Perez	396	10/10/1980 00:00
22333333	Luis	Lopez	396	12/05/1998 00:00
22444444	Marta	Perez	660	25/08/1990 00:00
22555555	Susana	Garcia	528	05/05/2000 00:00
22666666	Jose Maria	Morales	528	24/10/2005 00:00*/

--8- Ejecute el procedimiento almacenado "pa_empleados_aumentarsueldo" sin par�metros
begin
 pa_empleados_aumentarsueldo();
end;
--PLS-00306: wrong number or types of arguments in call to 'PA_EMPLEADOS_AUMENTARSUELDO'

--9- Cree un procedimiento almacenado llamado "pa_empleados_ingresar" que ingrese un empleado en la tabla 
--"empleados", debe recibir valor para el documento, el nombre, apellido y almacenar valores nulos en los campos 
--"sueldo" y "fechaingreso"
create or replace procedure pa_empleados_ingresar (documento char,nombre varchar2,apellido varchar2)
is
begin
    insert into empleados values(documento, nombre, apellido, null, null);
end pa_empleados_ingresar;

--10- Ejecute el procedimiento creado anteriormente y verifique si se ha ingresado en "empleados" un nuevo registro
begin
 pa_empleados_ingresar(22666667, 'Jose', 'Mora');
end;
select * from empleados;

--11- Reemplace el procedimiento almacenado llamado "pa_empleados_ingresar" para que ingrese un empleado en la tabla 
--"empleados", debe recibir valor para el documento (con valor por defecto nulo) y fechaingreso (con la fecha actual 
--como valor por defecto), los dem�s campos se llenan con valor nulo
create or replace procedure pa_empleados_ingresar(documento char default null, fechaingreso date default sysdate)
is
begin
    insert into empleados values(documento, null, null, null, fechaingreso);
end pa_empleados_ingresar;

--12- Ejecute el procedimiento creado anteriormente envi�ndole valores para los 2 par�metros y verifique si se 
--ha ingresado en "empleados" un nuevo registro
begin
 pa_empleados_ingresar('564656',null);
end;
select * from empleados;

--13- Ejecute el procedimiento creado anteriormente enviando solamente la fecha de ingreso y vea el resultado
--Oracle toma el valor enviado como primer argumento e intenta ingresarlo en el campo "documento", muestra un 
--mensaje de error indicando que el valor es muy grande, ya que tal campo admite 8 caracteres.
begin
 pa_empleados_ingresar('10/10/1980');
end;
--ORA-12899: value too large for column "SYSTEM"."EMPLEADOS"."DOCUMENTO" (actual: 10, maximum: 8)

--14- Cree (o reemplace) un procedimiento almacenado que reciba un documento y elimine de la tabla "empleados" 
--el empleado que coincida con dicho documento
create or replace procedure pa_empleados_borrar(doc char)
is
begin
    delete from empleados where documento like doc;
end pa_empleados_borrar;

--15- Elimine un empleado empleando el procedimiento del punto anterior
begin
    pa_empleados_borrar('22222222');
end;

--16- Verifique la eliminaci�n
select * from empleados;
/*
22333333	Luis	Lopez	396	12/05/1998 00:00
22444444	Marta	Perez	660	25/08/1990 00:00
22555555	Susana	Garcia	528	05/05/2000 00:00
22666666	Jose Maria	Morales	528	24/10/2005 00:00
*/