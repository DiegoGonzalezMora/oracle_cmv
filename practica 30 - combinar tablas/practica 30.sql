/*Un club dicta clases de distintos deportes. En una tabla llamada "socios" guarda los datos de los socios, 
en una tabla llamada "deportes" la informaci�n referente a los diferentes deportes que se dictan y en una 
tabla denominada "inscriptos", las inscripciones de los socios a los distintos deportes.

Un socio puede inscribirse en varios deportes el mismo a�o. Un socio no puede inscribirse en el mismo deporte 
el mismo a�o. Distintos socios se inscriben en un mismo deporte en el mismo a�o.

1- Elimine las tablas:*/
drop table socios;
drop table deportes;
drop table inscriptos;

--2- Cree las tablas con las siguientes estructuras:
create table socios(documento char(8) not null,nombre varchar2(30),domicilio varchar2(30),primary key(documento));
create table deportes(codigo number(2),nombre varchar2(20),profesor varchar2(15),primary key(codigo));
create table inscriptos(documento char(8) not null,codigodeporte number(2) not null,a�o char(4),matricula char(1),--'s'=paga, 'n'=impaga
primary key(documento,codigodeporte,a�o));

--3- Ingrese algunos registros en "socios":
insert into socios values('22222222','Ana Acosta','Avellaneda 111');
insert into socios values('23333333','Betina Bustos','Bulnes 222');
insert into socios values('24444444','Carlos Castro','Caseros 333');
insert into socios values('25555555','Daniel Duarte','Dinamarca 44');

--4- Ingrese algunos registros en "deportes":
insert into deportes values(1,'basquet','Juan Juarez');
insert into deportes values(2,'futbol','Pedro Perez');
insert into deportes values(3,'natacion','Marina Morales');
insert into deportes values(4,'tenis','Marina Morales');

--5- Inscriba a varios socios en el mismo deporte en el mismo a�o:
insert into inscriptos values ('22222222',3,'2006','s');
insert into inscriptos values ('23333333',3,'2006','s');
insert into inscriptos values ('24444444',3,'2006','n');

--6- Inscriba a un mismo socio en el mismo deporte en distintos a�os:
insert into inscriptos values ('22222222',3,'2005','s');
insert into inscriptos values ('22222222',3,'2007','n');

--7- Inscriba a un mismo socio en distintos deportes el mismo a�o:
insert into inscriptos values ('24444444',1,'2006','s');
insert into inscriptos values ('24444444',2,'2006','s');

--8- Ingrese una inscripci�n con un c�digo de deporte inexistente y un documento de socio que no exista en "socios":
insert into inscriptos values ('26666666',0,'2006','s');

select * from inscriptos;

--9- Muestre el nombre del socio, el nombre del deporte en que se inscribi� y el a�o empleando diferentes tipos de join (8 filas):
SELECT S.NOMBRE AS NOMBRE_SOCIO, D.NOMBRE AS NOMBRE_DEPORTE, I.A�O
    FROM SOCIOS S JOIN  INSCRIPTOS I ON (S.DOCUMENTO=I.DOCUMENTO) 
            JOIN DEPORTES D ON (I.CODIGODEPORTE=D.CODIGO);

--10- Muestre todos los datos de las inscripciones (excepto los c�digos) incluyendo aquellas inscripciones cuyo c�digo
--de deporte no existe en "deportes" y cuyo documento de socio no se encuentra en "socios" (10 filas)
insert into inscriptos values ('66666666',0,'2006','s');
          
SELECT I.DOCUMENTO, I.A�O, I.MATRICULA
    FROM DEPORTES D,INSCRIPTOS I,SOCIOS S
    WHERE D.CODIGO NOT IN( SELECT D2.CODIGO 
                            FROM DEPORTES D2, INSCRIPTOS I2 
                            WHERE D2.CODIGO=I2.CODIGODEPORTE)
        AND I.Documento NOT IN( SELECT S2.Documento 
                                    FROM SOCIOS S2);
    
--11- Muestre todas las inscripciones del socio con documento "22222222" (3 filas)
SELECT * FROM INSCRIPTOS WHERE DOCUMENTO='22222222';