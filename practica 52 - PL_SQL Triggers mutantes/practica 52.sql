--1. Usando la tabla EMP y DEPT del usuario Scott/tiger realizar un trigger que compruebe cada vez que se 
--realiza un INSERT o UPDate sobre la tabla EMP que no existan m�s de 6 empleados.
create or replace trigger biu_emp
before insert or update of deptno on emp
for each row
declare
vi_total number;
vi_limite constant number:=6;
begin
select count(*) into vi_total from emp where deptno=:new.deptno;
if vi_total>vi_limite then
raise_application_error(-2000,'Accion invalida');
end if;
end;

update emp set deptno=20 where deptno=30;

--2. Con las funciones y procedimientos realizados en clase crea un paquete llamado �mispaquetes�
create or replace package pkg_prueba
is
    FUNCTION Numero_Clientes(numero number) return number;
    PROCEDURE Insertar(ID_Cliente number, Descripcion varchar2);
    procedure pa_empleados_aumentarsueldo (ayear number, aporcentaje number);
    procedure pa_empleados_ingresar(documento char default null, fechaingreso date default sysdate);
    procedure pa_empleados_borrar(doc char);
end pkg_prueba;

create or replace package body pkg_prueba
is
    --FUNCIONES
    FUNCTION Numero_Clientes(numero number)
    RETURN Number
    IS
        result NUMBER;
    BEGIN
        Select COUNT(ID_CLIENTES) INTO result from Clientes;
        return(result);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
        return 0;
    END;
    
    --PROCEDIMIENTOS
    PROCEDURE Insertar(ID_Cliente number,Descripcion varchar2)
    IS
    BEGIN
    Insert into Clientes values(ID_Cliente,Descripcion,null);
    END Insertar;
    
    procedure pa_empleados_aumentarsueldo (ayear number, aporcentaje number)
    is
        current_year number;
    begin
        update empleados set
            sueldo = (aporcentaje*sueldo/100)+sueldo
        where (extract(year from current_date)-extract(year from fechaingreso))>=ayear;
    end pa_empleados_aumentarsueldo;
    
    procedure pa_empleados_ingresar(documento char default null, fechaingreso date default sysdate)
    is
    begin
        insert into empleados values(documento, null, null, null, fechaingreso);
    end pa_empleados_ingresar;
    
    procedure pa_empleados_borrar(doc char)
    is
    begin
        delete from empleados where documento like doc;
    end pa_empleados_borrar;
    
end pkg_prueba;

set SERVEROUTPUT ON;
begin
    dbms_output.put_line(PKG_PRUEBA.NUMERO_CLIENTES(2));
end;





