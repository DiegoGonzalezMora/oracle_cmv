/*Una empresa tiene registrados sus clientes en una tabla llamada "clientes", tambi�n tiene una tabla "provincias"
donde registra los nombres de las provincias.

1- Elimine las tablas "clientes" y "provincias" y cr�elas:*/
drop table clientes; 
drop table provincias;
create table clientes ( codigo number(5), nombre varchar2(30), domicilio varchar2(30), ciudad varchar2(20), codigoprovincia number(2) );
create table provincias( codigo number(2), nombre varchar2(20) ); 
--En este ejemplo, el campo "codigoprovincia" de "clientes" es una clave for�nea, se emplea para enlazar la tabla "clientes" con "provincias".

--2- Intente agregar una restricci�n "foreign key" a la tabla "clientes" que haga referencia al campo "codigo" de "provincias"
ALTER TABLE CLIENTES ADD CONSTRAINT FK_CLIENTES_CODPROVINCIA FOREIGN KEY(CODIGOPROVINCIA) REFERENCES PROVINCIAS(CODIGO);
--No se puede porque "provincias" no tiene restricci�n "primary key" ni "unique".

--3- Establezca una restricci�n "unique" al campo "codigo" de "provincias"
ALTER TABLE PROVINCIAS ADD CONSTRAINT UQ_PROVINCIAS_CODIGO UNIQUE(CODIGO);

--4- Ingrese algunos registros para ambas tablas:
insert into provincias values(1,'Cordoba'); 
insert into provincias values(2,'Santa Fe'); 
insert into provincias values(3,'Misiones'); 
insert into provincias values(4,'Rio Negro'); 
insert into clientes values(100,'Perez Juan','San Martin 123','Carlos Paz',1); 
insert into clientes values(101,'Moreno Marcos','Colon 234','Rosario',2); 
insert into clientes values(102,'Acosta Ana','Avellaneda 333','Posadas',3); 
insert into clientes values(103,'Luisa Lopez','Juarez 555','La Plata',6); 

--5- Intente agregar la restricci�n "foreign key" del punto 2 a la tabla "clientes"
ALTER TABLE CLIENTES ADD CONSTRAINT FK_CLIENTES_CODPROVINCIA FOREIGN KEY(CODIGOPROVINCIA) REFERENCES PROVINCIAS(CODIGO);
--No se puede porque hay un registro en "clientes" cuyo valor de "codigoprovincia" no existe en "provincias".
/*
ORA-02298: cannot validate (SYSTEM.FK_CLIENTES_CODPROVINCIA) - parent keys not found
02298. 00000 - "cannot validate (%s.%s) - parent keys not found"
*Cause:    an alter table validating constraint failed because the table has
           child records.
*Action:   Obvious
*/

--6- Elimine el registro de "clientes" que no cumple con la restricci�n y establezca la restricci�n nuevamente.
UPDATE CLIENTES SET CODIGOPROVINCIA=1 WHERE CODIGOPROVINCIA=6;
ALTER TABLE CLIENTES ADD CONSTRAINT FK_CLIENTES_CODPROVINCIA FOREIGN KEY(CODIGOPROVINCIA) REFERENCES PROVINCIAS(CODIGO);

--7- Intente agregar un cliente con un c�digo de provincia inexistente en "provincias"
INSERT INTO CLIENTES VALUES(104,'Diego Mora', 'Avd Los Limones', 'Sevilla', 18);

--8- Intente eliminar el registro con c�digo 3, de "provincias".
-- No se puede porque hay registros en "clientes" al cual hace referencia.
--ORA-02292: integrity constraint (SYSTEM.FK_CLIENTES_CODPROVINCIA) violated - child record found

--9- Elimine el registro con c�digo "4" de "provincias" Se permite porque en "clientes" ning�n registro hace referencia a �l.
delete from provincias where codigo=4;

--10- Intente modificar el registro con c�digo 1, de "provincias" 
update provincias set codigo=5 where codigo=1;
--No se puede porque hay registros en "clientes" al cual hace referencia.
--ORA-02292: integrity constraint (SYSTEM.FK_CLIENTES_CODPROVINCIA) violated - child record found

--11- Vea las restricciones de "clientes" consultando "user_constraints"
SELECT * FROM USER_CONSTRAINTS WHERE TABLE_NAME='CLIENTES';

--12- Vea las restricciones de "provincias"
SELECT * FROM USER_CONSTRAINTS WHERE TABLE_NAME='PROVINCIAS';

--13- Intente eliminar la tabla "provincias" (mensaje de error)
DROP TABLE PROVINCIAS;
--ORA-02449: unique/primary keys in table referenced by foreign keys

--14- Elimine la restricci�n "foreign key" de "clientes" y luego elimine la tabla "provincias"
ALTER TABLE CLIENTES DROP CONSTRAINT FK_CLIENTES_CODPROVINCIA;
DROP TABLE PROVINCIAS;