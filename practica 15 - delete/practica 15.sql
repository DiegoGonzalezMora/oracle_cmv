--Trabaje con la tabla "agenda" que registra la informaci�n referente a sus amigos.
--1- Elimine la tabla.
drop table agenda;

--2- Cree la tabla con los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), 
--domicilio (cadena de 30) y telefono (cadena de 11):
create table agenda( apellido varchar2(30), nombre varchar2(20), domicilio varchar2(30), telefono varchar2(11) ); 

--3- Ingrese los siguientes registros (insert into):
insert into agenda(apellido,nombre,domicilio,telefono) values ('Alvarez','Alberto','Colon 123','4234567'); 
insert into agenda(apellido,nombre,domicilio,telefono) values ('Juarez','Juan','Avellaneda 135','4458787'); 
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez','Maria','Urquiza 333','4545454'); 
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez','Jose','Urquiza 333','4545454'); 
insert into agenda(apellido,nombre,domicilio,telefono) values ('Salas','Susana','Gral. Paz 1234','4123456');

--4- Elimine el registro cuyo nombre sea "Juan" (1 registro)
delete from agenda where nombre='Juan';

--5- Elimine los registros cuyo n�mero telef�nico sea igual a "4545454" (2 registros)
delete from agenda where telefono='4545454';

--6- Elimine todos los registros (2 registros)
delete from agenda;
