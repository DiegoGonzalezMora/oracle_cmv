/*1. Crea un tablespace Trabajo asociado a dos ficheros con una capacidad total de 15M. Para ello se va a crear un tablespace 
�Trabajo� que est� formado por 2 ficheros, y con valor inicial, siguiente y porcentaje de incremento.*/
create tablespace Trabajo DATAFILE 'Archivo1.dbf' SIZE 6M, 'Archivo2.dbf' SIZE 6M DEFAULT STORAGE (INITIAL 10 NEXT 10 PCTINCREASE 25);

/*2. Visualice los tablespaces creados.*/
SELECT * FROM DBA_TABLESPACES;

/*3. Visualice las tablas creadas en el tablespace.*/
SELECT * FROM DBA_DATA_FILES;

/*4. Desactiva el tablespace trabajo.*/
ALTER TABLESPACE TRABAJO OFFLINE;

ALTER TABLESPACE TRABAJO ONLINE;

/*5. A�adir un fichero de 6 Mb al tablespace Trabajo*/
ALTER TABLESPACE TRABAJO ADD DATAFILE 'Archivo3' SIZE 6M;

/*6. Crea un usuario llamado �miusuario� que se identifique con la clave �miclavesecreta� en el tablespace trabajo.*/
CREATE USER misuario IDENTIFIED BY miclavesecreta DEFAULT TABLESPACE Trabajo;

/*7. Crea un rol llamado �Programador�*/
CREATE ROLE Programador;

/*8. Concede permisos al rol de programador para abrir sesi�n, para crear (tablas,vistas ,procedimientos y sin�nimos),
para modificar (tablas,vistas,procedimientos,sin�nimos),para borrar (tablas,vistas,procedimientos,sin�nimos).*/
GRANT CREATE SESSION, CREATE TABLE, CREATE VIEW, CREATE PROCEDURE, 
    CREATE SYNONYM, DROP ANY TABLE, ALTER ANY TABLE TO Programador;
    
/*9. Conceder permisos de programador al usuario creado anteriormente.*/
GRANT Programador TO misuario;

/*10. Borrar tablespace trabajo.*/
DROP TABLESPACE Trabajo;