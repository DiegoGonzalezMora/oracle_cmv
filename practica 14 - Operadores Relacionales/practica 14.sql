--Un comercio que vende art�culos de computaci�n registra los datos de sus art�culos en una tabla con ese nombre.
--1- Elimine "articulos"
drop table articulos;

--2- Cree la tabla, con la siguiente estructura:
create table articulos( codigo number(5), nombre varchar2(20), descripcion varchar2(30), precio number(6,2), cantidad number(3) ); 

--3- Vea la estructura de la tabla.
describe articulos;

--4- Ingrese algunos registros:
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (1,'impresora','Epson Stylus C45',400.80,20); 
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (2,'impresora','Epson Stylus C85',500,30); 
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (3,'monitor','Samsung 14',800,10);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (4,'teclado','ingles Biswal',100,50);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (5,'teclado','espa�ol Biswal',90,50);

--5- Seleccione los datos de las impresoras (2 registros)
select * from articulos where nombre='impresora';

--6- Seleccione los art�culos cuyo precio sea mayor o igual a 400 (3 registros)
select * from articulos where precio>=400;

--7- Seleccione el c�digo y nombre de los art�culos cuya cantidad sea menor a 30 (2 registros)
select codigo, nombre from articulos where cantidad<30;

--8- Selecciones el nombre y descripci�n de los art�culos que NO cuesten 100 (4 registros)
select nombre, descripcion from articulos where precio!=100;

