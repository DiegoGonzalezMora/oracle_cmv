/*Una empresa registra los datos de sus empleados en una tabla llamada "empleados".

1- Elimine la tabla "empleados":*/
drop table empleados; 

--2- Cree la tabla:
create table empleados( legajo number(3), documento char(8) not null, nombre varchar2(30) not null, primary key(legajo) );

--3- Elimine la secuencia "sec_legajoempleados" y luego cr�ela estableciendo el valor m�nimo (1), m�ximo (999), 
--valor inicial (100), valor de incremento (2) y no circular. Finalmente inicialice la secuencia.
drop sequence sec_legajoempleados;
create sequence sec_legajoempleados start with 100 minvalue 1 maxvalue 999 increment by 2 nocycle;

--4- Ingrese algunos registros, empleando la secuencia creada para los valores de la clave primaria:
insert into empleados values (sec_legajoempleados.currval,'22333444','Ana Acosta');
insert into empleados values (sec_legajoempleados.nextval,'23444555','Betina Bustamante');
insert into empleados values (sec_legajoempleados.nextval,'24555666','Carlos Caseros');
insert into empleados values (sec_legajoempleados.nextval,'25666777','Diana Dominguez');
insert into empleados values (sec_legajoempleados.nextval,'26777888','Estela Esper');

--5- Recupere los registros de "libros" para ver los valores de clave primaria.
--Note que los valores se incrementaron en 2, porque as� se estableci� el valor de incremento al crear la secuencia.
select uc.table_name, column_name 
    from user_cons_columns ucc 
        join user_constraints uc on ucc.constraint_name=uc.constraint_name 
    where uc.constraint_type='P' and uc.table_name='EMPLEADOS';

--6- Vea el valor actual de la secuencia empleando la tabla "dual". Retorna 108.
select sec_legajoempleados.currval from dual;

--7- Recupere el valor siguiente de la secuencia empleando la tabla "dual" Retorna 110.
select sec_legajoempleados.nextval from dual;

--8- Ingrese un nuevo empleado (recuerde que la secuencia ya tiene el pr�ximo valor, emplee "currval" para almacenar el valor de legajo)
insert into empleados values (sec_legajoempleados.currval,'26777888','Diego Mora');

--9- Recupere los registros de "libros" para ver el valor de clave primaria ingresado anteriormente.select legajo from empleados;
select legajo from empleados;

--10- Incremente el valor de la secuencia empleando la tabla "dual" (retorna 112)
select sec_legajoempleados.nextval from dual;

--11- Ingrese un empleado con valor de legajo "112".
insert into empleados values (sec_legajoempleados.currval,'26777889','Ana Sanchez');

--12- Intente ingresar un registro empleando "currval":
insert into empleados values (sec_legajoempleados.currval,'29000111','Hector Huerta'); 
--Mensaje de error porque el legajo est� repetido y la clave primaria no puede repetirse.
/*
Error que empieza en la l�nea: 44 del comando :
insert into empleados values (sec_legajoempleados.currval,'29000111','Hector Huerta')
Informe de error -
ORA-00001: unique constraint (SYSTEM.SYS_C007132) violated
*/

--13- Incremente el valor de la secuencia. Retorna 114.
select sec_legajoempleados.nextval from dual;

--14- Ingrese el registro del punto 11.
insert into empleados values (sec_legajoempleados.currval,'26777889','Ana Sanchez');
--Ahora si lo permite, pues el valor retornado por "currval" no est� repetido en la tabla "empleados".

--15- Recupere los registros.
select * from empleados;

--16- Vea las secuencias existentes y analice la informaci�n retornada.
select * from dba_sequences;
--Debe aparecer "sec_legajoempleados".
--SYSTEM	SEC_LEGAJOEMPLEADOS	1	999	2	N	N	20	140

--17- Vea todos los objetos de la base de datos actual que contengan en su nombre la cadena "EMPLEADOS".
--Debe aparacer la tabla "empleados" y la secuencia "sec_legajoempleados".
select * from dba_objects where object_name like '%EMPLEADOS%';

--18- Elimine la secuencia creada.
drop sequence sec_legajoempleados;

--19- Consulte todos los objetos de la base de datos que sean secuencias y verifique que "sec_legajoempleados" ya no existe.
select * from dba_sequences where SEQUENCE_NAME='SEC_LEGAJOEMPLEADOS';
