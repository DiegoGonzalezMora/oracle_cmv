--Usando la base de datos Empresa realizar las siguientes consultas:
--1. Hallar, para cada departamento, el salario medio, el m�nimo y el m�ximo.
SELECT D.NUMERO, FLOOR(AVG(E.SALARIO)) AS SALARIO_MEDIO, MAX(E.SALARIO) AS MAX, MIN(E.SALARIO) AS MIN 
    FROM DEPARTAMENTOS D JOIN EMPLEADOS E ON(D.NUMERO=E.DEPARTAMENTO)
    GROUP BY D.NUMERO;

--2. Agrupando por departamento y n�mero de hijos ,hallar cu�ntos empleados hay en cada grupo.
SELECT D.NUMERO, E.NUM_HIJOS, COUNT(*) AS NUM_EMPLEADOS
    FROM DEPARTAMENTOS D JOIN EMPLEADOS E ON(D.NUMERO=E.DEPARTAMENTO)
    GROUP BY D.NUMERO, E.NUM_HIJOS;

--3. Para cada extensi�n telef�nica, hallar cu�ntos empleados la usan y el salario medio de �stos.
SELECT E.TELEFONO, COUNT(*) AS NUM_EMPLEADOS, FLOOR(AVG(E.SALARIO)) AS SALARIO_MEDIO
    FROM EMPLEADOS E
    GROUP BY E.TELEFONO;

--4. Hallar el salario m�ximo y el m�nimo para cada grupo de empleados con igual n�mero de hijos y que tienen 
--al menos uno, pero s�lo si hay m�s de un empleado en el grupo y el salario m�ximo del grupo excede de 200 euros.
SELECT NUM_HIJOS, COUNT(*) NUM_EMPLEADOS, MAX(SALARIO), MIN(SALARIO)
    FROM EMPLEADOS
    WHERE NUM_HIJOS>=1
    GROUP BY NUM_HIJOS
    HAVING MAX(SALARIO)>200 AND COUNT(*)>1;

--5. Para los departamentos en los que hay alg�n empleado cuyo salario sea superior a 400 euros mensuales hallar el 
--n�mero de empleados y la suma de sus salarios ,comisiones y n�meros de hijos.    
SELECT D.NOMBRE, COUNT(E.NUMERO) NUM_EMPLEADOS, SUM(E.SALARIO), SUM(E.COMISION), SUM(E.NUM_HIJOS)
    FROM EMPLEADOS E JOIN DEPARTAMENTOS D ON(E.DEPARTAMENTO=D.NUMERO)
    WHERE E.SALARIO = SOME (SELECT E1.SALARIO FROM EMPLEADOS E1 WHERE E1.SALARIO>400)
    GROUP BY D.NOMBRE;   

--6. Hallar,por orden creciente,los n�meros de extensiones telef�nicas de los departamentos que tienen m�s de dos y que
--son compartidas por menos de cuatro empleados,excluyendo las que no son compartidas.
SELECT TELEFONO
    FROM EMPLEADOS
    WHERE DEPARTAMENTO in (SELECT DEPARTAMENTO 
                                FROM EMPLEADOS 
                                group BY DEPARTAMENTO 
                                having count (distinct TELEFONO) > 2
                            )
    GROUP BY TELEFONO
    having count (DEPARTAMENTO) BETWEEN 2 AND 3
    ORDER BY TELEFONO ASC;
    
select e.telefono 
    from empleados e 
    where e.telefono in (select e2.telefono 
                            from empleados e2 
                            group by e2.departamento, e2.telefono 
                            having count()<4 and count()>1) 
    group by e.departamento,e.telefono having count(*)>2 
    order by e.telefono;

/*
SELECT TELEFONO
    FROM EMPLEADOS E JOIN DEPARTAMENTOS D ON(E.DEPARTAMENTO=D.NUMERO)
    GROUP BY DEPARTAMENTO, TELEFONO 
    HAVING COUNT(TELEFONO)>2
    ORDER BY TELEFONO ASC  
INTERSECT
SELECT E.TELEFONO
    FROM EMPLEADOS E JOIN DEPARTAMENTOS D ON(E.DEPARTAMENTO=D.NUMERO)
    GROUP BY E.TELEFONO 
    HAVING COUNT(E.NUMERO)>4
    ORDER BY TELEFONO ASC;*/

--7. Obtener,por orden alfabetico, los nombres de los empleados cuyos sueldos superan al del empleado n�mero 420 en un 50%.
SELECT SUBSTR(nombre, INSTR(nombre, ' ')+1) NOMBRE
    FROM EMPLEADOS E
    WHERE E.SALARIO > (SELECT (SALARIO*50)/100 FROM EMPLEADOS WHERE NUMERO = 420)
    ORDER BY E.NOMBRE;

--8. Obtener,por orden alfab�tico,los nombres de los empleados cuyo salario supera a todos los salarios de los 
--empleados del departamento 122
SELECT SUBSTR(nombre, INSTR(nombre, ' ')+1) NOMBRE
    FROM EMPLEADOS E
    WHERE E.SALARIO > (SELECT SUM(SALARIO) FROM EMPLEADOS WHERE DEPARTAMENTO=122)
    ORDER BY E.NOMBRE;

--9. Repetir la consulta anterior para un departamento inexistente,por ejemplo el 150.�Cual es el resultado?
SELECT SUBSTR(nombre, INSTR(nombre, ' ')+1) NOMBRE
    FROM EMPLEADOS E
    WHERE E.SALARIO > (SELECT SUM(SALARIO) FROM EMPLEADOS WHERE DEPARTAMENTO=150)
    ORDER BY E.NOMBRE;
--NINGUNO PORQUE COMPARAMOS CON UN NULL

--10. Obtener, por orden alfab�tico,los nombres de los empleados cuyo salario es igual o superior al menor de 
--los salarios de los empleados del departamento 122.
SELECT SUBSTR(nombre, INSTR(nombre, ' ')+1) NOMBRE
    FROM EMPLEADOS E
    WHERE E.SALARIO >= (SELECT MIN(SALARIO) FROM EMPLEADOS WHERE DEPARTAMENTO=122)
    ORDER BY E.NOMBRE;

--11. Obtener , por orden alfab�tico,los nombres y los salarios de los empleados cuyo salario coincide con la 
--comisi�n de cualquier otro o la suya propia.
SELECT SUBSTR(nombre, INSTR(nombre, ' ')+1) NOMBRE, E.SALARIO
    FROM EMPLEADOS E
    WHERE E.SALARIO = SOME (SELECT COMISION FROM EMPLEADOS)
    ORDER BY E.NOMBRE;

--12. Obtener por orden alfab�tico, los nombres y los salarios de los empleados cuyo salario es inferior a 
--la m�s alta comisi�n existente.
SELECT SUBSTR(nombre, INSTR(nombre, ' ')+1) NOMBRE, E.SALARIO
    FROM EMPLEADOS E
    WHERE E.SALARIO < (SELECT MAX(COMISION) FROM EMPLEADOS)
    ORDER BY E.NOMBRE;

--13. Obtener los nombres de los centros de trabajo, si hay alguno ,que est�n en Madrid.
SELECT NOMBRE FROM CENTROS WHERE DIRECCION LIKE '%MADRID%';

--14. Obtener, por orden alfab�tico, los nombres y las comisiones de los empleados del departamento 110, si es 
--que hay en �l alg�n empleado que tenga comisi�n.    
SELECT SUBSTR(nombre, INSTR(nombre, ' ')+1) NOMBRE, E.COMISION
    FROM EMPLEADOS E
    WHERE E.DEPARTAMENTO=110 AND COMISION IS NOT NULL
    ORDER BY E.NOMBRE;
    


--15. Obtener, por orden alfab�tico, los nombres y los salarios de los empleados de los departamentos 110 y 111 que, 
--o bien no tengan hijos o bien su salario por hijo supere los 602 euros, si hay alguno sin comisi�n en los departamentos 111 � 112.
SELECT E.NOMBRE, E.SALARIO
    FROM EMPLEADOS E
    WHERE E.DEPARTAMENTO IN(110, 111)
        AND (E.NUM_HIJOS=0 OR E.SALARIO/E.NUM_HIJOS>602)
        AND (COMISION IS NULL) AND DEPARTAMENTO IN (111,112)
    ORDER BY E.NOMBRE;

--16. Hallar el salario medio, el m�nimo, el m�ximo y el promedio de estos dos para el colectivo de todos los empleados.
SELECT FLOOR(AVG(SALARIO)), MAX(SALARIO), MIN(SALARIO), (MAX(SALARIO) + MIN(SALARIO))/2
    FROM EMPLEADOS;

--17. Obtener , por orden alfab�tico, los salarios y nombres de los empleados cuyo salario se diferencia con el m�ximo 
--en menos de un 40% de �ste
select salario, nombre
    from empleados 
    where salario > (select (max(salario)*40)/100 from empleados) 
        or salario < (select (max(salario)*40)/100 from empleados);

--18. Hallar, la masa salarial (salarios + comisiones) anual de la empresa
select sum((salario+comision)*12) masa_salarial_anual
    from empleados;

--19. Hallar el salario medio de los empleados cuyo salario no supera en m�s del 20% al salario m�nimo de los empleados 
--que tienen alg�n hijo y su salario medio por hijo es mayor que 600 euros
select avg(salario) as salariomedio 
    from empleados 
    where salario < (select 1.2*min(salario) 
                        From empleados 
                        where num_hijos>=1 and salario*num_hijos > 600
                    );
------
select min(salario) from empleados where NUM_HIJOS>0 and (salario/NUM_HIJOS)>600;
select avg(salario)
    from empleados
    where salario <= (salario + (select (20 * min(salario))/100 
                                    from empleados 
                                    where NUM_HIJOS>0 and (salario/NUM_HIJOS)>600)
                        );

--20. Hallar la diferencia entre los salarios m�s alto y m�s bajo de la empresa
select max(salario)-min(salario) from empleados;

--21. Hallar el presupuesto medio de los departamentos cuyo presupuesto supera el promedio de los presupuestos
select avg(salario)
    from empleados
    where salario > (select avg(salario) from empleados);

--22. Agrupando por n�mero de hijos,hallar la media por hijo del total del salario y de comisi�n
select num_hijos, floor(avg((salario+nvl(comision,0))/replace(num_hijos,0,1))) as media
    from empleados
    group by num_hijos
    order by num_hijos;

--23. Para cada departamento hallar la media de la comisi�n con respecto a los empleados que la reciben y con 
--respecto al total de los empleados.
select e.departamento, floor(avg(nvl(comision,0))) media_empleados_comision, 
            (select floor(avg(comision)) 
                    from empleados 
                    where departamento=e.departamento and comision is not null ) media_empleados_total 
    FROM empleados e
    group by e.departamento
    order by e.departamento;

--24. Hallar el salario medio por departamentos para aquellos departamentos cuyo salario m�ximo es inferior 
--al salario medio de todos los empleados
select departamento, floor(avg(salario)) media
    from empleados
    group by departamento
    having max(salario) < (select avg(e1.salario) from empleados e1);

--25. Para los departamentos cuyo salario medio supera al promedio de la empresa.Hallar cu�ntas extensiones 
--telef�nicas tienen.
select departamento, count(distinct telefono) extensiones
    from empleados
    group by departamento
    having avg(salario) > (select avg(salario) from empleados);

--26. Obtener un listado con los nombres de los empleados junto al nombre del departamento en el que trabaja.
SELECT DEPARTAMENTO, NOMBRE
    FROM EMPLEADOS
    GROUP BY DEPARTAMENTO, NOMBRE
    ORDER BY DEPARTAMENTO;

--27. Obtener un listado que incluya el nombre de cada uno de los departamentos junto al nombre de su director
SELECT D.NUMERO, E.NOMBRE
    FROM DEPARTAMENTOS D JOIN EMPLEADOS E ON(D.NUMERO=E.DEPARTAMENTO)
    WHERE D.DIRECTOR=E.NUMERO
    GROUP BY D.NUMERO, E.NOMBRE;
    
--28. Obtener un listado alfab�tico de todos los trabajadores ,agrupados por centros
SELECT C.NOMBRE, E.NOMBRE
    FROM EMPLEADOS E JOIN DEPARTAMENTOS D ON(E.DEPARTAMENTO=D.NUMERO)
        JOIN CENTROS C ON (D.CENTRO=C.NUMERO)
    GROUP BY C.NOMBRE, E.NOMBRE
    ORDER BY C.NOMBRE;

--29. Obtener un listado que incluya el nombre de cada uno de los empleados junto al nombre de su departamento,
--el nombre de su jefe y la direcci�n de su centro de trabajo.
SELECT DISTINCT E.NOMBRE NOMBRE, D.NOMBRE DEPARTAMENTO, (SELECT E1.NOMBRE FROM EMPLEADOS E1 WHERE E1.NUMERO=D.DIRECTOR) DIRECTOR, C.NOMBRE
    FROM EMPLEADOS E JOIN DEPARTAMENTOS D ON(E.DEPARTAMENTO=D.NUMERO)
        JOIN CENTROS C ON (D.CENTRO=C.NUMERO)
        ORDER BY E.NOMBRE;