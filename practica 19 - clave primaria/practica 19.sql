--Trabaje con la tabla "libros" de una librer�a.
--1- Elimine la tabla:
drop table libros;

--2- Cr�ela con los siguientes campos, estableciendo como clave primaria el campo "codigo":
create table libros( codigo number(4) not null, titulo varchar2(40) not null, autor varchar2(20), 
editorial varchar2(15), primary key (codigo) ); 

--3- Ingrese los siguientes registros:
insert into libros (codigo,titulo,autor,editorial) values (1,'El aleph','Borges','Emece'); 
insert into libros (codigo,titulo,autor,editorial) values (2,'Martin Fierro','Jose Hernandez','Planeta'); 
insert into libros (codigo,titulo,autor,editorial) values (3,'Aprenda PHP','Mario Molina','Nuevo Siglo'); 

--4- Ingrese un registro con c�digo repetido (aparece un mensaje de error)
insert into libros (codigo,titulo,autor,editorial) values (3,'Aprenda PHP','Mario Molina','Nuevo Siglo'); 

--5- Intente ingresar el valor "null" en el campo "codigo"
insert into libros (codigo,titulo,autor,editorial) values (null,'Aprenda C#','',''); 

--6- Intente actualizar el c�digo del libro "Martin Fierro" a "1" (mensaje de error)
insert into libros (codigo,titulo,autor,editorial) values (1,'Martin Fierro','Jose Hernandez','Planeta'); 

--7- Actualice el c�digo del libro "Martin Fierro" a "10"
insert into libros (codigo,titulo,autor,editorial) values (10,'Martin Fierro','Jose Hernandez','Planeta'); 

--8- Vea qu� campo de la tabla "LIBROS" fue establecido como clave primaria
select uc.table_name, column_name from user_cons_columns ucc join user_constraints uc 
on ucc.constraint_name=uc.constraint_name where uc.constraint_type='P' and uc.table_name='LIBROS';

--9- Vea qu� campo de la tabla "libros" (en min�sculas) fue establecido como clave primaria
--La tabla aparece vac�a porque Oracle no encuentra la tabla "libros", ya que almacena los nombres 
--de las tablas con may�sculas.
select uc.table_name, column_name from user_cons_columns ucc join user_constraints uc 
on ucc.constraint_name=uc.constraint_name where uc.constraint_type='P' and uc.table_name='libros';

