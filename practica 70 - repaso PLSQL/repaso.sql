create view vista_empleados as
    select e.nombre as nombre, D.nombre as seccion, E.NUM_hijos
   from empleados e join DEPARTAMENTOS D on (E.DEPARTAMENTO=D.NUMERO);
   
--Para ver la informaci�n contenida en la vista creada anteriormente tipeamos:
select * from vista_empleados;
 
--Podemos realizar consultas a una vista como si se tratara de una tabla:
select seccion, count(*) as cantidad
  from vista_empleados
  GROUP BY SECCION;

---

create view vista_empleados_ingreso (fecha,cantidad)
as
    select extract(year from fecha_ingreso),count(*)
        from empleados
        group by extract(year from fecha_ingreso);
SELECT * FROM vista_empleados_ingreso;

--1. Cree una vista llamada INF_GENERAL que contiene el n�mero total de empleados,
--el de departamentos, el de centros, el n�mero de jefes, la fecha en que se incorpor� el
--primer empleado a la empresa y la fecha del �ltimo que lo ha hecho y el total de lo
--gastado por la empresa en el pago de comisiones.
--Ahora cree una tabla INF_GRAL_FISICA que sirva para almacenar estos datos. Cree
--un bloque PL/SQL que realice una consulta a la base de datos para cada uno de estos
--atributos y vaya almacenando los valores en variables para, finalmente hacer una
--inserci�n de todos los datos en la tabla.
ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY'; 

CREATE VIEW INF_GENERAL (NUM_EMPLEADOS, NUM_DEPARTAMENTOS, NUM_CENTROS, NUM_JEFES, PRIMERA_INCORPORACION, ULTIMA_INCORPORACION, COMISIONES)
AS
SELECT COUNT(E.NUMERO), COUNT(DISTINCT E.DEPARTAMENTO), COUNT(DISTINCT C.NUMERO), COUNT(DISTINCT D.DIRECTOR),  
       MAX(E.FECHA_INGRESO), MIN(E.FECHA_INGRESO), SUM(E.COMISION)
    FROM EMPLEADOS E JOIN DEPARTAMENTOS D ON(E.DEPARTAMENTO=D.NUMERO) JOIN CENTROS C ON(C.NUMERO=D.CENTRO);
    
DESCRIBE INF_GENERAL;

CREATE TABLE INF_GRAL_FISICA(NUM_EMPLEADOS NUMBER(4), NUM_DEPARTAMENTOS NUMBER(3), NUM_CENTROS NUMBER(3), 
    NUM_JEFES NUMBER(3), PRIMERA_INCORPORACION DATE, ULTIMA_INCORPORACION DATE, COMISIONES  NUMBER(5));

create or replace procedure PA_INFO
as
CURSOR CINF IS SELECT * FROM INF_GENERAL;
RINF CINF%ROWTYPE;

BEGIN
    OPEN CINF;
    LOOP
        FETCH CINF INTO RINF;
        EXIT WHEN CINF%NOTFOUND;
        INSERT INTO INF_GRAL_FISICA VALUES(RINF.NUM_EMPLEADOS, RINF.NUM_DEPARTAMENTOS, RINF.NUM_CENTROS, 
            RINF.NUM_JEFES, RINF.PRIMERA_INCORPORACION, RINF.ULTIMA_INCORPORACION, RINF.COMISIONES);
    END LOOP;
    CLOSE CINF;
END;

BEGIN
    PA_INFO();
END;

SELECT * FROM INF_GRAL_FISICA;

--2. Cree una vista llamada INF_EMPLEADOS que contiene la informaci�n del nombre
--de cada empleado, su fecha de nacimiento y el c�digo de su departamento.
--Haga lo mismo creando f�sicamente una tabla, llamada INF_EMP_FISICA y
--volcando la informaci�n mediante la sentencia INSERT INTO � SELECT �;
--Compruebe que la informaci�n de la tabla y la vista es la misma usando el operador
--MINUS.
--Por �ltimo, borre todas las tuplas que hay en INF_EMP_FISICA y dise�e un bloque
--PL/SQL que realice la misma funci�n que el INSERT anterior pero usando un cursor
--de lectura que lee sobre la tabla empleados. Vuelva a comprobar que existe la misma
--informaci�n en la tabla y en la vista
CREATE VIEW INF_EMPLEADOS AS SELECT NOMBRE, FECHA_NACIMIENTO, DEPARTAMENTO FROM EMPLEADOS;

CREATE TABLE INF_EMP_FISICA(NOMBRE VARCHAR(30), FECHA_NACIMIENTO DATE, DEPARTAMENTO NUMBER(3));

INSERT INTO INF_EMP_FISICA VALUES('PONS, CESAR','10/11/2029',121);
INSERT INTO INF_EMP_FISICA VALUES('LASA, MARIO','09/06/2035',112);

SELECT * FROM INF_EMPLEADOS
MINUS
SELECT * FROM INF_EMP_FISICA;

create or replace procedure PA_INFO2
as 
CURSOR CINF IS SELECT * FROM INF_EMPLEADOS;
RINF CINF%ROWTYPE;

BEGIN
    OPEN CINF;
    LOOP
        FETCH CINF INTO RINF;
        EXIT WHEN CINF%NOTFOUND;
        INSERT INTO INF_EMP_FISICA VALUES(RINF.NOMBRE,RINF.FECHA_NACIMIENTO,RINF.DEPARTAMENTO);
    END LOOP;
    CLOSE CINF;
END;

DELETE FROM INF_EMP_FISICA;
BEGIN
    PA_INFO2();
END;

SELECT * FROM INF_EMPLEADOS
MINUS
SELECT * FROM INF_EMP_FISICA;

--3. Cree una tabla HISTORIAL con el siguiente esquema:
--En estas tuplas se observa que ambos empleados han tenido una serie de trabajos.
--Consideramos que las tuplas cuya fecha de fin es nula se corresponden con los trabajos
--actuales. Perfile el modelo para que cumpla lo siguiente:
--� La fecha de inicio ha de ser anterior a la de fin. (use un CHECK).
--� Los atributos EMPLEADO y FECHA_INICIO forman la clave primaria.
--Con la segunda restricci�n pretendemos asegurar que cada empleado en cada momento
--s�lo tiene un trabajo. Sin embargo, observe que las dos primeras tuplas representan dos
--trabajos simult�neos si bien la tabla cumple las restricciones impuestas.
--Cree un bloque PL/SQL que revise todas las tuplas de esta tabla y nos avise cuando
--haya dos trabajos que se desarrollan simult�neamente. El mensaje de error se guarda en
--una tabla HISTORIAL_ERROR que tiene como atributos el c�digo del empleado
--afectado y las cuatro fechas que determinan los intervalos solapados (fecha de inicio y
--de fin de los dos trabajos afectados).
DROP TABLE HISTORIAL;
CREATE TABLE HISTORIAL (EMPLEADO NUMBER(38), FEC_INICIO DATE, FEC_FIN DATE, TRABAJO VARCHAR2(255))

INSERT INTO HISTORIAL VALUES(110,'24/7/2000','24/8/2000','INFORME DE ANALISIS DE RIESGO');
INSERT INTO HISTORIAL VALUES(110,'01/8/2000','10/8/2000','SUPLENCIA DEL EMPLEADO 120');
INSERT INTO HISTORIAL VALUES(110,'25/8/2000', NULL,'DESARROLLO DE PROGRAMA EMERGENCIAS');
INSERT INTO HISTORIAL VALUES(120,'24/7/1999','23/7/2000','DESARROLLO DE PROGRAMA DE GESTION');
INSERT INTO HISTORIAL VALUES(120,'24/7/2000', NULL,'COMERCIAL PROGRAMA GESTION');

ALTER TABLE HISTORIAL ADD CONSTRAINT CK_HISTORIAL_FECHA CHECK(FEC_INICIO<FEC_FIN);
ALTER TABLE HISTORIAL ADD CONSTRAINT PK_HISTORIAL PRIMARY KEY(EMPLEADO,FEC_INICIO);

DROP TABLE HISTORIAL_ERROR;
CREATE TABLE HISTORIAL_ERROR (EMPLEADO VARCHAR2(38), FEC_INI_T1 DATE, FEC_FIN_T1 DATE, FEC_INI_T2 DATE, FEC_FIN_T2 DATE);

CREATE OR REPLACE PROCEDURE PA_CHECK_HISTORIAL
AS 
CURSOR CT1 IS SELECT EMPLEADO, FEC_INICIO, NVL(FEC_FIN, SYSDATE) AS FEC_FIN FROM HISTORIAL;
RT1 CT1%ROWTYPE;

CURSOR CT2 IS SELECT EMPLEADO, FEC_INICIO, NVL(FEC_FIN, SYSDATE) AS FEC_FIN FROM HISTORIAL;
RT2 CT2%ROWTYPE;

BEGIN
    OPEN CT1;
    LOOP
        FETCH CT1 INTO RT1;
        EXIT WHEN CT1%NOTFOUND;
        OPEN CT2;
        LOOP
            FETCH CT2 INTO RT2;
            EXIT WHEN CT2%NOTFOUND;
            
            IF RT1.EMPLEADO = RT2.EMPLEADO AND RT1.FEC_INICIO=RT2.FEC_INICIO AND RT1.FEC_FIN=RT2.FEC_FIN THEN
                FETCH CT2 INTO RT2;
                EXIT WHEN CT2%NOTFOUND;
            END IF;
            
            IF RT1.EMPLEADO = RT2.EMPLEADO AND RT2.FEC_INICIO BETWEEN RT1.FEC_INICIO AND RT1.FEC_FIN THEN
                INSERT INTO HISTORIAL_ERROR VALUES(RT1.EMPLEADO, RT1.FEC_INICIO, 
                                RT1.FEC_FIN, RT2.FEC_INICIO, RT2.FEC_FIN);
            END IF;
        END LOOP;
        CLOSE CT2;
    END LOOP;
    CLOSE CT1;
END;

DELETE FROM HISTORIAL_ERROR;

BEGIN
PA_CHECK_HISTORIAL();
END;

SELECT * FROM HISTORIAL_ERROR;
    

--4. Cree un bloque PL/SQL que recibe los datos de un nuevo contrato y comprueba si
--se dan las restricciones del enunciado anterior. Si no es as�, inserta el empleado pero
--poniendo la fecha del contrato anterior al d�a de ayer (uno menos que SYSDATE).
SELECT SYSDATE - 1 FROM DUAL;

create or replace trigger BI_HISTORIAL
BEFORE insert on HISTORIAL
for each row
declare
    CURSOR CT1 IS SELECT EMPLEADO, FEC_INICIO, NVL(FEC_FIN, SYSDATE) AS FEC_FIN FROM HISTORIAL;
    RT1 CT1%ROWTYPE;
    TRABAJOSOLAPADO BOOLEAN;
    EMPLEADOEXISTE BOOLEAN;
begin
    TRABAJOSOLAPADO:=FALSE;
    EMPLEADOEXISTE:=FALSE;
    OPEN CT1;
    LOOP
        FETCH CT1 INTO RT1;
        EXIT WHEN CT1%NOTFOUND;

        IF :NEW.EMPLEADO=RT1.EMPLEADO THEN EMPLEADOEXISTE:=TRUE; END IF;

        IF :NEW.FEC_INICIO BETWEEN RT1.FEC_INICIO AND RT1.FEC_FIN THEN
            TRABAJOSOLAPADO:=TRUE;
        END IF;
    END LOOP;
    CLOSE CT1;

    IF EMPLEADOEXISTE = FALSE THEN
        INSERT INTO HISTORIAL VALUES(:NEW.EMPLEADO, :NEW.FEC_INICIO - 1, :NEW.FEC_FIN, :NEW.TRABAJO);
        RETURN;
    END IF;
    
    IF  EMPLEADOEXISTE = TRUE AND TRABAJOSOLAPADO = FALSE THEN
        INSERT INTO HISTORIAL VALUES(:NEW.EMPLEADO, :NEW.FEC_INICIO - 1, :NEW.FEC_FIN, :NEW.TRABAJO);
    ELSE RAISE_APPLICATION_ERROR(-20201, 'FECHA INICIO INVALIDA');
    END IF;
end;

INSERT INTO HISTORIAL VALUES(120, '16/12/2017', NULL, 'LIMPIAR CHIRINGUITO');
