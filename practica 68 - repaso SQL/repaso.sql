drop table empleados;
drop table departamentos;
drop table centros;

create table centros (numero number(2), nombre varchar2(40), direccion varchar2(50), primary key(numero)
);

create table departamentos(numero number(3), centro number(3), director number(3), tipo_dir char(1), presupuesto number(2),
                dpto_jefe number(3), nombre varchar2(40), primary key(numero), foreign key (centro) references centros(numero));

create table empleados (numero number(3), departamento number(3), telefono number(3), fecha_nacimiento date, fecha_ingreso date,
                salario number(3), comision number(3), num_hijos number(1), nombre varchar2(50), primary key(numero),
                foreign key(departamento) references departamentos(numero));

alter session set nls_date_language = 'ENGLISH';
alter session set nls_date_format = 'DD-MON-YY';

-- Inserci�n de valores en la tabla CENTROS:
insert into CENTROS values (10,  'SEDE CENTRAL', 'C. ALCALA 820, MADRID');
insert into CENTROS values (20,  'RELACION CON CLIENTES', 'C. ATOCHA 405, MADRID');

-- Inserci�n de valores en la tabla DEPARTAMENTOS:
insert into DEPARTAMENTOS values (100, 10, 260, 'P', 12, null, 'DIRECCION GENERAL');
insert into DEPARTAMENTOS values (110, 20, 180, 'P', 15, 100, 'DIRECC. COMERCIAL');
insert into DEPARTAMENTOS values (111, 20, 180, 'F', 11, 110, 'SECTOR INDUSTRIAL');
insert into DEPARTAMENTOS values (112, 20, 270, 'P', 9, 100, 'SECTOR SERVICIOS');
insert into DEPARTAMENTOS values (120, 10, 150, 'F', 3, 100, 'ORGANIZACION');
insert into DEPARTAMENTOS values (121, 10, 150, 'P', 2, 120, 'PERSONAL');
insert into DEPARTAMENTOS values (122, 10, 350, 'P', 6, 120, 'PROCESO DE DATOS');
insert into DEPARTAMENTOS values (130, 10, 310, 'P', 2, 100, 'FINANZAS');

-- Inserci�n de valores en la tabla EMPLEADOS:
insert into EMPLEADOS values (110, 121, 350, '10-NOV-29', '10-FEB-50', 310, null, 3, 'PONS, CESAR');
insert into EMPLEADOS values (120, 112, 840, '09-JUN-35', '01-OCT-68', 350, 110, 1, 'LASA, MARIO');
insert into EMPLEADOS values (130, 112, 810, '09-NOV-45', '01-FEB-69', 290, 110, 2, 'TEROL, LUCIANO');
insert into EMPLEADOS values (150, 121, 340, '10-AUG-30', '15-JAN-48', 440, null, 0, 'PEREZ, JULIO');
insert into EMPLEADOS values (160, 111, 740, '09-JUL-39', '11-NOV-68', 310, 110, 2, 'AGUIRRE, AUREO');
insert into EMPLEADOS values (180, 110, 508, '18-OCT-34', '18-MAR-56', 480, 50,  2, 'PEREZ, MARCOS');
insert into EMPLEADOS values (190, 121, 350, '12-MAY-32', '11-FEB-62', 300, null, 4, 'VEIGA, JULIANA');
insert into EMPLEADOS values (210, 100, 200, '28-SEP-40', '22-JAN-59', 380, null, 2, 'GALVEZ, PILAR');
insert into EMPLEADOS values (240, 111, 760, '26-FEB-42', '24-FEB-66', 280, 100, 3, 'SANZ, LAVINIA');
insert into EMPLEADOS values (250, 100, 250, '27-OCT-46', '01-MAR-67', 450, null, 0, 'ALBA, ADRIANA'); 
insert into EMPLEADOS values (260, 100, 220, '03-DEC-43', '12-JUL-68', 720, null, 6, 'LOPEZ, ANTONIO');
insert into EMPLEADOS values (270, 112, 800, '21-MAY-45', '10-SEP-66', 380, 80, 3, 'GARCIA, OCTAVIO');
insert into EMPLEADOS values (280, 130, 410, '11-JAN-48', '08-OCT-71', 290, null, 5, 'FLOR, DOROTEA');
insert into EMPLEADOS values (285, 122, 620, '25-OCT-49', '15-FEB-68', 380, null, 0, 'POLO, OTILIA');
insert into EMPLEADOS values (290, 120, 910, '30-NOV-47', '14-FEB-68', 270, null, 3, 'GIL, GLORIA');
insert into EMPLEADOS values (310, 130, 480, '21-NOV-46', '15-JAN-71', 420, null, 0, 'GARCIA, AUGUSTO');
insert into EMPLEADOS values (320, 122, 620, '25-DEC-57', '05-FEB-78', 405, null, 2, 'SANZ, CORNELIO');
insert into EMPLEADOS values (330, 112, 850, '19-AUG-48', '01-MAR-72', 280, 90, 0, 'DIEZ, AMELIA');
insert into EMPLEADOS values (350, 122, 610, '13-APR-49', '10-SEP-84', 450, null, 1, 'CAMPS, AURELIO');
insert into EMPLEADOS values (360, 111, 750, '29-OCT-58', '10-OCT-68', 250, 100, 2, 'LARA, DORINDA');
insert into EMPLEADOS values (370, 121, 360, '22-JUN-67', '20-JAN-87', 190, null, 1, 'RUIZ, FABIOLA');
insert into EMPLEADOS values (380, 112, 880, '30-MAR-68', '01-JAN-88', 180, null, 0, 'MARTIN, MICAELA');
insert into EMPLEADOS values (390, 110, 500, '19-FEB-66', '08-OCT-86', 215, null, 1, 'MORAN, CARMEN');
insert into EMPLEADOS values (400, 111, 780, '18-AUG-69', '01-NOV-87', 185, null, 0, 'LARA, LUCRECIA');
insert into EMPLEADOS values (410, 122, 660, '14-JUL-68', '13-OCT-88', 175, null, 0, 'MU�OZ, AZUCENA');
insert into EMPLEADOS values (420, 130, 450, '22-OCT-66', '19-NOV-88', 400, null, 0, 'FIERRO, CLAUDIA');
insert into EMPLEADOS values (430, 122, 650, '26-OCT-67', '19-NOV-88', 210, null, 1, 'MORA, VALERIANA');
insert into EMPLEADOS values (440, 111, 760, '27-SEP-66', '28-FEB-86', 210, 100, 0, 'DURAN, LIVIA');
insert into EMPLEADOS values (450, 112, 880, '21-OCT-66', '28-FEB-86', 210, 100, 0, 'PEREZ, SABINA');
insert into EMPLEADOS values (480, 111, 760, '04-APR-65', '28-FEB-86', 210, 100, 1, 'PINO, DIANA');
insert into EMPLEADOS values (490, 112, 880, '06-JUN-64', '01-JAN-88', 180, 100, 0, 'TORRES, HORACIO');
insert into EMPLEADOS values (500, 111, 750, '08-OCT-65', '01-JAN-87', 200, 100, 0, 'VAZQUEZ, HONORIA');
insert into EMPLEADOS values (510, 110, 550, '04-MAY-66', '01-NOV-86', 200, null, 1, 'CAMPOS, ROMULO');
insert into EMPLEADOS values (550, 111, 780, '10-JAN-70', '21-JAN-88', 100, 120, 0, 'SANTOS, SANCHO');

--1. A�ada un campo nuevo a la tabla empleados: Categoria VARCHAR2(15) DEFAULT 'Empleado' �Qu� sucede con las tuplas 
--que hab�a ya en la tabla? �Y con aquellas que se introducen nuevas y no especificamos nada en el nuevo atributo? 
--Escriba un procedimiento PL/SQLque tenga un �nico argumento del mismo tipo que el Salario de la tabla Empleados. 
--El procedimiento actualizar� la Categor�a de cada empleado atendiendo a ciertas condiciones. Realice los siguientes pasos:
set SERVEROUTPUT ON;
select * from empleados;
alter table empleados add Categoria varchar2(15) default 'Empleado';
update empleados set categoria='EMPLEADO';

--Al alterar la tabla empleado, las tuplas nuevas se inicializan al valor por defecto.
insert into EMPLEADOS(numero,departamento,telefono,fecha_nacimiento,fecha_ingreso,salario,comision,num_hijos,nombre) 
values (560, 121, 350, '10-NOV-20', '20-FEB-90', 310, null, 0, 'UNO, DOS');
--Si no se especifica el valor tambien se inserta con el de por defecto

create or replace procedure pa_empleados_categoria (val number)
as
CURSOR CEMPLEADOSDIRECTOR IS select E.NUMERO,D.TIPO_DIR from empleados e join departamentos d on(e.numero=d.director);
REMPLEADOSDIRECTOR CEMPLEADOSDIRECTOR%ROWTYPE;

CURSOR CEMPLEADOS IS SELECT * FROM EMPLEADOS; 
REMPLEADOS CEMPLEADOS%ROWTYPE;

CURSOR CDEPARTAMENTOSINJEFE IS select numero from departamentos minus select distinct dpto_jefe from departamentos;
RDEPARTAMENTOSINJEFE  CDEPARTAMENTOSINJEFE %ROWTYPE;

CURSOR CDEPARTAMENTOSMADRID IS select D.NUMERO FROM departamentos d join centros c on(d.CENTRO=c.numero) where c.direccion like '%MADRID%';
RDEPARTAMENTOSMADRID CDEPARTAMENTOSMADRID %ROWTYPE;

CURSOR CNUMEROEMPLEADOS IS select d.NUMERO from empleados e join departamentos d on(e.DEPARTAMENTO=d.NUMERO) group by d.NUMERO having count(*)<4;
RNUMEROEMPLEADOS CNUMEROEMPLEADOS %ROWTYPE;

CURSOR CTLFNEMPLEADOS IS select telefono from empleados group by TELEFONO having count(telefono)>3;
RTLFNEMPLEADOS CTLFNEMPLEADOS %ROWTYPE;
begin
    OPEN CEMPLEADOSDIRECTOR;
    LOOP
        FETCH CEMPLEADOSDIRECTOR INTO REMPLEADOSDIRECTOR;
        --- Si el empleado es director en funciones o en propiedad se le asignar� la categor�a 'DirectorF' o 'DirectorP', respectivamente.
        IF REMPLEADOSDIRECTOR.TIPO_DIR LIKE 'F' THEN
            UPDATE EMPLEADOS SET CATEGORIA='DIRECTORF' WHERE NUMERO=REMPLEADOSDIRECTOR.NUMERO;
        ELSE
            UPDATE EMPLEADOS SET CATEGORIA='DIRECTORP' WHERE NUMERO=REMPLEADOSDIRECTOR.NUMERO;
        END IF;

        EXIT WHEN CEMPLEADOSDIRECTOR%NOTFOUND;
    END LOOP;
    CLOSE CEMPLEADOSDIRECTOR;

    OPEN CEMPLEADOS;
    LOOP
        FETCH CEMPLEADOS INTO REMPLEADOS;
        --- Si un empleado no tiene ninguna categor�a de las anteriores se le asignar� la categor�a 'Empleado'.
        UPDATE EMPLEADOS SET CATEGORIA='EMPLEADO' WHERE NUMERO=REMPLEADOS.NUMERO;
        --- Si el Salario del empleado no supera el valor del argumento del procedimiento se le asignar� la categor�a 'Precario'.
        IF REMPLEADOS.SALARIO < val THEN
            UPDATE EMPLEADOS SET CATEGORIA='PRECARIO' WHERE NUMERO=REMPLEADOS.NUMERO;
        END IF;
        
        --- Si la suma del Salario y la Comisi�n del empleado no superan cierto valor conseguido al aumentar el 10% el valor 
        --del argumento del procedimiento, entonces se le concatenar� una admiraci�n a la categor�a que previamente tuviera, 
        --quedando, por ejemplo, como 'DirectorF!', para un director en funciones.
        dbms_output.put_line(TO_CHAR(val+(val*10/100))||' '||TO_CHAR(REMPLEADOS.SALARIO+REMPLEADOS.COMISION));
        IF val+(val*10/100)>(REMPLEADOS.SALARIO+REMPLEADOS.COMISION) THEN
            UPDATE EMPLEADOS SET CATEGORIA=CATEGORIA||'!' WHERE NUMERO=REMPLEADOS.NUMERO;
        END IF;
        
        OPEN CDEPARTAMENTOSINJEFE;
        LOOP
            FETCH CDEPARTAMENTOSINJEFE INTO RDEPARTAMENTOSINJEFE;
            --- Si el empleado est� adscrito a un departamento que no tiene departamento jefe, se le concatenar� un asterisco a la 
            --categor�a que previamente tuviera, quedando, por ejemplo, como 'DirectorF*', para un director en funciones.
            IF REMPLEADOS.DEPARTAMENTO=RDEPARTAMENTOSINJEFE.NUMERO THEN
                UPDATE EMPLEADOS SET CATEGORIA=CATEGORIA||'*' WHERE NUMERO=REMPLEADOS.NUMERO;
            END IF;
            EXIT WHEN CDEPARTAMENTOSINJEFE %NOTFOUND;
            
        END LOOP;
        CLOSE CDEPARTAMENTOSINJEFE;
        
        --- Si el empleado est� adscrito a un departamento con direcci�n en MADRID, se le concatenar� un s�mbolo de �mayor que� a 
        --la categor�a que previamente tuviera, quedando, por ejemplo, como 'DirectorF>', para un director en funciones.
        OPEN CDEPARTAMENTOSMADRID;
        LOOP
            FETCH CDEPARTAMENTOSMADRID INTO RDEPARTAMENTOSMADRID;
            IF REMPLEADOS.DEPARTAMENTO=RDEPARTAMENTOSMADRID.NUMERO THEN
                UPDATE EMPLEADOS SET CATEGORIA=CATEGORIA||'>' WHERE NUMERO=REMPLEADOS.NUMERO;
            END IF;
            EXIT WHEN CDEPARTAMENTOSMADRID %NOTFOUND;
        END LOOP;
        CLOSE CDEPARTAMENTOSMADRID;
        
        --- Si el empleado est� adscrito a un departamento con menos de 4 empleados o dicho empleado est� compartiendo tel�fono 
        --con m�s de 3 empleados, entonces se le concatenar� un s�mbolo de suma a la categor�a que previamente tuviera, quedando, 
        --por ejemplo, como 'DirectorF+', para un director en funciones.
        OPEN CNUMEROEMPLEADOS;
        LOOP
            FETCH CNUMEROEMPLEADOS INTO RNUMEROEMPLEADOS;
            
            OPEN CTLFNEMPLEADOS;
            LOOP
                FETCH CTLFNEMPLEADOS INTO RTLFNEMPLEADOS;
                IF (RNUMEROEMPLEADOS.NUMERO = REMPLEADOS.DEPARTAMENTO) OR (RTLFNEMPLEADOS.TELEFONO = REMPLEADOS.TELEFONO) THEN
                    UPDATE EMPLEADOS SET CATEGORIA=CATEGORIA||'+' WHERE NUMERO=REMPLEADOS.NUMERO;
                END IF;
                EXIT WHEN CTLFNEMPLEADOS %NOTFOUND;
            END LOOP;
            CLOSE CTLFNEMPLEADOS;
            
            EXIT WHEN CNUMEROEMPLEADOS %NOTFOUND;
        END LOOP;
        CLOSE CNUMEROEMPLEADOS;
        
        
        EXIT WHEN CEMPLEADOS %NOTFOUND;
    END LOOP;
    CLOSE CEMPLEADOS;

end pa_empleados_categoria;

begin 
    pa_empleados_categoria(300);
end;

/*2. Escriba una funci�n PL/SQL que calcule y devuelva el n�mero de a�os completos que lleva Trabajando en 
la empresa el empleado cuyo c�digo es pasado como �nico argumento. Utilizando esa funci�n haga un procedimiento 
PL/SQL que ayude a seleccionar un empleado como director de departamento para aquellos departamentos que tienen director en funciones.*/

/*
Para cada departamento con un director en funciones se visualizar� el n�mero y nombre de ese departamento y todos 
los candidatos a director en propiedad. Los candidatos ser�n aquellos empleados que est�n adscritos a ese departamento, 
lleven m�s de 5 a�os trabajando para la empresa y tengan el s�mbolo de �mayor que� en el atributo Categor�a tratado anteriormente 
(adscritos a MADRID).

Adem�s, si un departamento con un director en funciones no tiene ning�n candidato se indicar� tal eventualidad y se mostrar�n 
los datos de antig�edad y si est�n o no adscritos a MADRID para todos los empleados de dicho departamento.*/
set SERVEROUTPUT ON;
select * from empleados;

create or replace FUNCTION FN_EMPELADOS_ANTIGUEDAD (VAL NUMBER) RETURN varchar2
IS
    CURSOR CEMPLEADOS IS select * from empleados;
    REMPLEADOS CEMPLEADOS%ROWTYPE;
BEGIN
    OPEN CEMPLEADOS;
    LOOP
        FETCH CEMPLEADOS INTO REMPLEADOS;
        IF REMPLEADOS.NUMERO=val THEN
            RETURN EXTRACT(YEAR FROM CURRENT_DATE)-(EXTRACT(YEAR FROM REMPLEADOS.fecha_INGRESO)-100);
        END IF;
        EXIT WHEN CEMPLEADOS%NOTFOUND;
    END LOOP;
    CLOSE CEMPLEADOS;
END;

begin 
    dbms_output.put_line( FN_EMPELADOS_ANTIGUEDAD(120));
end;

---------

SELECT * FROM DEPARTAMENTOS WHERE TIPO_DIR ='F';
INSERT INTO DEPARTAMENTOS VALUES(666,); 

create or replace procedure pa_empleados_funciones 
as
CURSOR CFUNCIONES IS SELECT NUMERO, NOMBRE FROM DEPARTAMENTOS WHERE TIPO_DIR ='F';
RFUNCIONES CFUNCIONES%ROWTYPE;

CURSOR CEMPLEADOS IS select NUMERO, DEPARTAMENTO, CATEGORIA from empleados;
REMPLEADOS CEMPLEADOS%ROWTYPE;
begin
    OPEN CFUNCIONES;
    LOOP
        FETCH CFUNCIONES INTO RFUNCIONES;
        EXIT WHEN CFUNCIONES%NOTFOUND;
        dbms_output.put_line( 'NOMBRE: '|| RFUNCIONES.NOMBRE || ' - NUMERO: '|| RFUNCIONES.NUMERO );
        OPEN CEMPLEADOS;
        LOOP
            FETCH CEMPLEADOS INTO REMPLEADOS;
            EXIT WHEN CEMPLEADOS%NOTFOUND;
            IF REMPLEADOS.DEPARTAMENTO=RFUNCIONES.NUMERO 
                AND FN_EMPELADOS_ANTIGUEDAD(REMPLEADOS.NUMERO)>5 
                AND REMPLEADOS.CATEGORIA LIKE '%>%' THEN
                dbms_output.put_line( '    EMPLEADO: '||REMPLEADOS.NUMERO );
            END IF;
        END LOOP;
        CLOSE CEMPLEADOS;
    END LOOP;
    CLOSE CFUNCIONES;        
end pa_empleados_funciones;

begin 
    pa_empleados_funciones();
end;


