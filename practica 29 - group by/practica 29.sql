/*Un comercio que tiene un stand en una feria registra en una tabla llamada "visitantes" algunos datos 
de las personas que visitan o compran en su stand para luego enviarle publicidad de sus productos.

1- Elimine la tabla "visitantes" y cr�ela con la siguiente estructura:*/
drop table visitantes;
create table visitantes(nombre varchar2(30),edad number(2),sexo char(1) default 'f',domicilio varchar2(30),
    ciudad varchar2(20) default 'Cordoba',telefono varchar2(11),mail varchar2(30) default 'no tiene',montocompra number(6,2));

--3- Ingrese algunos registros:
insert into visitantes values ('Susana Molina',35,default,'Colon 123',default,null,null,59.80);
insert into visitantes values ('Marcos Torres',29,'m',default,'Carlos Paz',default,'marcostorres@hotmail.com',150.50);
insert into visitantes values ('Mariana Juarez',45,default,default,'Carlos Paz',null,default,23.90);
insert into visitantes (nombre, edad,sexo,telefono, mail) values ('Fabian Perez',36,'m','4556677','fabianperez@xaxamail.com');
insert into visitantes (nombre, ciudad, montocompra) values ('Alejandra Gonzalez','La Falda',280.50);
insert into visitantes (nombre, edad,sexo, ciudad, mail,montocompra) values ('Gaston Perez',29,'m','Carlos Paz','gastonperez1@gmail.com',95.40);
insert into visitantes values ('Liliana Torres',40,default,'Sarmiento 876',default,default,default,85);
insert into visitantes values ('Gabriela Duarte',21,null,null,'Rio Tercero',default,'gabrielaltorres@hotmail.com',321.50);

--4- Queremos saber la cantidad de visitantes de cada ciudad utilizando la cl�usula "group by" (4 filas devueltas)
select count(*) AS NUMERO_VISITANTES from VISITANTES;

--5- Queremos la cantidad visitantes con tel�fono no nulo, de cada ciudad (4 filas devueltas)
SELECT CIUDAD, COUNT(*) AS CON_TELEFONO
    FROM VISITANTES
    WHERE TELEFONO IS NOT NULL
    GROUP BY CIUDAD;
-- Cordoba	1

--6- Necesitamos el total del monto de las compras agrupadas por sexo (3 filas)
SELECT SEXO, SUM(montocompra) TOT_MONTO_COMPRAS
    FROM VISITANTES
    GROUP BY SEXO;
/*
(NULL)	321,5
f	449,2
m	245,9
*/
--Note que los registros con valor nulo en el campo "sexo" se procesan como un grupo diferente.

--7- Se necesita saber el m�ximo y m�nimo valor de compra agrupados por sexo y ciudad (6 filas)
SELECT CIUDAD, MAX(MONTOCOMPRA) AS MAX_COMPRA, MIN (MONTOCOMPRA) AS MIN_COMPRA
    FROM VISITANTES
    GROUP BY CIUDAD;
/*La Falda	280,5	280,5
Rio Tercero	321,5	321,5
Cordoba	85	59,8
Carlos Paz	150,5	23,9*/

--8- Calcule el promedio del valor de compra agrupados por ciudad (4 filas)
SELECT CIUDAD, ROUND(AVG(MONTOCOMPRA), 3)
    FROM VISITANTES
    GROUP BY CIUDAD;
/*La Falda	280,5
Rio Tercero	321,5
Cordoba	72,4
Carlos Paz	89,933*/

--9- Cuente y agrupe por ciudad sin tener en cuenta los visitantes que no tienen mail (3 filas)
SELECT CIUDAD, COUNT(*) AS TOTAL
    FROM VISITANTES
    WHERE MAIL IS NOT NULL
    GROUP BY CIUDAD;
/*La Falda	1
Rio Tercero	1
Cordoba	2
Carlos Paz	3*/