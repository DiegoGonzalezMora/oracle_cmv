/*Una empresa tiene registrados datos de sus empleados en una tabla llamada "empleados". 
1- Elimine la tabla:*/
drop table empleados;

--2- Cr�ela con la siguiente estructura:
create table empleados ( documento char(8), nombre varchar2(30), cantidadhijos number(2), 
seccion varchar2(20), sueldo number(6,2) default -1 );

--3- Agregue una restricci�n "check" para asegurarse que no se ingresen valores negativos para el sueldo.
ALTER TABLE EMPLEADOS DROP CONSTRAINT CK_EMPLEADOS_SUELDO;
ALTER TABLE EMPLEADOS ADD CONSTRAINT CK_EMPLEADOS_SUELDO CHECK(SUELDO>=0);
--Note que el campo "sueldo" tiene establecido un valor por defecto (el valor -1) que va contra la restricci�n; 
--Oracle no controla esto, permite establecer la restricci�n, pero al intentar ingresar un registro con el valor por 
--defecto en tal campo, muestra un mensaje de error.

--4- Intente ingresar un registro con la palabra clave "default" en el campo "sueldo" (mensaje de error)
INSERT INTO EMPLEADOS(documento, nombre, cantidadhijos, seccion) VALUES('00000001', 'Luis', 2, 'Sistemas');
--ORA-02290: check constraint (SYSTEM.CK_EMPLEADOS_SUELDO) violated

--5- Ingrese algunos registros v�lidos:
insert into empleados values ('22222222','Alberto Lopez',1,'Sistemas',1000); 
insert into empleados values ('33333333','Beatriz Garcia',2,'Administracion',3000); 
insert into empleados values ('34444444','Carlos Caseres',0,'Contadur�a',6000);
select * from empleados;

--6- Intente agregar otra restricci�n "check" al campo sueldo para asegurar que ninguno supere el valor 5000. 
ALTER TABLE EMPLEADOS DROP CONSTRAINT CK_EMPLEADOS_SUELDO2;
ALTER TABLE EMPLEADOS ADD CONSTRAINT CK_EMPLEADOS_SUELDO2 CHECK(SUELDO<5000);
--La sentencia no se ejecuta porque hay un sueldo que no cumple la restricci�n.
/*
ORA-02293: cannot validate (SYSTEM.CK_EMPLEADOS_SUELDOS2) - check constraint violated
02293. 00000 - "cannot validate (%s.%s) - check constraint violated"
*Cause:    an alter table operation tried to validate a check constraint to
           populated table that had nocomplying values.
*Action:   Obvious*/

--7- Elimine el registro infractor y vuelva a crear la restricci�n
UPDATE EMPLEADOS SET SUELDO=NULL WHERE DOCUMENTO='34444444';
ALTER TABLE EMPLEADOS ADD CONSTRAINT CK_EMPLEADOS_SUELDO2 CHECK(SUELDO<5000);

--8- Establezca una restricci�n "check" para "seccion" que permita solamente los valores "Sistemas", "Administracion" y "Contadur�a".
ALTER TABLE EMPLEADOS ADD CONSTRAINT CK_EMPLEADOS_SECCION CHECK(seccion IN('Sistemas', 'Administracion', 'Contadur�a'));

--9- Ingrese un registro con valor "null" en el campo "seccion".
UPDATE EMPLEADOS SET SECCION=NULL WHERE DOCUMENTO='34444444';

--10- Establezca una restricci�n "check" para "cantidadhijos" que permita solamente valores entre 0 y 15.
ALTER TABLE EMPLEADOS ADD CONSTRAINT CK_EMPLEADOS_CANTIDADHIJOS CHECK(CANTIDADHIJOS>=0 AND CANTIDADHIJOS <= 15);

--11- Vea todas las restricciones de la tabla (4 filas)
SELECT * FROM USER_CONSTRAINTS WHERE TABLE_NAME='EMPLEADOS';

--12- Intente agregar un registro que vaya contra alguna de las restricciones al campo "sueldo". Mensaje de error porque 
--se infringe la restricci�n "CK_empleados_sueldo_positivo".
INSERT INTO EMPLEADOS VALUES('44444444', 'Diego Mora', 0, 'Sistemas', -1000);
/*INSERT INTO EMPLEADOS VALUES('44444444', 'Diego Mora', 0, 'Sistemas', -1000)
Informe de error -
ORA-02290: check constraint (SYSTEM.CK_EMPLEADOS_SUELDO) violated*/

--13- Intente modificar un registro colocando en "cantidadhijos" el valor "21".
UPDATE EMPLEADOS SET CANTIDADHIJOS=21 where NOMBRE='Diego Mora';
-- 0 filas actualizadas.

--14- Intente modificar el valor de alg�n registro en el campo "seccion" cambi�ndolo por uno 
--que no est� incluido en la lista de permitidos.
update empleados set SECCION='congelados' where NOMBRE='Diego Mora';
-- 0 filas actualizadas.

--15- Intente agregar una restricci�n al campo secci�n para aceptar solamente valores que comiencen con la letra "B".
ALTER TABLE EMPLEADOS ADD CONSTRAINT CK_EMPLEADOS_SECCION2 CHECK(SECCION='B%');
--Note que NO se puede establecer esta restricci�n porque va en contra de la establecida anteriormente para el mismo campo, 
--si lo permitiera, no podr�amos ingresar ning�n valor para "seccion".

--16- Agregue un registro con documento nulo.
SELECT * FROM EMPLEADOS;
UPDATE EMPLEADOS SET DOCUMENTO=NULL WHERE NOMBRE='Carlos Caseres';

--17- Intente agregar una restricci�n "primary key" para el campo "documento".
ALTER TABLE EMPLEADOS ADD CONSTRAINT PK_EMPLEADOS_DOCUMENTO PRIMARY KEY(DOCUMENTO);
--No lo permite porque existe un registro con valor nulo en tal campo.

--18- Elimine el registro que infringe la restricci�n y establezca la restricci�n del punto 17.
UPDATE EMPLEADOS SET DOCUMENTO='34444444' WHERE NOMBRE='Carlos Caseres';
ALTER TABLE EMPLEADOS ADD CONSTRAINT PK_EMPLEADOS_DOCUMENTO PRIMARY KEY(DOCUMENTO);

--19- Consulte "user_constraints", mostrando los campos "constraint_name", "constraint_type" y "search_condition" de la 
SELECT CONSTRAINT_NAME, CONSTRAINT_TYPE, SEARCH_CONDITION FROM USER_CONSTRAINTS WHERE TABLE_NAME='EMPLEADOS';
--tabla "empleados" (5 filas)

--20- Consulte el cat�logo "user_cons_colums" recuperando el nombre de las restricciones establecidas en el campo sueldo 
--de la tabla "empleados" (2 filas)
SELECT * FROM USER_CONS_COLUMNS WHERE TABLE_NAME='EMPLEADOS' AND COLUMN_NAME='SUELDO';
