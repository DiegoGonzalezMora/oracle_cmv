--Un consultorio m�dico en el cual trabajan 3 m�dicos registra las consultas de los pacientes en una tabla llamada "consultas".
--1- Elimine la tabla:
drop table consultas; 

--2- La tabla contiene los siguientes datos:
--- fechayhora: date not null, fecha y hora de la consulta, - medico: varchar2(30), not null, nombre del m�dico (Perez,Lopez,Duarte), 
--- documento: char(8) not null, documento del paciente, - paciente: varchar2(30), nombre del paciente,
--- obrasocial: varchar2(30), nombre de la obra social (IPAM,PAMI, etc.).

--3- Haga  uso del  formato de "date" para que nos muestre d�a, mes, a�o, hora y minutos:
ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI'; 

--4- Un m�dico s�lo puede atender a un paciente en una fecha y hora determinada. En una fecha y hora determinada,
--varios m�dicos atienden a distintos pacientes. Cree la tabla definiendo una clave primaria compuesta:
create table consultas( fechayhora date not null, medico varchar2(30) not null, documento char(8) not null, 
paciente varchar2(30), obrasocial varchar2(30), primary key(fechayhora,medico) );

--4- Ingrese varias consultas para un mismo m�dico en distintas horas el mismo d�a:
insert into consultas values ('05/11/2006 8:00','Lopez','12222222','Acosta Betina','PAMI'); 
insert into consultas values ('05/11/2006 8:30','Lopez','23333333','Fuentes Carlos','PAMI');

--5- Ingrese varias consultas para diferentes m�dicos en la misma fecha y hora:
insert into consultas values ('05/11/2006 8:00','Perez','34444444','Garcia Marisa','IPAM'); 
insert into consultas values ('05/11/2006 8:00','Duarte','45555555','Pereyra Luis','PAMI'); 

--6- Intente ingresar una consulta para un mismo m�dico en la misma hora el mismo d�a (mensaje de error)
insert into consultas values ('05/11/2006 8:00','Duarte','45555555','Pereyra Luis','PAMI'); 

/*Error que empieza en la l�nea: 27 del comando :
insert into consultas values ('05/11/2006 8:00','Duarte','45555555','Pereyra Luis','PAMI')
Informe de error -
ORA-00001: unique constraint (SYSTEM.SYS_C007108) violated*/


--7- Intente cambiar la hora de la consulta de "Acosta Betina" por una no disponible ("8:30") (error)
update consultas set FECHAYHORA='05/11/2006 8:30' where PACIENTE='Acosta Betina';

/*Error que empieza en la l�nea: 36 del comando :
update consultas set FECHAYHORA='05/11/2006 8:30' where PACIENTE='Acosta Betina'
Informe de error -
ORA-00001: unique constraint (SYSTEM.SYS_C007108) violated*/

--8- Cambie la hora de la consulta de "Acosta Betina" por una disponible ("9:30")
update consultas set FECHAYHORA='05/11/2006 9:30' where PACIENTE='Acosta Betina';

--9- Ingrese una consulta para el d�a "06/11/2006" a las 10 hs. para el doctor "Perez"
insert into consultas values ('06/11/2006 10:00','Perez','88222222','Manolo Mora','IPAM'); 

--10- Recupere todos los datos de las consultas de "Lopez" (3 registros)
select * from CONSULTAS where MEDICO='Lopez'; 

--11- Recupere todos los datos de las consultas programadas para el "05/11/2006 8:00" (2 registros)
select * from consultas where FECHAYHORA='05/11/2006 8:00';

--12- Muestre d�a y mes de todas las consultas de "Lopez"
/*select to_char(to_date(fechayhora,'dd/mm/yyyy hh24:mi:ss'),'dd') dia,
        to_char(to_date(fechayhora,'dd/mm/yyyy hh24:mi:ss'),'mm') mes
    from consultas where MEDICO='Lopez';*/
    
select to_char(fechayhora, 'dd/mm')
    from consultas
        where medico='Lopez';
    