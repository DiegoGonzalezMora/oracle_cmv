drop table empleados;
drop table departamentos;
drop table centros;

create table centros (numero number(2), nombre varchar2(40), direccion varchar2(50), primary key(numero)
);

create table departamentos(numero number(3), centro number(3), director number(3), tipo_dir char(1), presupuesto number(2),
                dpto_jefe number(3), nombre varchar2(40), primary key(numero), foreign key (centro) references centros(numero));

create table empleados (numero number(3), departamento number(3), telefono number(3), fecha_nacimiento date, fecha_ingreso date,
                salario number(3), comision number(3), num_hijos number(1), nombre varchar2(50), primary key(numero),
                foreign key(departamento) references departamentos(numero));

alter session set nls_date_language = 'ENGLISH';
alter session set nls_date_format = 'DD-MON-YY';

-- Inserci�n de valores en la tabla CENTROS:
insert into CENTROS values (10,  'SEDE CENTRAL', 'C. ALCALA 820, MADRID');
insert into CENTROS values (20,  'RELACION CON CLIENTES', 'C. ATOCHA 405, MADRID');

-- Inserci�n de valores en la tabla DEPARTAMENTOS:
insert into DEPARTAMENTOS values (100, 10, 260, 'P', 12, null, 'DIRECCION GENERAL');
insert into DEPARTAMENTOS values (110, 20, 180, 'P', 15, 100, 'DIRECC. COMERCIAL');
insert into DEPARTAMENTOS values (111, 20, 180, 'F', 11, 110, 'SECTOR INDUSTRIAL');
insert into DEPARTAMENTOS values (112, 20, 270, 'P', 9, 100, 'SECTOR SERVICIOS');
insert into DEPARTAMENTOS values (120, 10, 150, 'F', 3, 100, 'ORGANIZACION');
insert into DEPARTAMENTOS values (121, 10, 150, 'P', 2, 120, 'PERSONAL');
insert into DEPARTAMENTOS values (122, 10, 350, 'P', 6, 120, 'PROCESO DE DATOS');
insert into DEPARTAMENTOS values (130, 10, 310, 'P', 2, 100, 'FINANZAS');

-- Inserci�n de valores en la tabla EMPLEADOS:
insert into EMPLEADOS values (110, 121, 350, '10-NOV-29', '10-FEB-50', 310, null, 3, 'PONS, CESAR');
insert into EMPLEADOS values (120, 112, 840, '09-JUN-35', '01-OCT-68', 350, 110, 1, 'LASA, MARIO');
insert into EMPLEADOS values (130, 112, 810, '09-NOV-45', '01-FEB-69', 290, 110, 2, 'TEROL, LUCIANO');
insert into EMPLEADOS values (150, 121, 340, '10-AUG-30', '15-JAN-48', 440, null, 0, 'PEREZ, JULIO');
insert into EMPLEADOS values (160, 111, 740, '09-JUL-39', '11-NOV-68', 310, 110, 2, 'AGUIRRE, AUREO');
insert into EMPLEADOS values (180, 110, 508, '18-OCT-34', '18-MAR-56', 480, 50,  2, 'PEREZ, MARCOS');
insert into EMPLEADOS values (190, 121, 350, '12-MAY-32', '11-FEB-62', 300, null, 4, 'VEIGA, JULIANA');
insert into EMPLEADOS values (210, 100, 200, '28-SEP-40', '22-JAN-59', 380, null, 2, 'GALVEZ, PILAR');
insert into EMPLEADOS values (240, 111, 760, '26-FEB-42', '24-FEB-66', 280, 100, 3, 'SANZ, LAVINIA');
insert into EMPLEADOS values (250, 100, 250, '27-OCT-46', '01-MAR-67', 450, null, 0, 'ALBA, ADRIANA');
insert into EMPLEADOS values (260, 100, 220, '03-DEC-43', '12-JUL-68', 720, null, 6, 'LOPEZ, ANTONIO');
insert into EMPLEADOS values (270, 112, 800, '21-MAY-45', '10-SEP-66', 380, 80, 3, 'GARCIA, OCTAVIO');
insert into EMPLEADOS values (280, 130, 410, '11-JAN-48', '08-OCT-71', 290, null, 5, 'FLOR, DOROTEA');
insert into EMPLEADOS values (285, 122, 620, '25-OCT-49', '15-FEB-68', 380, null, 0, 'POLO, OTILIA');
insert into EMPLEADOS values (290, 120, 910, '30-NOV-47', '14-FEB-68', 270, null, 3, 'GIL, GLORIA');
insert into EMPLEADOS values (310, 130, 480, '21-NOV-46', '15-JAN-71', 420, null, 0, 'GARCIA, AUGUSTO');
insert into EMPLEADOS values (320, 122, 620, '25-DEC-57', '05-FEB-78', 405, null, 2, 'SANZ, CORNELIO');
insert into EMPLEADOS values (330, 112, 850, '19-AUG-48', '01-MAR-72', 280, 90, 0, 'DIEZ, AMELIA');
insert into EMPLEADOS values (350, 122, 610, '13-APR-49', '10-SEP-84', 450, null, 1, 'CAMPS, AURELIO');
insert into EMPLEADOS values (360, 111, 750, '29-OCT-58', '10-OCT-68', 250, 100, 2, 'LARA, DORINDA');
insert into EMPLEADOS values (370, 121, 360, '22-JUN-67', '20-JAN-87', 190, null, 1, 'RUIZ, FABIOLA');
insert into EMPLEADOS values (380, 112, 880, '30-MAR-68', '01-JAN-88', 180, null, 0, 'MARTIN, MICAELA');
insert into EMPLEADOS values (390, 110, 500, '19-FEB-66', '08-OCT-86', 215, null, 1, 'MORAN, CARMEN');
insert into EMPLEADOS values (400, 111, 780, '18-AUG-69', '01-NOV-87', 185, null, 0, 'LARA, LUCRECIA');
insert into EMPLEADOS values (410, 122, 660, '14-JUL-68', '13-OCT-88', 175, null, 0, 'MU�OZ, AZUCENA');
insert into EMPLEADOS values (420, 130, 450, '22-OCT-66', '19-NOV-88', 400, null, 0, 'FIERRO, CLAUDIA');
insert into EMPLEADOS values (430, 122, 650, '26-OCT-67', '19-NOV-88', 210, null, 1, 'MORA, VALERIANA');
insert into EMPLEADOS values (440, 111, 760, '27-SEP-66', '28-FEB-86', 210, 100, 0, 'DURAN, LIVIA');
insert into EMPLEADOS values (450, 112, 880, '21-OCT-66', '28-FEB-86', 210, 100, 0, 'PEREZ, SABINA');
insert into EMPLEADOS values (480, 111, 760, '04-APR-65', '28-FEB-86', 210, 100, 1, 'PINO, DIANA');
insert into EMPLEADOS values (490, 112, 880, '06-JUN-64', '01-JAN-88', 180, 100, 0, 'TORRES, HORACIO');
insert into EMPLEADOS values (500, 111, 750, '08-OCT-65', '01-JAN-87', 200, 100, 0, 'VAZQUEZ, HONORIA');
insert into EMPLEADOS values (510, 110, 550, '04-MAY-66', '01-NOV-86', 200, null, 1, 'CAMPOS, ROMULO');
insert into EMPLEADOS values (550, 111, 780, '10-JAN-70', '21-JAN-88', 100, 120, 0, 'SANTOS, SANCHO');

--1) Desarrollar un procedimiento que visualice el apellido y la fecha de alta de todos los empleados ordenados por apellido.
--SUBSTR(nombre, 0, CHARINDEX(',', nombre)) as 
select SUBSTR(nombre, 0, INSTR(nombre,',')-1) AS nombre, fecha_ingreso from empleados order by nombre;

create or replace procedure pa_empleados_alta AS
CURSOR CEMPLEADOS IS
    select SUBSTR(nombre, 0, INSTR(nombre,',')-1) AS nombre, fecha_ingreso 
        from empleados order by nombre;
    
    RESULTADO CEMPLEADOS%ROWTYPE;
    BEGIN
        OPEN CEMPLEADOS;
        FETCH CEMPLEADOS INTO RESULTADO.NOMBRE, RESULTADO.fecha_ingreso;
        WHILE CEMPLEADOS%FOUND
        LOOP
            dbms_output.put_line(RESULTADO.NOMBRE||' '||RESULTADO.fecha_ingreso);
            FETCH CEMPLEADOS INTO RESULTADO.NOMBRE, RESULTADO.fecha_ingreso;
        END LOOP;
        CLOSE CEMPLEADOS;
end pa_empleados_alta;

begin
 pa_empleados_alta();
end;

--2) Codificar un procedimiento que muestre el nombre de cada departamento y el n�mero de empleados que tiene.
SELECT D.NOMBRE, COUNT(*) AS NUM_EMPLEADOS
    FROM DEPARTAMENTOS D JOIN EMPLEADOS E ON (D.NUMERO=E.DEPARTAMENTO)
    GROUP BY D.NOMBRE;
    
create or replace procedure pa_empleados_NUMERO AS
CURSOR CEMPLEADOS IS
    SELECT D.NOMBRE, COUNT(*) AS NUM_EMPLEADOS
    FROM DEPARTAMENTOS D JOIN EMPLEADOS E ON (D.NUMERO=E.DEPARTAMENTO)
    GROUP BY D.NOMBRE;
    
    NOMBRE VARCHAR2(20);
    EMPLEADOS NUMBER;
    BEGIN
        OPEN CEMPLEADOS;
        LOOP
            FETCH CEMPLEADOS INTO NOMBRE, EMPLEADOS;
            dbms_output.put_line(NOMBRE||' '||EMPLEADOS);
            EXIT WHEN  CEMPLEADOS%NOTFOUND;
        END LOOP;
        CLOSE CEMPLEADOS;
end pa_empleados_NUMERO;

begin
 pa_empleados_NUMERO();
end;

--3) Escribir un procedimiento que reciba una cadena y visualice el apellido y el n�mero de empleado de todos los empleados
--cuyo apellido contenga la cadena especificada.
SELECT SUBSTR(nombre, 0, INSTR(nombre,',')-1) AS nombre, NUMERO FROM EMPLEADOS WHERE NOMBRE LIKE '%HO%';

create or replace procedure pa_empleados_NOMBRE(N VARCHAR) AS
CURSOR CEMPLEADOS IS
    SELECT SUBSTR(nombre, 0, INSTR(nombre,',')-1) AS nombre, NUMERO 
        FROM EMPLEADOS 
        WHERE NOMBRE LIKE '%'||N||'%';
    
    NOMBRE VARCHAR2(20);
    NUMERO NUMBER;
    BEGIN
        OPEN CEMPLEADOS;
        LOOP
            FETCH CEMPLEADOS INTO NOMBRE, NUMERO;
            dbms_output.put_line(NOMBRE||' '||NUMERO);
            EXIT WHEN CEMPLEADOS%NOTFOUND;
        END LOOP;
        CLOSE CEMPLEADOS;
end pa_empleados_NOMBRE;

begin
 pa_empleados_NOMBRE('HO');
end;


--4) Escribir un programa que visualice el apellido y el salario de los cinco empleados que tienen el salario m�s alto.
SELECT SUBSTR(nombre, 0, INSTR(nombre,',')-1) AS nombre, SALARIO FROM (select * FROM empleados ORDER BY SALARIO DESC)E2 WHERE ROWNUM<=5;

create or replace procedure pa_empleados_SALARIO AS
CURSOR CEMPLEADOS IS
    SELECT SUBSTR(nombre, 0, INSTR(nombre,',')-1) AS nombre, SALARIO 
        FROM (select * FROM empleados ORDER BY SALARIO DESC)E2 WHERE ROWNUM<=5;
    
    NOMBRE VARCHAR2(20);
    SALARIO NUMBER;
    BEGIN
        OPEN CEMPLEADOS;
        LOOP
            FETCH CEMPLEADOS INTO NOMBRE, SALARIO;
            EXIT WHEN CEMPLEADOS%NOTFOUND;
            dbms_output.put_line(NOMBRE||' '||SALARIO);
        END LOOP;
        CLOSE CEMPLEADOS;
end pa_empleados_SALARIO;

begin
 pa_empleados_SALARIO();
end;

 select trim('Hola mundo') from dual;