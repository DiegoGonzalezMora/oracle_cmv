--1) Codificar un programa que visualice los dos empleados que ganan menos de cada departamento.
set SERVEROUTPUT ON;

--PROCEDURE--
CREATE OR REPLACE PROCEDURE pd_empleados01
IS
cursor C_01 IS
    SELECT departamento,salario,nombre
        FROM empleados
        ORDER BY departamento,salario;
        
    r_aux C_01%rowtype;
    contador number(2):=0;
    dept empleados.departamento%type;
    
    begin
        open C_01;
        dept:=0;
        loop
            FETCH C_01 INTO r_aux;
            exit when C_01%NOTFOUND;
            
            if dept<>r_aux.departamento then
                contador:=0;
                dept:=r_aux.departamento;
            END IF;
            
            IF(contador<2) THEN
                dbms_output.put_line('Departamento: ' || r_aux.departamento||' Nombre: '||r_aux.nombre);
            END IF;
            
            contador:=contador+1;
        end loop;
        close C_01;
end pd_empleados01;
        
begin
pd_empleados01;
end;

--Solo saca 1 emppleado. Sin cursor no puedo controlar cuando un empleado no existe
select * from (select * from empleados order by salario asc) where rownum<=2;
SELECT NUMERO FROM DEPARTAMENTOS GROUP BY ROWNUM, NUMERO HAVING ROWNUM=3;
create or replace procedure pminimo_salario 
    is
        CONTADOR NUMBER;
        sentencia varchar2(255);
        resultado empleados%rowtype;
    begin
        sentencia:='SELECT COUNT(NUMERO) FROM DEPARTAMENTOS';
        EXECUTE IMMEDIATE sentencia INTO CONTADOR;
        FOR i IN 1..CONTADOR LOOP    
        
            sentencia:='select * 
                from (select * from empleados order by salario asc) 
                where DEPARTAMENTO=(SELECT NUMERO FROM DEPARTAMENTOS GROUP BY ROWNUM, NUMERO HAVING ROWNUM='||i||') and rownum=2';
            EXECUTE IMMEDIATE sentencia INTO RESULTADO;
            
            DBMS_OUTPUT.PUT_LINE('departamento: '||to_char(i)||' '||RESULTADO.NOMBRE||' '||RESULTADO.salario);
        END LOOP;
end pminimo_salario;

begin
    pminimo_salario();
end;

--2) Escribir un programa que muestre los siguientes datos: - Para cada empleado: apellido y salario. - 
--Para cada departamento: N�mero de empleados y suma de los salarios del departamento. - Al final del listado: 
--N�mero total de empleados y suma de todos los salarios.
SELECT SUBSTR(nombre, 0, INSTR(nombre,',')-1) AS APELLIDO, SALARIO FROM EMPLEADOS;
SELECT D.NOMBRE, COUNT(E.NUMERO) AS NUM_EMPLEADOS, SUM(SALARIO) AS SUMA_SALARIO 
    FROM DEPARTAMENTOS D JOIN EMPLEADOS E ON (D.NUMERO=E.DEPARTAMENTO) 
    GROUP BY D.NOMBRE;
SELECT COUNT(*) AS NUM_EMPLEADOS_TOTAL, SUM(salario) as suma_salario_total from empleados;

create or replace procedure pdatos 
    is
        cursor C_01 IS SELECT SUBSTR(nombre, 0, INSTR(nombre,',')-1) AS APELLIDO, SALARIO FROM EMPLEADOS;
        cursor C_02 IS SELECT D.NOMBRE, COUNT(E.NUMERO) AS NUM_EMPLEADOS, SUM(SALARIO) AS SUMA_SALARIO 
                            FROM DEPARTAMENTOS D JOIN EMPLEADOS E ON (D.NUMERO=E.DEPARTAMENTO) 
                            GROUP BY D.NOMBRE;
        cursor C_03 IS SELECT COUNT(*) AS NUM_EMPLEADOS_TOTAL, SUM(salario) as suma_salario_total from empleados;
        
        r_aux1 C_01%rowtype;
        r_aux2 C_02%rowtype;
        r_aux3 C_03%rowtype;

    begin
        DBMS_OUTPUT.PUT_LINE('EMPLEADOS');
        open C_01;
        FETCH C_01 INTO r_aux1;
        DBMS_OUTPUT.PUT_LINE(r_aux1.APELLIDO||' '||r_aux1.SALARIO);
        CLOSE C_01;

        DBMS_OUTPUT.PUT_LINE(chr(10)||'DEPARTAMENTOS');
        open C_02;
        FETCH C_02 INTO r_aux2;
        DBMS_OUTPUT.PUT_LINE(r_aux2.NOMBRE||' '||r_aux2.NUM_EMPLEADOS||' '||r_aux2.SUMA_SALARIO);
        CLOSE C_02;

        DBMS_OUTPUT.PUT_LINE(chr(10)||'TOTAL');
        open C_03;
        FETCH C_03 INTO r_aux3;
        DBMS_OUTPUT.PUT_LINE(r_aux3.NUM_EMPLEADOS_TOTAL||' '||r_aux3.suma_salario_total);
        CLOSE C_03;

end pdatos;

begin
    pdatos();
end;

--3) Desarrollar un procedimiento que permita insertar nuevos departamentos seg�n las siguientes especificaciones: 
--Se pasar� al procedimiento el nombre del departamento y la localidad. El procedimiento insertar� la fila nueva 
--asignando como n�mero de departamento la decena siguiente al n�mero mayor de la tabla. Se incluir� gesti�n de 
--posibles errores.
set SERVEROUTPUT ON;
update CENTROS set direccion='C. SAGRADA FAMILIA 405, BARCELONA' where numero=10;

SELECT NUMERO FROM (SELECT * FROM DEPARTAMENTOS ORDER BY NUMERO DESC) WHERE ROWNUM=1;

create or replace procedure PINSERTAR_DEPARTAMENTO(NOMBRE VARCHAR2, LOCALIDAD VARCHAR2)
    is
        cursor C_01 IS SELECT SUBSTR(DIRECCION, INSTR(DIRECCION,',')+2) as direccion, numero FROM CENTROS;
        r_aux1 C_01%rowtype;
        ENCUENTRALOCALIDAD BOOLEAN;
        LOCALIDAD_NO_ENCONTRADA EXCEPTION;
        NUM_DEPT DEPARTAMENTOS.NUMERO%type;
    begin
        ENCUENTRALOCALIDAD:=FALSE;
        select max(numero)+10 into NUM_DEPT from departamentos;

        open C_01;
        loop
            FETCH C_01 INTO r_aux1;
            exit when C_01%NOTFOUND;
            IF (r_aux1.direccion = LOCALIDAD) THEN
                ENCUENTRALOCALIDAD:=TRUE;
                dbms_output.put_line(NUM_DEPT||to_char(r_aux1.numero)|| nombre);      
                insert into DEPARTAMENTOS(numero, centro, nombre) values (NUM_DEPT, r_aux1.numero, NOMBRE);
            END IF;
        END LOOP;
        close C_01;

        IF ENCUENTRALOCALIDAD=FALSE THEN 
            RAISE LOCALIDAD_NO_ENCONTRADA;
        END IF;
        
        EXCEPTION
            WHEN  LOCALIDAD_NO_ENCONTRADA THEN
                DBMS_OUTPUT.PUT_LINE('El parametro localidad no coincide con la direccion del centro');
END PINSERTAR_DEPARTAMENTO;

begin
    PINSERTAR_DEPARTAMENTO('SISTEMAS','MALAGA');
end;

select * from departamentos;

--4) Escribir un procedimiento que reciba todos los datos de un nuevo empleado procese la transacci�n de alta, 
--gestionando posibles errores.
create or replace procedure PINSERTAR_EMPLEADO(numero number, departamento number, telefono number, fecha_nacimiento date, fecha_ingreso date,
                salario number, comision number, num_hijos number, nombre varchar2)
    is
        cursor C_01 IS SELECT NUMERO FROM (SELECT * FROM DEPARTAMENTOS ORDER BY NUMERO DESC) WHERE ROWNUM=1;
        r_aux C_01%rowtype;
        NUM_DEPT DEPARTAMENTOS.NUMERO%type;
    begin
        open C_01;
        loop
            FETCH C_01 INTO r_aux;
            exit when C_01%NOTFOUND;
            NUM_DEPT:=r_aux.NUMERO;
        END LOOP;
        CLOSE C_01;
        --DBMS_OUTPUT.PUT_LINE(TO_CHAR(NUM_DEPT));
        
END PINSERTAR_EMPLEADO;

begin
    PINSERTAR_EMPLEADO();
end;